#ifndef XxsMaze_h
#define XxsMaze_h

#include <iostream>
#include"types/String.h"
#include"types/Direction.h"
#include"types/PlayerList.h"
#include"IxxsMaze.h"
#include"XxsMazeMap.h"

namespace xxsMaze {

class XxsMaze : public IxxsMaze{
public:
	XxsMaze(XxsMazeMap*map, const int m) 
		: state(LOGON), max(m), playerCount(0), ready(false), map(*map) {}

	~XxsMaze() {}
	
	//methods of IxxsMaze interface
	//-----------------------------------
	virtual PlayerId join(IPlayer* pl);

	virtual void leave(PlayerId ident);

	virtual void move(PlayerId ident, Direction dir);

	//-------------------------------------
	
	Point getPos(const PlayerId& id);
	void setPos(const PlayerId& id, const Point pos);
	bool isFree(const Point pos);

	const XxsMazeMap* getMap() const;
	GameState getState(){return state;}
	//void setState(GameState s){state = s;}

	bool isReady(){return playerCount == max;};

	/*
	*Send all players the start signal
	*/
	void start()
	{
		std::cerr<<"starting game and ..."<<std::endl;
		//send playerlist to all clients
		std::cerr<<"... sending player list to all clients"<<std::endl;
		for(unsigned long i=0; i < playerCount; i++){
		std::cerr<<"sending to"<<players[i]<<std::endl;
			players[i]->startGame(*(map.getAllPos()));
		}
		std::cerr<<"sent to all players"<<std::endl;
		state = IN_GAME;
	}

	/*
	*Send up to date gameinfo to all players
	*/
	void sendUpdate()
	{
		//send playerlist to all clients
		std::cerr<<"... sending player list to all clients"<<std::endl;
		for(unsigned long i=0; i < playerCount; i++){
			players[i]->update(*(map.getAllPos()));
		}
	}

private:
	//States the server can be in
	volatile GameState state;
	//Number of clients which will join us
	unsigned int max;
	//Number of clients that joined us so far
	unsigned int playerCount;
	//Flag if all clients have logged on AND received their gamedata
	bool ready;
	//mapdata
	XxsMazeMap map;	
	//List of Players
	PlayerList players;
};
} //end of namespace xxsMaze

#endif
