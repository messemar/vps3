#include "types/PlayerPositions.h"
#include "IPlayer.h"

/* need this hack to avoid wrong preprocessor dependencies */
#ifdef __JNI_INC_HACK__
#include <jni.h>
#endif

//namespace jxxsMaze {

class JavaPlayerProxy : public xxsMaze::IPlayer {
	jobject jPlayer;

public:
	// this is a hack! but i don't know another way :/
	// we need this to reset the reference to env
	// each time we want to call a method of JavaPlayerProxy.
	// Storing of env is not feasible because the reference
	// changes with each call from java!
	static JNIEnv* env; 

	JavaPlayerProxy(jobject player);

	~JavaPlayerProxy();
	
	void startGame(const xxsMaze::PlayerPositions);
	
	void update(const xxsMaze::PlayerPositions);
	
	void finish();
};

//} //end namespace jxxsMaze
