#ifndef IGame_h
#define IGame_h

#include "IPlayer.h"
#include "types/Direction.h"

//States the server can be in
typedef enum {LOGON,PRE_GAME,IN_GAME,FINISH} GameState;

namespace xxsMaze {

class IxxsMaze {
public:
	virtual ~IxxsMaze() {}

	virtual PlayerId join(IPlayer* pl) = 0;

	virtual void leave(PlayerId ident) = 0;

	virtual void move(PlayerId id, Direction dir) = 0;
};
} //end of namespace xxsMaze

#endif
