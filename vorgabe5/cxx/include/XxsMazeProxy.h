#ifndef XxsMazeProxy_h
#define XxsMazeProxy_h

#include <iostream>
#include"types/Direction.h"
#include"corba/IxxsMaze.h"
#include"corba/PlayerAdapter.h"
#include"IxxsMaze.h"
#include"XxsMaze.h"


namespace xxsMaze {

class XxsMazeProxy : public IxxsMaze{
public:
	XxsMazeProxy(Remote::IxxsMaze* game) 
		: remote(game) {}

	~XxsMazeProxy() 
	{
		delete remotePlayer;
	}
	
	//methods of IxxsMaze interface
	//-----------------------------------
	virtual PlayerId join(IPlayer* pl);

	virtual void leave(PlayerId ident);

	virtual void move(PlayerId ident, Direction dir);

	//-------------------------------------

private:
	Remote::IxxsMaze* remote;
	PlayerAdapter* remotePlayer;
};
} //end of namespace xxsMaze

#endif
