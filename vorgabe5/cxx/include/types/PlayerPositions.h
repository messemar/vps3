#ifndef types_PlayerPositions_h
#define types_PlayerPositions_h

//#include "IPlayerPositions.h"
#include <iostream>
#include <map>
#include "types/PlayerId.h"
#include "types/Point.h"

//class PlayerPositions : public IPlayerPositions{
namespace xxsMaze {

class PlayerPositions {
public:
	PlayerPositions() {}
	/*PlayerPositions(const PlayerPositions& other) 
	{
		std::cerr << "PlayPos::copycstr " << std::endl << std::flush;
		std::cerr << "numPlayers " << other.numPlayers() << std::endl << std::flush;
		for (int i=0; i< other.numPlayers(); i++) {
			add(i, other.getPos(i));
		}
	}*/

	/*PlayerPositions& operator=(const PlayerPositions& other)
	{
		std::cerr << "PlayerPositions operator= called!!!" << std::endl;
		for (int i=0; i< other.numPlayers(); i++) {
			add(i, other.getPos(i));
		}
		return *this;
	}*/
	
	PlayerPositions(const Point* p, int size)
	{
		for(PlayerId i = 0; i < size; i++) {
			add(i,p[i]);
		}
	}
		
	void add(PlayerId id, Point pos);

	void remove(PlayerId id);

	bool exists(PlayerId id);

	Point getPos(PlayerId id) const;

	void setPos(PlayerId id, Point pos);

	int numPlayers() const;

	bool isEmpty() const;

private:
	typedef std::map<PlayerId, Point> playermap_type;
	typedef playermap_type::const_iterator iterator;
	playermap_type playerList;
	iterator it;
};


inline
void PlayerPositions::add(PlayerId id, Point pos) {
	setPos(id,pos);
}

inline
void PlayerPositions::remove(PlayerId id) {
	iterator elem = playerList.find(id);
	if (elem != playerList.end()) {
		playerList.erase(elem->first);
	}
}

inline
bool PlayerPositions::exists(PlayerId id)
{
	return playerList.find(id) != playerList.end();
}

inline
Point PlayerPositions::getPos(PlayerId id) const {
	iterator ret = playerList.find(id);
	if (ret == playerList.end()) {
		return Point::undefined();
	}
	
	return (*ret).second;
}

inline
void PlayerPositions::setPos(PlayerId id, Point pos) {
	playerList[id] = pos;
}

inline
int PlayerPositions::numPlayers() const {
	return playerList.size();
}
	
inline
bool PlayerPositions::isEmpty() const
{
	return playerList.empty();
}
} //end of namespace xxsMaze

#endif
