#ifndef types_Point_h
#define types_Point_h

#include <limits>

// a coordinate
namespace xxsMaze {

class Point {
public:
	typedef unsigned long coord_type;
	
	Point() {}

	/*Point(const Point& other) 
	{
		std::cerr << "Point copy cstr called!" << std::endl;
		x = other.x;
		y = other.y;
	}
	*/
	

	/*Point(Point& other) 
	{
		x = other.x;
		y = other.y;
	}
	*/

	bool operator==(const Point& p) const { return isEqual(p); }
	//Point& operator=(const Point& p) { return assign(p); }

	Point(const coord_type x, const coord_type y) 
	{
		this->x = x;
		this->y = y;
	}
	
	coord_type x;
	coord_type y;

	static Point const undefined() 
	{
		Point p;
		p.x = undef;
		p.y = undef;
		return p;
	}

	bool isUndefined()
	{
		return ((x == undef) || (y == undef));
	}

	bool isEqual(const Point& p) const
	{
		return ((this->x == p.x) && (this->y == p.y));
	}

	/*Point& assign(const Point& p)
	{
		x = p.x;
		y = p.y;
		return *this;
	}
	*/

private:
	// highest possible value for 32-bit 
	static const coord_type undef;
};
} //end of namespace xxsMaze

#endif
