#ifndef Direction_h
#define Direction_h

namespace xxsMaze {

// directions the player can move to
enum Direction { 
	STAY, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT 
};

} //end of namespace xxsMaze

#endif
