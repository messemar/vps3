#include "XxsMaze.h"
#include <assert.h>

using namespace xxsMaze;

PlayerId XxsMaze::join(IPlayer* pl) 
{
	assert(pl != NULL);
	std::cerr << "XxsMaze::join called" << std::endl;
	if (state != LOGON) {
		std::cerr << "Game is in Progress, no logon possible!" << std::endl;
		return 4711;
	}
	// add player to list
	std::cerr << "XxsMaze::join adding player to List" << pl << std::endl;
	players.add(playerCount,pl);
	// generate initial start point for the player
	//std::cerr << "generate start point" << &players << std::endl;
	Point p;
	assert(map.width() != 0);
	do {
		p.x = random()%map.width();
		p.y = random()%map.height();
	} while (!map.isFree(p));
	
	// update map
	std::cerr << "XxsMaze::join updating map" << std::endl << std::flush;
	map.setPlayerPos(playerCount,p);

	if ((unsigned int)playerCount+1 == max) {
		std::cerr << "all clients have logged on" << std::endl << std::flush;
		state = PRE_GAME;
	}
	
	return playerCount++; // so the id is ok for indexing the arrays
}


void XxsMaze::move(PlayerId player, Direction direction) {
	if (state == IN_GAME) {
		map.move(player, direction);
	} else {
		std::cerr << "game not started yet!!" << std::endl;
	}
}


void XxsMaze::leave(PlayerId player) {
	std::cerr<<"logout[ ID: "<<player<<" ]"<<std::endl;
	players[player]->finish();
	players.remove(player);
}
