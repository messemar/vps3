#include <string.h>
#include <iostream>
#include <fstream>
#include "XxsMazeMap.h"

using namespace xxsMaze;

void XxsMazeMap::open(const char* file) {
	unsigned long height = 0;
	unsigned long width = 0;
	char buf[1024];
	
	// read file into Map
	std::ifstream in(file);
	while (in.getline(buf,sizeof(buf)) && buf[0]) {
		size_t buf_len = std::strlen(buf);

		std::cerr<<"read line [ length: "<<buf_len<<" | "<<buf<<" ]"<< std::endl;
		
		// check if line lenght is correct
		if (height && width != buf_len) {
			std::cerr << "[" << file << "] wrong fileformat. line " << height+1 
				<< " is " << buf_len << " characters long and not " << width 
				<< " like the first line." << std::endl;
			exit(-1);
		} else
			width = buf_len;
		// copy the bufer into a new line of the map
		std::vector<MapChar>* line = new std::vector<MapChar>(strlen(buf));
		while (buf_len) {
			buf_len--;
			(*line)[buf_len] = buf[buf_len];
		}
		height++;
		// append the new line to the Map
		map.push_back(*line);
	}
	in.close();
	
	std::cerr<<"read Map [ height: "<<this->height()<<" | width: "<<this->width()
		<<" ]"<< std::endl;
}
