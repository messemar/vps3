#define __JNI_INC_HACK__
#include "JavaPlayerProxy.h"
#undef __JNI_INC_HACK__
#include "types/PlayerPositions.h"
#include "types/Point.h"
#include <string>
#include <iostream>
#include <exception>
#include <assert.h>

//namespace jxxsMaze {
	using xxsMaze::PlayerPositions;
	using xxsMaze::Point;

JavaPlayerProxy::JavaPlayerProxy(jobject player) {
	assert(env != NULL);
	// create new global reference to player
	// ...
}


void JavaPlayerProxy::startGame(const PlayerPositions pos) 
{
	assert(env != NULL);
	// convert c++-pos to java-pos
	// ...

	// finaly invoke startGame on the real player
	// (that's an RMI!)
	// ...
}


void JavaPlayerProxy::update(const PlayerPositions pos) 
{
	assert(env != NULL);
	// convert c++-pos to java-pos
	// ...

	// finaly invoke startGame on the real player
	// (that's an RMI!)
	// ...
}


void JavaPlayerProxy::finish() 
{
	assert(env != NULL);
	// just delegate the call to the player
	// ...
}


JavaPlayerProxy::~JavaPlayerProxy() {
	// should release the java reference but 
	// have no pointer to the JVM :/
}

//} // end namespace jxxsMaze
