// Implement the functions defined in the file generated from javah here
// The definitions and includes below are just hints. It is not mandatory
// (though advisable)to use them.
// The map is needed for storing the references to the XxsMaze instances. 
// Note that this is going to be built into a shared library. If multiple
// servers should happen to run on the same machine, we need some means to
// store and retrieve references to multiple games. Since each game can host
// multiple players we need to store a map of JavaPlayerProxies for each game
// instance. 

#include <map>
#include <assert.h>
#include "jni.h"
#include "XxsMaze.h"
#include "XxsMazeMap.h"
#include "native/jxxsMaze_XxsMaze.h"
#include "types/MapData.h"
#include "JavaPlayerProxy.h"

using xxsMaze::XxsMaze;
using xxsMaze::XxsMazeMap;
using xxsMaze::MapData;
using xxsMaze::MapLine;
using xxsMaze::MapChar;
using xxsMaze::Direction;
using std::map;
using std::cerr;
using std::endl;
using std::flush;

static map<int, XxsMaze*> games;
static map<int, map<int, JavaPlayerProxy*> > myProxy;
JNIEnv* JavaPlayerProxy::env;
