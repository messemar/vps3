/**
 * 
 */
package jxxsMaze;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

import jxxsMaze.swing.Instruction;
import jxxsMaze.swing.InstructionConsumer;
import jxxsMaze.types.Direction;
import jxxsMaze.types.PlayerPositions;
import jxxsMaze.types.PlayerState;

/**
 * @author Andreas Lagemann
 *
 */
public class XxsMazeHuman extends UnicastRemoteObject 
	implements IPlayer, InstructionConsumer  {

	/**
	 * 
	 */
	public XxsMazeHuman(IxxsMaze game, String filename, String name) 
		throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
		myGame = game;
		state = PlayerState.START_WAIT;
		ui = new XxsMazeGUI(filename, this, name);
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IPlayer#startGame(jxxsMaze.types.PlayerPositions)
	 */
	public void startGame(PlayerPositions pos) {
		// TODO Auto-generated method stub
		System.out.println("Human::startGame");
		this.update(pos);
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IPlayer#update(jxxsMaze.types.PlayerPositions)
	 */
	public void update(PlayerPositions pos) {
		// TODO Auto-generated method stub
		System.out.println("Human::update");
		ui.update(pos);
		state = PlayerState.GAMEING;
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IPlayer#finish()
	 */
	public void finish() {
		// TODO Auto-generated method stub
		state = PlayerState.FINISHED;
	}
	
	/* (non-Javadoc)
	 * @see jxxsMaze.swing.InstructionConsumer#consume(jxxsMaze.swing.Instruction)
	 */
	public void consume(Instruction i) {
		// TODO Auto-generated method stub
		switch (i) {
		case QUIT:

			break;
		case UP:
			way.add(Direction.UP);			
			break;
		case UP_RIGHT:
			way.add(Direction.UP_RIGHT);
			break;
		case RIGHT:
			way.add(Direction.RIGHT);
			break;
		case DOWN_RIGHT:
			way.add(Direction.DOWN_RIGHT);
			break;
		case DOWN:
			way.add(Direction.DOWN);
			break;
		case DOWN_LEFT:
			way.add(Direction.DOWN_LEFT);
			break;
		case LEFT:
			way.add(Direction.LEFT);
			break;
		case UP_LEFT:
			way.add(Direction.UP_LEFT);
			break;

		default:
			break;
		}
	}
	
	public void startup()
	{
		System.out.println("Human startup");
		try {
			myId = myGame.join(this);
			//long time = System.currentTimeMillis();
			//while(System.currentTimeMillis() < time + 10000000 );
			System.out.println("Id" + myId + myGame.toString());
		} catch(RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public void makeStep()
	{		

		try {
			if (!way.isEmpty()) {
				System.out.println("Human makeStep");
				myGame.move(myId,way.remove(0));
			}
		} catch(RemoteException e) {
			e.printStackTrace();
			System.out.println(myGame.toString());
		}
	}
	
	PlayerState getState()
	{
		return state;
	}
	
	private Vector<Direction> way = new Vector<Direction>();
	private short myId;
	private IxxsMaze myGame;
	private PlayerState state;
	private XxsMazeGUI ui;
}
