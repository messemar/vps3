/**
 * 
 */
package jxxsMaze;

import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;

import jxxsMaze.map.Map;
import jxxsMaze.types.Direction;

/**
 * @author loki
 *
 */
public class XxsMaze extends UnicastRemoteObject implements IxxsMaze {

	/**
	 * @throws RemoteException
	 */
	public XxsMaze(Map map, int num) throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
		myCppHandle = _create(map,num); 
	}

	/**
	 * @param port
	 * @throws RemoteException
	 */
	public XxsMaze(int port, Map map, int num) throws RemoteException {
		super(port);
		// TODO Auto-generated constructor stub
		myCppHandle = _create(map,num); 
	}

	/**
	 * @param port
	 * @param csf
	 * @param ssf
	 * @throws RemoteException
	 */
	public XxsMaze(int port, RMIClientSocketFactory csf,
		RMIServerSocketFactory ssf, Map map, int num) throws RemoteException {
		super(port, csf, ssf);
		// TODO Auto-generated constructor stub
		myCppHandle = _create(map,num); 
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IxxsMaze#join(jxxsMaze.IPlayer)
	 */
	public short join(IPlayer pl) {
		// TODO Auto-generated method stub
		return _join(myCppHandle, pl);
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IxxsMaze#leave(short)
	 */
	public void leave(short id) {
		// TODO Auto-generated method stub
		_leave(myCppHandle,id);
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IxxsMaze#move(short, jxxsMaze.types.Direction)
	 */
	public void move(short id, Direction dir) {
		// TODO Auto-generated method stub
		System.out.println(dir.toString());
		_move(myCppHandle,id,dir);
	}

	public GameState getState() {
		GameState stateArr[] = GameState.values();
		return stateArr[_getState(myCppHandle)];
	}
	
	public void start() {
		_start(myCppHandle);
	}
	
	public void sendUpdate() {
		_sendUpdate(myCppHandle);
	}
	
	private native int _create(Map map, int num);
	private native short _join(int handle, IPlayer pl);
	private native void _leave(int handle, short id);
	private native void _move(int handle, short id, Direction dir);
	private native int _getState(int handle);
	private native void _start(int handle);
	private native void _sendUpdate(int handle);
	
	private int myCppHandle = -1;
	//private GameState state = GameState.LOGON;

	static { 

        System.loadLibrary("xxsMaze"); 

    }

}

