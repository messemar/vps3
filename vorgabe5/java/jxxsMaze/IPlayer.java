/**
 * 
 */
package jxxsMaze;

import java.rmi.Remote;
import java.rmi.RemoteException;

import jxxsMaze.types.PlayerPositions;

/**
 * @author loki
 *
 */
public interface IPlayer extends Remote
{
	void startGame(PlayerPositions pos) throws RemoteException;
	
	void update(PlayerPositions pos) throws RemoteException;
	
	void finish() throws RemoteException;
}
