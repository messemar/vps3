/**
 * 
 */
package jxxsMaze;

import java.rmi.Remote;
import java.rmi.RemoteException;

import jxxsMaze.types.Direction;

/**
 * @author loki
 *
 */
public interface IxxsMaze extends Remote {

	short join(IPlayer pl) throws RemoteException;
	
	void leave(short id) throws RemoteException;
	
	void move(short id, Direction dir) throws RemoteException;
}
