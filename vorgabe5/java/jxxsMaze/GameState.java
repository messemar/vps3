/**
 * 
 */
package jxxsMaze;

/**
 * @author loki
 *
 */
public enum GameState {
	LOGON(0),
	PRE_GAME(1),
	IN_GAME(2),
	FINISH(3);
	
	private GameState(int v) {
		val = v;
	}
	
	public int val() { return val; }
	
	private final int val;
}
