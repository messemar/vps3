package jxxsMaze.map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import jxxsMaze.types.Direction;
import jxxsMaze.types.PlayerPositions;
import jxxsMaze.types.Point;

public class Map {

	/**
	 * Create a copy of the given map
	 * @param w the width of the rectangular map
	 * @param h the height of the rectangular map
	 */
	public Map(final Map other)
	{
		map = other.getMapVector();
	}
	
	/**
	 * Create a map and read the contents from the file named filename
	 * @see readFromFile
	 * @param filename the name of the file with the map data
	 */
	public Map(final String filename)
	{
		readFromFile(filename);		
	}
	
	public char [][] getMapData()
	{
		char ret[][] = new char[map.size()][];
		int height = 0;
		for (Vector<Character> line : map) {
			ret[height] = new char[line.size()];
			int width = 0;
			for (Character character : line) {
				ret[height][width] = character.charValue();
				width++;
			}
			height++;
		}
		return ret;
	}
	
	public Vector<Vector<Character> > getMapVector() {
		return map;
	}
	
	public int height()
	{
		return map.size();
	}

	public int width()
	{
		return map.get(0).size();
	}

	public char val(final Point p) 
	{
		return map.get(p.y).get(p.x);
	}
	
	public char val(final int x, final int y) 
	{		
		return map.get(y).get(x);
	}
	
	public boolean isFree(final Point p) 
	{
		return map.get(p.y).get(p.x) == MapChars.MAP_WAY;
	}
	
	public boolean isFree(final int x, final int y) 
	{
		return map.get(y).get(x) == MapChars.MAP_WAY;
	}
	
	public Point getPlayerPos(short id)
	{
		return positions.getPos(id);
	}
	
	/**
	 * @return Returns the positions of all players.
	 */
	public PlayerPositions getAllPos() {
		return positions;
	}

	/**
	 * @param pos The positions to set.
	 */
	public void setAllPos(PlayerPositions pos) {
		if(positions != null) {
			for (short i = 0; i < positions.numPlayers(); i++) {
				Point p = positions.getPos(i);
				map.get(p.y).set(p.x,Character.valueOf(MapChars.MAP_WAY));
			}
		}
		for (short i = 0; i < pos.numPlayers(); i++) {
			//System.out.println("Map::setAllPos");
			Point p = pos.getPos(i);
			//System.out.println("Player " + i + " at (" 
			//		+ p.x + "," + p.y +")");
			map.get(p.y).set(p.x,Character.valueOf((char)i));
		}
		this.positions = pos;
	}
	
	public void move(short id, Direction dir) {
		Point pos = positions.getPos(id);
		switch (dir) {
		case UP:
			pos.y -= 1;
			break;
		case UP_RIGHT:
			pos.y -= 1;
			pos.x += 1;
			break;
		case RIGHT:
			pos.x += 1;
			break;
		case DOWN_RIGHT:
			pos.y += 1;
			pos.x += 1;
			break;
		case DOWN:
			pos.y += 1;
			break;
		case DOWN_LEFT:
			pos.y += 1;
			pos.x -= 1;
			break;
		case LEFT:
			pos.x -= 1;
			break;
		case UP_LEFT:
			pos.y -= 1;
			pos.x -= 1;
			break;
		default:
			break;
		}
	}
	
	public void removePlayer(short id) {
		positions.remove(id);
	}
	
	public int numPlayers() {
		return positions.numPlayers();
	}

	/**
	 * Read the file named filename and add the contents to
	 * map
	 * @param filenameVector< Vector<Character> > v = map.getMapData();
		Vector<Character> vc[]; 
		v.toArray(vc);
		
	 */
	public void readFromFile(String filename)
	{
		int height = 0;
		int width = 0;
		
		FileReader f = null;
		System.out.println("Reading Map from file: " + filename);
		try {
			f = new FileReader(filename);
		} catch (FileNotFoundException e) {
			System.out.println("File " + filename + " not found!!!!");
			System.out.println("Exiting!");
			e.printStackTrace();
			System.exit(-1);
		}
		
		BufferedReader br = new BufferedReader(f);
		
		try {
			for(String line; (line = br.readLine()) != null;) {
				System.out.println(line);
				if(line.length() == 0) {
					continue;
				}
				
				if (height > 0 && width != line.length()) {
					System.out.println("Incorrect line width\n Exiting now!!!");
					System.out.println(height + " " + " " + width + line);
					System.exit(-1);
				}
				
				width = line.length();
				
				map.add(height, new Vector<Character>(width));
				char buf[] = new char[width];
				line.getChars(0,width, buf, 0);
				for (int i = 0; i < buf.length; i++) {
					map.get(height).add(i,buf[i]);
				}
				height++;
			}
		} catch (IOException e) {
			System.out.println("Incorrect file format\n Exiting now!!!");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public final class MapChars {
		static public final char MAP_WAY = ' ';
		static public final char MAP_WALL = 'x';
	}
	
	private Vector< Vector<Character> > map = new Vector< Vector<Character> >();
	
	private PlayerPositions positions;
}
