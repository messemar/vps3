/**
 * 
 */
package jxxsMaze.swing;

/**
 * @author loki
 *
 */
public interface InstructionConsumer {

	public void consume(Instruction i);
}
