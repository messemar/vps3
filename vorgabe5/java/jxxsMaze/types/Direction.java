/**
 * 
 */
package jxxsMaze.types;
import java.lang.String;

/**
 * @author loki
 *
 */
public enum Direction {
	STAY, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT;
}
