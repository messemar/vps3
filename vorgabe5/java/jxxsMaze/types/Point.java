/**
 * JxxsMaze - A Java implementation of the xxsMaze game 
 * 
 * Class(es):	Point
 * Last edit:	04.10.2005
 * Author:	Andreas Lagemann
 * Description:
 * 		This class represents a point on the map
 */
package jxxsMaze.types;

import java.io.Serializable;

/**
 * @author Andreas Lagemann
 *
 */
public class Point implements Serializable{
	
	public Point() 
	{
		x = undef;
		y = undef;
	}

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public boolean isUndefined(Point p) 
	{
		return (p.x == undef) || (p.y == undef);
	}
	
	public static Point undefined()
	{
		Point p = new Point(undef,undef);
		return p;
	}
	
	public int x;
	public int y;
	
	private static final int undef = Integer.MAX_VALUE;
}
