/**
 * JxxsMaze - A Java implementation of the xxsMaze game 
 * 
 * Class(es):	PlayerPositions
 * Last edit:	04.10.2005
 * Author:	Andreas Lagemann
 * Description:
 * 		This encapsulates a map of player ids to their corresponding
 * 		positions.
 */
package jxxsMaze.types;

import java.io.Serializable;
import java.util.*;
import jxxsMaze.types.Point;
/**
 * @author Andreas Lagemann
 *
 */

public class PlayerPositions implements Serializable{
	//use the name short as if it were an int;
	
	public PlayerPositions() {}
	
	public void add(short id, Point pos)
	{
		map.put(id,pos);
	}

	public void add(short id, int x, int y)
	{
		map.put(id,new Point(x,y));
	}

	
	public void remove(short id)
	{
		map.remove(id);
	}

	public boolean exists(short id)
	{
		return map.containsKey(id);
	}

	public Point getPos(short id)
	{
		return map.get(id);
	}

	public void setPos(short id, Point pos)
	{
		map.put(id,pos);
	}

	public int numPlayers()
	{
		return map.size();
	}

	public boolean isEmpty()
	{
		return map.isEmpty();
	}
	
	private Map<Short,Point> map = new TreeMap<Short, Point>();
}
