#include <iostream>
#include <fstream>
#include <CORBA.h>
#include <coss/CosNaming.h>	// needed for the naming service

// include the generated Interface mapping
#include "Sheep.h"

#include "CPUTimer.h"
using namespace std;


int main(int argc, char* argv[]) {
	// initialize the ORB
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");
	
	// get the Sheep's ObjectId from somewhere
	// and convert it back to the object reference
	// maybe the _narrow()-function can help
	/*
	first part
	fstream instream;
	instream.open("location.loc",ios::in);
	char input[500];
	instream.getline(input,500);
	instream.close();
	CORBA::Object_var obj = orb-> string_to_object(input);
	*/
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
	CosNaming::NamingContext_var nc= CosNaming::NamingContext::_narrow(nsobj);
	CosNaming::Name name;
	name.length(1);
	name[0].id= CORBA::string_dup("Sheep");
	name[0].kind=CORBA::string_dup("");
	CORBA::Object_var obj = nc->resolve(name);
	BlackSheep_var bsheep=BlackSheep::_narrow(obj);
	Sheep_var sheep_ref=Sheep::_narrow(obj);
	
	
	// now use the object reference like a normal 
	// pointer to invoke it's method's
	CPUTimer timer(2500);
	timer.start();
	std::cout<<"Weight: "<<sheep_ref->weight()<< std::endl;
	std::cout<<"Weight: "<<bsheep->weight()<< std::endl;
	sheep_ref->feed(100);
	sheep_ref->shear();
	std::cout<<"Weight after feed and shear: "<<sheep_ref->weight()<<" Current time for weight,feed,shear and weight is: "<< timer.stop()<<std::endl;
	return 0;
}
