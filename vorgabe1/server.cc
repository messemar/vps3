#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <CORBA.h>
#include <coss/CosNaming.h>	// needed for the naming service

// include the generated Interface mapping
#include "Sheep.h"


// include the Sheep's implementation class direct into the
// server to reduce compile time ;)
// 
// we are using BOA so have to inherit from <CLASS>_skel
class Sheep_impl : virtual public POA_BlackSheep {
	// have to use CORBA data types
	CORBA::ULong cur_weight;

public:
	Sheep_impl() {
		cur_weight = 20+random()%50;
	}

	// jam. jam.
	void feed(CORBA::ULong amount) {
		cur_weight += amount;
	}

	// check the weight from time to time
	CORBA::ULong weight() {
		return cur_weight;
	}

	// to reduce weight ;)
	void shear() {
		cur_weight -= cur_weight/10;
	}

	void maaah(){}
};


using namespace std;


// here comes the server
int main(int argc, char* argv[]) {
	// initialize the ORB
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");
	
	// get a reference to the RootPOA...
	CORBA::Object_var poaobj = orb->resolve_initial_references("RootPOA");
	PortableServer::POA_var poa = PortableServer::POA::_narrow(poaobj);
	// ...and the manager
	PortableServer::POAManager_var mgr = poa->the_POAManager();

	// create new Sheep
	// ...
	Sheep_impl* sheep=new Sheep_impl();

	// and activate it
	// ...
	PortableServer::ObjectId_var oid=poa->activate_object(sheep);
	mgr->activate();
	// implement a way to tell the client the ObjectId 
	// without using the terminals cut-n-paste ;)
	// but maybe the ORB's object_to_string()-function
	// can help
	// ...
	/*
		part one
	CORBA::Object_var ref= poa->id_to_reference(oid);
	
	char* out=orb->object_to_string(ref);
	
	fstream outstream;
	outstream.open("location.loc", ios::out);
	outstream<< out<< endl;	
	outstream.close();
	*/

	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
	CosNaming::NamingContext_var nc = CosNaming::NamingContext::_narrow(nsobj);

	CosNaming::Name name;
	name.length(1);
	name[0].id=CORBA::string_dup("Sheep");
	name[0].kind=CORBA::string_dup("");
	nc->bind(name, sheep->_this());
	
	// start dispatching methods to the Sheep object
	orb->run();

	// shutdown managed object
	poa->destroy(TRUE, TRUE);

	// then free space occupied by sheep
	// ...
	return 0;
}
