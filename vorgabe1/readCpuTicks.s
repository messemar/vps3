.text
	.align 4
.globl readCpuTicks
#ifndef DARWIN
	.type	 readTimer,@function
#endif

readCpuTicks:
	.byte 0x0f			# magic instruction code to access timer register
	.byte 0x31			# ... will store 64 Bit value in eax and edx
	ret
