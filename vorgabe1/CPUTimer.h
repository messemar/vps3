#ifndef CPUTimer_h
#define CPUTimer_h 


// read timer register of Pentium CPU

extern "C" unsigned long long readCpuTicks();

class CPUTimer {
public:
	CPUTimer(unsigned long SpeedMHz) { mhz = SpeedMHz; }
	
	void start() { t0 = readCpuTicks(); }

	// measurement is in CPU-Cycles! See /proc/cpuinfo 
	unsigned long long stop() { t1 = readCpuTicks(); return (t1-t0)/mhz; }

private:
	unsigned long long t0;
	unsigned long long t1;
	unsigned long mhz;
};

#endif /* Timer_h  */
