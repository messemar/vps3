using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.InteropServices;


namespace Remote {

	// this implementation is trivial, so it is given completely
public class XxsMazeBot : MarshalByRefObject, Remote.IPlayer {

		 public XxsMazeBot(Local.IPlayer l) {
			 local = l;
		 }

		 ~XxsMazeBot() {
			 Console.WriteLine("XxsMazeBot finalized!");
		 }

		 public void startGame(Local.PlayerPositions pos)
		 {
			 local.startGame(pos);
		 }

		 public void update(Local.PlayerPositions pos)
		 {
			 local.update(pos);
		 }

		 public void finish()
		 {
			 local.finish();
		 }

		 private Local.IPlayer local;
	}

}
