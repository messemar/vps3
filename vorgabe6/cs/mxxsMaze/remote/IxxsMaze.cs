namespace Remote {

	public interface IxxsMaze {

		ushort join(IPlayer pl);

		void leave(ushort ident);

		void move(ushort ident, Local.Direction dir);

		int ping();

	};

}
