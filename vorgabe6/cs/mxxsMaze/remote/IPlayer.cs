namespace Remote {

	public interface IPlayer {

		void startGame(Local.PlayerPositions pos);

		void update(Local.PlayerPositions pos);

		void finish();

	};
}
