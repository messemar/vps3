using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.InteropServices;

namespace Remote {

	public class XxsMaze : MarshalByRefObject, Remote.IxxsMaze {

		 public XxsMaze(Local.XxsMaze l)
		 {
			 // save reference to local game
			 // do all neccessary initializations
			 local = l;
			 players = new ArrayList();
		 }

		 public ushort join(Remote.IPlayer pl)
		 {
			 // create PlayerProxy and call join method of local game instance
			 Local.IPlayer player = new Local.XxsMazePlayerProxy(pl);
			 // note: keep a reference to all proxies, because otherwise the
			 // garbage collector would collect the proxy in its next run
			 ushort id =  local.join(player);
			 players.Insert(id, player);
			 return id;
		 }

		 public void leave(ushort ident)
		 {
			 // call leave method of local instance
			 // take care to clear the reference to the corresponding
			 // proxy, so the garbage collector can take care of it
			 players.RemoveAt(ident);
			 local.leave(ident);
		 }

		 public void move(ushort ident, Local.Direction dir)
		 {
			 local.move(ident, dir);
		 }
		 
		 public int ping()
		 {
			return 0815;
		 }

		 private Local.XxsMaze local;
		 //put more members here ...
		 private ArrayList players;
	};

}
