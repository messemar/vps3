using System;
using System.Threading;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Collections;
using System.Runtime.Serialization.Formatters;
namespace Remote {

public class XxsMazeServer {

	static public void Main(string[] args)
	{
       if( args.Length != 4){
            Console.WriteLine(" usage XxsMazeClient <Mapfile> <Playercount> > <port>");
       } 
       string filename = args[0];
		short playerCount = short.Parse(args[1]);
		int port = int.Parse(args[2]); 
		// create new game
        Local.XxsMazeMap map= new Local.XxsMazeMap(filename);
        Local.XxsMaze game = new Local.XxsMaze(map,playerCount);
		// create wrapper object for the game which can be
		// exported to the global object space
        Remote.XxsMaze remoteGame= new Remote.XxsMaze(game);
		// register tcp channel with dedicated port
        BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();

        provider.TypeFilterLevel = TypeFilterLevel.Full;
        IDictionary props = new Hashtable();
        props["port"] = port;
        ChannelServices.RegisterChannel(new TcpChannel(props,null,provider),false);
		// make object accessible for remote clients
        RemotingServices.Marshal(remoteGame,"Game.srv");  
		// enter the game loop
        Local.GameState state;
        do{
            state=game.getState();
            switch(state){
                case Local.GameState.PRE_GAME:
                    game.start();
                    break;
                case Local.GameState.IN_GAME:
                    game.sendUpdate();
                    break;
                default:
                    break;
            }
        Thread.Sleep(1000);
        }while(state != Local.GameState.FINISH);
	}
};

}
