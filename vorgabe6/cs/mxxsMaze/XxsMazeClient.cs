using System;
using System.Threading;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Collections;
using System.Runtime.Serialization.Formatters;

namespace Remote {

public class XxsMazeClient {
	public static void Main(string[] args) {

		if (args.Length != 4) {
			Console.WriteLine("usage XxsMazeClient <mapfile> <playername> <port>");
			//System.exit(-1);
		}

		string filename = args[0];
		string name = args[1];
		int port = int.Parse(args[2]);

		// register tcp channel	
		//get remote reference to game
        BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();

        provider.TypeFilterLevel = TypeFilterLevel.Full;
        IDictionary props = new Hashtable();
        props["port"] = port;
        ChannelServices.RegisterChannel(new TcpChannel(props,null,provider),false);
		Remote.XxsMaze remoteGame = (Remote.XxsMaze) Activator.GetObject(typeof(XxsMaze), "tcp://localhost:4711/Game.srv");

		// create proxy object for game which resolves to
		// C++ object when get CPtr is called
		Local.XxsMazeProxy gameProxy = new Local.XxsMazeProxy(remoteGame);
		
		// create player object and pass proxy as reference to game
		Local.XxsMazeBot player = new Local.XxsMazeBot(gameProxy, filename, name);
		//XxsMazeBot player = new XxsMazeBot(localPlayer); 

		// start the game
		player.startup();
		
		// the game loop follows
		
		Local.PlayerState state;

		do
		{
			state = player.getState();
			switch(state) {
				case Local.PlayerState.GAMEING:
					player.makeStep();
					break;
				case Local.PlayerState.START_WAIT:
					break;
				case Local.PlayerState.WAIT4UPDATE:
				default:
					break;
			}
		}while(state != Local.PlayerState.FINISHED);
	}
}

}
