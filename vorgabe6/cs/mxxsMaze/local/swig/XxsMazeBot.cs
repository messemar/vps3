//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.8
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace Local {

public class XxsMazeBot : IDisposable, Local.IPlayer {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal XxsMazeBot(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef _getCPtr(XxsMazeBot obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

	public HandleRef getCPtr() {
		return _getCPtr(this);
	}

  ~XxsMazeBot() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          xxsMazePINVOKE.delete_XxsMazeBot(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
    }
  }

  public XxsMazeBot(XxsMazeBot other) : this(xxsMazePINVOKE.new_XxsMazeBot__SWIG_0(other.getCPtr()), true) {
  }

  public XxsMazeBot(Local.IxxsMaze game, string filename, string name) : this(xxsMazePINVOKE.new_XxsMazeBot__SWIG_1(game.getCPtr(), filename, name), true) {
  }

  public void startGame(PlayerPositions pos) {
    xxsMazePINVOKE.XxsMazeBot_startGame(swigCPtr, PlayerPositions.getCPtr(pos));
    if (xxsMazePINVOKE.SWIGPendingException.Pending) throw xxsMazePINVOKE.SWIGPendingException.Retrieve();
  }

  public void update(PlayerPositions pos) {
    xxsMazePINVOKE.XxsMazeBot_update(swigCPtr, PlayerPositions.getCPtr(pos));
    if (xxsMazePINVOKE.SWIGPendingException.Pending) throw xxsMazePINVOKE.SWIGPendingException.Retrieve();
  }

  public void finish() {
    xxsMazePINVOKE.XxsMazeBot_finish(swigCPtr);
  }

  public SWIGTYPE_p_String getName() {
    SWIGTYPE_p_String ret = new SWIGTYPE_p_String(xxsMazePINVOKE.XxsMazeBot_getName(swigCPtr), true);
    return ret;
  }

  public XxsMazeMap getMap() {
    XxsMazeMap ret = new XxsMazeMap(xxsMazePINVOKE.XxsMazeBot_getMap(swigCPtr), false);
    return ret;
  }

  public PlayerState getState() {
    PlayerState ret = (PlayerState)xxsMazePINVOKE.XxsMazeBot_getState(swigCPtr);
    return ret;
  }

  public void calcWay(Point source, Point dest) {
    xxsMazePINVOKE.XxsMazeBot_calcWay(swigCPtr, Point.getCPtr(source), Point.getCPtr(dest));
    if (xxsMazePINVOKE.SWIGPendingException.Pending) throw xxsMazePINVOKE.SWIGPendingException.Retrieve();
  }

  public void startup() {
    xxsMazePINVOKE.XxsMazeBot_startup(swigCPtr);
  }

  public void makeStep() {
    xxsMazePINVOKE.XxsMazeBot_makeStep(swigCPtr);
  }

  public bool reachedGoal() {
    bool ret = xxsMazePINVOKE.XxsMazeBot_reachedGoal(swigCPtr);
    return ret;
  }

}

}
