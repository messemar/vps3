using System.Runtime.InteropServices;

namespace Local {

	public interface IPlayer {

		void startGame(Local.PlayerPositions pos);

		void update(Local.PlayerPositions pos);

		void finish();
		
		HandleRef getCPtr();

	};
}
