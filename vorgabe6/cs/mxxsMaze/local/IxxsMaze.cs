using System.Runtime.InteropServices;

namespace Local {

	public interface IxxsMaze {

		ushort join(IPlayer pl);

		void leave(ushort ident);

		void move(ushort ident, Local.Direction dir);

		HandleRef getCPtr();

	};

}
