using System;
using System.Runtime.Remoting;
using System.Runtime.InteropServices;

// declare the apropriate delegates for the three methods of the
public delegate ushort JoinDelegate(IntPtr pl);
public delegate void LeaveDelegate(ushort ident);
public delegate void MoveDelegate(ushort id, int dir);

// IxxsMaze interface

namespace Local  {

	/* This class is the glue between .NET remoting and the C++ code.
	 * It serves as proxy for the game on the player side.
	 * You can give a reference to this class to the native implementation
	 * of the Player (because this implements th Local.IxxsMaze interface)
	 * If you do so, the intermediate code calls the getCPtr method to retrieve
	 * a valid reference to a C++ class implementing IxxsMaze(the C++ variant).
	 * The getCPtr-method instantiates a Native class (which was also wrapped
	 * by swig) which is initialized with pointers to this class's methods
	 * (delegates on the C# side). 
	 * Given that, a call from the native side is mapped to a call to the
	 * corrsponding method of this class which in turn calls the remote
	 * method.
	 */


	public class XxsMazeProxy :  Local.IxxsMaze {

		 public XxsMazeProxy(Remote.IxxsMaze r)
		 {
			 // save a reference to the remote game
			 remote = r;
			 proxy = null;
		 }

		 public ushort join(Local.IPlayer pl)
		 {
			 // create a new remote instance for the player
			 Remote.XxsMazeBot player = new Remote.XxsMazeBot(pl);
			 // and publish it to the remoting system.
			 RemotingServices.Marshal(player);
			 // after that call the remote join-method using
			 // a reference to the new player object
			 return remote.join(player);
		 }

		 public void leave(ushort ident)
		 {
			 remote.leave(ident);
		 }

		 public void move(ushort ident, Local.Direction dir)
		 {
			 remote.move(ident, dir);
		 }

		 private ushort _join(IntPtr pl)
		 {
			 // create a proper C#-object from this pointer
			 // using the apropriate constructer provided by 
			 // the swig-wrapper and use it as parameter to 
			 // the join method of this class
			 return join(new Local.XxsMazeBot(pl, false));
		 }

		 private void _move(ushort ident, int dir)
		 {
			 // you must convert this int explicitly to Direction
			 // to call move
			 move(ident, (Local.Direction)dir);
		 }
						 
		public HandleRef getCPtr()
		{
			// create the appropriate delegates and use them to create
			// the native side proxy. Assure that you keep a reference
			// to the proxy because of the garbage collector problem.
			if(proxy == null)
			{
				joinDel = new JoinDelegate(this._join);
				leaveDel = new LeaveDelegate(this.leave);
				moveDel = new MoveDelegate(this._move);

				proxy = new Local.MonoXxsMazeProxy(joinDel, leaveDel, moveDel);
			}

			return Local.MonoXxsMazeProxy.getCPtr(proxy);
		}
		
		private Remote.IxxsMaze remote;
		private Local.MonoXxsMazeProxy proxy;
		// the function pointers(i.e. instances of delegates) belong
		// here
		private JoinDelegate joinDel;
		private LeaveDelegate leaveDel;
		private MoveDelegate moveDel;

	};

}
