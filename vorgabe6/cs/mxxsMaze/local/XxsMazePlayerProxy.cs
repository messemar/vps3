using System;
using System.Runtime.Remoting;
using System.Runtime.InteropServices;

// declare the apropriate delegates for the three methods of the
// IPlayer interface
public delegate void StartGameDelegate(IntPtr ptr);
public delegate void UpdateDelegate(IntPtr ptr);
public delegate void FinishDelegate();
namespace Local {

/* For the purpose of this class see XxsMazeProxy. This class
 * is the correponding counterpart for the player side of
 * the game
 */
public class XxsMazePlayerProxy : Local.IPlayer {
	
	public XxsMazePlayerProxy(Remote.IPlayer r) 
	{
		// save a reference to the remote game
        remote=r;
        proxy = null;
	}

	public void startGame(Local.PlayerPositions pos) 
	{
		remote.startGame(pos);
	}
		
	public void update(Local.PlayerPositions pos) 
	{
		remote.update(pos);
	}

	public void finish()
	{
		remote.finish();
	} 

	public void _startGame(IntPtr ptr) 
	{
		// create a proper C#-object from this pointer
        Local.PlayerPositions ppos= new Local.PlayerPositions(ptr,false); 
		// using the apropriate constructer provided by 
        startGame(ppos);

		// the swig-wrapper and use it as parameter to 
		// the startGame method of this class
	}
		
	public void _update(IntPtr ptr) 
	{
		// create a proper C#-object from this pointer
		Local.PlayerPositions ppos= new Local.PlayerPositions(ptr,false);
        // using the apropriate constructer provided by 
		// the swig-wrapper and use it as parameter to 
        update(ppos);
		// the update method of this class
	}

	public HandleRef getCPtr()
	{
		// create the appropriate delegates and use them to create
		// the native side proxy. Assure that you keep a reference
		// to the proxy because of the garbage collector problem.
        if(proxy==null){
            startGame_= new StartGameDelegate(this._startGame);
            update_=new UpdateDelegate(this._update);
            finish_=new FinishDelegate(this.finish);
            proxy= new MonoPlayerProxy(startGame_, update_, finish_);
        }
    
        return Local.MonoPlayerProxy.getCPtr(proxy);
	}

	private Remote.IPlayer remote;
	private Local.MonoPlayerProxy proxy;
	// the function pointers(i.e. instances of delegates) belong
	// here
    private StartGameDelegate startGame_;
    private UpdateDelegate update_;
    private FinishDelegate finish_;
};

}
