%module xxsMaze
%{
#include <vector>
#include <map>
#include <iostream>
#include"types/Direction.h"
#include"types/PlayerPositions.h"
#include"IxxsMaze.h"
#include"IPlayer.h"
#include"XxsMazeMap.h"
#include"XxsMazeGUI.h"
#include"XxsMazeBot.h"
#include"XxsMaze.h"
#include"MonoXxsMazeProxy.h"
#include"MonoPlayerProxy.h"
%}


%include"types/PlayerId.h"
%include"types/Direction.h"
%include"types/Point.h"

%typemap(csimports) PlayerPositions %{ 
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.Serialization; 
%}

%typemap(csattributes) PlayerPositions "[Serializable]"
%typemap(csinterfaces) PlayerPositions "IDisposable, ISerializable"
%typemap(csbody) PlayerPositions %{
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal $csclassname(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr($csclassname obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

	public virtual void GetObjectData(SerializationInfo s, StreamingContext c) 
	{
		int size = this.numPlayers();
		s.AddValue("Size", size);
		for (ushort i=0; i < size; i++) {
			Point p = this.getPos(i);
			s.AddValue("Point_" + i + "_x", p.x);  
			s.AddValue("Point_" + i + "_y", p.y);  
		}
	}

	private PlayerPositions(SerializationInfo s, StreamingContext c) : 
		this(xxsMazePINVOKE.new_PlayerPositions(), true)
	{
		int size = s.GetInt32("Size");
		for (ushort i=0; i < size; i++) {
			uint x = s.GetUInt32("Point_" + i + "_x");
			uint y = s.GetUInt32("Point_" + i + "_y");
			this.setPos(i, new Point(x,y));
		}
	}
	
%}


%typemap(imtype) void* sg "StartGameDelegate"
%typemap(cstype) void* sg "StartGameDelegate"
%typemap(csin) void* sg "$csinput"

%typemap(imtype) void* up "UpdateDelegate"
%typemap(cstype) void* up "UpdateDelegate"
%typemap(csin) void* up "$csinput"

%typemap(imtype) void* fi "FinishDelegate"
%typemap(cstype) void* fi "FinishDelegate"
%typemap(csin) void* fi "$csinput"

%typemap(imtype) void* jo "JoinDelegate"
%typemap(cstype) void* jo "JoinDelegate"
%typemap(csin) void* jo "$csinput"

%typemap(imtype) void* lv "LeaveDelegate"
%typemap(cstype) void* lv "LeaveDelegate"
%typemap(csin) void* lv "$csinput"

%typemap(imtype) void* mv "MoveDelegate"
%typemap(cstype) void* mv "MoveDelegate"
%typemap(csin) void* mv "$csinput"

%include"types/PlayerPositions.h"

%typemap(csimports) XxsMazeMap %{ 
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
%}

%csattributes XxsMazeMap::setPlayerPos(const PlayerId& id, const Point& pos) "[MethodImplAttribute(MethodImplOptions.Synchronized)]"
%csattributes XxsMazeMap::setAllPos(const PlayerPositions& positions) "[MethodImplAttribute(MethodImplOptions.Synchronized)]"
%csattributes XxsMazeMap::move(const PlayerId& id, const Direction& dir) "[MethodImplAttribute(MethodImplOptions.Synchronized)]"
%csattributes XxsMazeMap::removePlayer(const PlayerId& id) "[MethodImplAttribute(MethodImplOptions.Synchronized)]"

%include"XxsMazeMap.h"
%include"XxsMazeGUI.h"

class MonoPlayerProxy {
	public:

		MonoPlayerProxy(void* sg, void* up, void* fi);
};

class MonoXxsMazeProxy {
	public:
		MonoXxsMazeProxy(void* jo, void* lv, void* mv);
};

%typemap(csinterfaces) XxsMaze "IDisposable, Local.IxxsMaze"
%typemap(csbody) XxsMaze %{
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal $csclassname(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef _getCPtr($csclassname obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

	public HandleRef getCPtr() {
		return _getCPtr(this);
	}
%}

%typemap(csinterfaces) XxsMazeBot "IDisposable, Local.IPlayer"
%typemap(csbody) XxsMazeBot %{
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal $csclassname(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef _getCPtr($csclassname obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

	public HandleRef getCPtr() {
		return _getCPtr(this);
	}
%}


%typemap(cstype) IPlayer* "Local.IPlayer"
%typemap(csin) IPlayer* "$csinput.getCPtr()"
%typemap(cstype) IxxsMaze* "Local.IxxsMaze"
%typemap(csin) IxxsMaze* "$csinput.getCPtr()"
%typemap(csin) XxsMaze* "$csinput.getCPtr()"
%typemap(csin) XxsMazeBot* "$csinput.getCPtr()"
%typemap(csin) XxsMazeBot& "$csinput.getCPtr()"


//States the server can be in
typedef enum {LOGON,PRE_GAME,IN_GAME,FINISH} GameState;

class XxsMaze {
public:
	XxsMaze(XxsMazeMap*map, const short m) 
		: state(LOGON), max(m), playerCount(0), ready(false), map(*map) {}

	~XxsMaze() {}
	
	//methods of IxxsMaze interface
	//-----------------------------------
	PlayerId join(IPlayer* pl);

	void leave(PlayerId ident);

	void move(PlayerId ident, Direction dir);

	//-------------------------------------
	
	Point getPos(const PlayerId& id);
	
	void setPos(const PlayerId& id, const Point pos);
	
	bool isFree(const Point pos);

	const XxsMazeMap* getMap() const;

	GameState getState();

	bool isReady();

	void start();

	void sendUpdate();
};


// States the Player can be in
typedef enum { START_WAIT, GAMEING, WAIT4UPDATE, FINISHED } PlayerState;

class XxsMazeBot {
	// shorter writing ;)
	typedef std::vector< std::vector< unsigned int > > StepMap;
	typedef std::vector< Direction > Way;

public:

	explicit XxsMazeBot(XxsMazeBot* other) : IPlayer(), 
		state(START_WAIT),
		myGame((XxsMaze*)other->getGame()), ui(other->getMap()), myName(other->getName())	{}

	XxsMazeBot(IxxsMaze* game, const char* filename, const char* name): IPlayer(), 
		state(START_WAIT),
		myGame((XxsMaze*)game), ui(filename), myName(name) {}
	
	virtual ~XxsMazeBot(){}

	// the methods of the interface
	// -------------------------------------------------------------
	void startGame(const PlayerPositions pos);
	
	void update(const PlayerPositions pos);
	
	void finish();
	// --------------------------------------------------------------

	String getName() { return myName; }

	const XxsMazeMap& getMap() const { return ui.getMap(); }

	PlayerState getState() { return state; }

	void calcWay(const Point& source, const Point& dest);

	void startup();

	void makeStep();

	bool reachedGoal () const;


private:
	// the current state of this robot
	PlayerState state;
	// the id of this robot
	PlayerId myId;
	// the game in which this robot participates
	IxxsMaze* myGame;
	// the graphical user interface
	XxsMazeGUI ui;
	// the name of this robot
	String myName;
	// the calculated way
	Way way;
	// the goal to reach
	Point goal;
};
