/** @file XxsMaze.h
 * Contains the definition of XxsMaze
 */

#ifndef XxsMaze_h
#define XxsMaze_h

#include <iostream>
#include <cassert>
#include"types/String.h"
#include"types/Direction.h"
#include"types/PlayerList.h"
#include"IxxsMaze.h"
#include"XxsMazeMap.h"

/** This is the implementation of the game interface.
 * It implemets IxxsMaze and defines additional methods
 * especially for control of the game from a thread 
 * or process.
 */
class XxsMaze : public IxxsMaze {
public:
	XxsMaze(XxsMazeMap*map, const short m) 
		: state(LOGON), max(m), playerCount(0), ready(false), map(*map) 
		{
			inBarrier[0] = new bool[m];
			for (int i=0; i<m; i++) {
				inBarrier[i] = false;
			}
		}

	~XxsMaze() {}
	
	//methods of IxxsMaze interface
	//-----------------------------------
	virtual PlayerId join(IPlayer* pl);

	virtual void leave(PlayerId ident);

	virtual void move(PlayerId ident, Direction dir);

	//-------------------------------------
	
	/** Returns the positions of player with Id id.
	 *
	 * @param id the id of the player from which the 
	 *  			position is requested
	 * @return the requested position 
	 */
	Point getPos(const PlayerId& id)
	{
		return map.getPlayerPos(id);
	}
	
	/** Sets the given position for the player with
	 * the given id.
	 * 
	 * @param id the id of the player 
	 * @param pos the new position
	 */
	void setPos(const PlayerId& id, const Point pos)
	{
		map.setPlayerPos(id,pos);
	}
	
	/** Indicates if pos is occupied by a wall or player.
	 *
	 * @param pos the position to be tested
	 * @return true if pos is free false otherwise
	 */
	bool isFree(const Point pos)
	{
		return map.isFree(pos);
	}

	/** Returns the game map
	 *
	 * @return a pointer to the game map
	 */
	const XxsMazeMap* getMap() const
	{
		return &map;
	}
	/**
	 *
	 */
	GameState getState(){return state;}
	//void setState(GameState s){state = s;}

	/** Tests if all players have connected already.
	 *
	 * @return true if all players have connected 
	 * 				 false otherwise
	 */
	bool isReady(){return playerCount == max;};

	/** Initiates game start by sending relevant
	 * information to all players.
	 *
	 */
	void start()
	{
		std::cerr<<"starting game and ..."<<std::endl;
		//send playerlist to all clients
		std::cerr<<"... sending player list to all clients"<<std::endl;
		for(unsigned long i=0; i < playerCount; i++){
		std::cerr<<"sending to"<< players[i] <<std::endl;
			players[i]->update(*(map.getAllPos()));
		}
		std::cerr<<"sent to all players"<<std::endl;
		state = IN_GAME;
	}

	/** Updates all players by sending all relevant 
	 * information to them.
	 *
	 */
	void sendUpdate()
	{
		//send playerlist to all clients
		//std::cerr<<"C++ --- sendUpdate called!"<<std::endl;
		for(unsigned long i=0; i < playerCount; i++){
			assert(players[i] != NULL);
		//std::cerr<<"C++ --- sendUpdate players[" << i << "]: " << players[i] <<std::endl;
			players[i]->update(*(map.getAllPos()));
		}
	}

private:
	void barrier(PlayerId id) {
		while(inBarrier[id]); 		
		inBarrier[id] = true;
		bool release = false;
		while(!release) {
			bool tmp = true;
			for(int i=0; i<max; i++)
				tmp = tmp && inBarrier[i];
			release = tmp;
		}
		
		for(int i=0; i<max; i++)
			inBarrier[i] = false;
	}
		

	
	//States the server can be in
	volatile GameState state;
	//Number of clients which will join us
	unsigned short max;
	//Number of clients that joined us so far
	unsigned short playerCount;
	//Flag if all clients have logged on AND received their gamedata
	bool ready;

	volatile bool inBarrier[];
	//mapdata
	XxsMazeMap map;	
	//List of Players
	PlayerList players;
};
#endif
