#ifndef PlayerProxy_h
#define PlayerProxy_h

#include "types/PlayerPositions.h"

class PlayerProxy : IPlayer {
public:
	virtual ~PlayerProxy() {}

	PlayerProxy(IPlayer* o) : other(o) {}

	virtual void startGame(const PlayerPositions pos)
	{
		other->startGame(pos);
	}

	virtual void update(const PlayerPositions pos)
	{
		other->update(pos);
	}

	virtual void finish()
	{
		other->finish();
	}

private:
	IPlayer* other;
};
#endif
