/**
 * @file XxsMazeClient.h
 */
#ifndef XxsMazeBotClient_h
#define XxsMazeBotClient_h

#include "XxsMazeBot.h"
#include "XxsMaze.h"

/** A thread which controls a XxsMazeBot.
 *
 */
class XxsMazeBotClient : {
	public:
		/** Contructor from pointer to XxsMazeBot.
		 *
		 */
		explicit XxsMazeBotClient(XxsMazeBot* p)  
			: state(START_WAIT), myBot(p), game(p->getGame()) {}

		/** Contructor from reference to XxsMazeBot.
		 *
		 */
		explicit XxsMazeBotClient(XxsMazeBot& p) 
			: state(START_WAIT), myBot(&p), game(p.getGame()) {}

		virtual ~XxsMazeBotClient() 
		{
			join();
		}

		/** The main method of the client implementing Thread::run.
		 *
		 */
		virtual void run();
		 
	private:
		PlayerState state;
		XxsMazeBot* myBot;
		IxxsMaze* game;
};
#endif
