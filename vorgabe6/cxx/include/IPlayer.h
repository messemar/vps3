#ifndef IxxsPlayer_h
#define IxxsPlayer_h

#include "types/PlayerPositions.h"

// States the Player can be in
typedef enum { START_WAIT, GAMEING, WAIT4UPDATE, FINISHED } PlayerState;

class IPlayer {
public:
	virtual ~IPlayer() {}
	
	/** This Method is called by the game, when all playeres have signaled
	 * 	to be ready for starting.
	 *
	 * @param PlayerPositions the position data of all players on the map
	 */
	virtual void startGame(const PlayerPositions) = 0;

	/** This Method is called by the game, whenever there is new game state
	 * 	available.
	 *
	 * @param PlayerPositions the position data of all players on the map
	 */
	virtual void update(const PlayerPositions) = 0;

	/** This method is called by the game to indicate the end of the game
	 * 	to a player.
	 */
	virtual void finish() = 0;
};
#endif
