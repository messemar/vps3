#ifndef MonoXxsMazeProxy_h
#define MonoXxsMazeProxy_h

#include <assert.h>
#include "types/PlayerId.h"
#include "IxxsMaze.h"

class MonoXxsMazeProxy;

extern "C" {
typedef PlayerId (*JOIN)(void*);
typedef void (*LEAVE)(PlayerId);
typedef void (*MOVE)(PlayerId, int);

//PlayerId join(MonoXxsMazeProxy* p, IPlayer* pl);

}

class MonoXxsMazeProxy : public IxxsMaze {
	public:
		MonoXxsMazeProxy(void* jo, void* lv, void* mv) :
			fpJoin((JOIN)jo),
			fpLeave((LEAVE)lv),
			fpMove((MOVE)mv)
		{
			cout << "C++ --- MonoXxsMazeProxy::MonoXxsMazeProxy called" << this << endl;
		}

		virtual PlayerId join(IPlayer* pl)
		{
			cout << "C++ --- MonoXxsMazeProxy::join called! " << pl << " " << fpJoin << endl;
			PlayerId ret = (fpJoin)(pl);
			//PlayerId ret = ::join(this,pl);
			cerr << "C++ --- MonoXxsMazeProxy::join result is: " << ret << endl;
			//return (fpJoin)(pl);
			return ret;
		}

		virtual void leave(PlayerId ident)
		{
			(fpLeave)(ident);
		}

		virtual void move(PlayerId id, Direction dir)
		{
			(fpMove)(id,(int)dir);
		}
	//private:
		JOIN fpJoin;
		LEAVE fpLeave;
		MOVE fpMove;
};

/*extern "C" {
	PlayerId join(MonoXxsMazeProxy* p, IPlayer* pl) {
		return (p->fpJoin)(pl);
	}
}
*/
#endif
