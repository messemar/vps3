#ifndef Monitior_h
#define Monitior_h

#include "sync/Lockable.h"

template <class T>
class Monitor {
	public:
		
		explicit Monitor(T* prot) : protectee(prot) {}

		void setProtectee(T* prot) {
			protectee.setPtr(prot);
		}

		T* getProtectee()
		{
			return protectee.getPtr();
		}

		Lockable<T> operator->() { return protectee; } 

	private:
		Lockable<T> protectee;
};
#endif
