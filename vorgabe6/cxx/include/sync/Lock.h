#ifndef Lock_h
#define Lock_h

#include "sync/Mutex.h"

class Lock {
	public:
		Lock(const Lock& other)
		{
			mutex = other.mutex;
			mutex->lock();
		}
		
		explicit Lock(Mutex* m)
		{
			mutex = m;
			mutex->lock();
		}

		explicit Lock(Mutex& m)
		{
			mutex = &m;
			mutex.lock();
		}

		~Lock() { mutex->unlock(); }

	private:
		Mutex* mutex;
};
#endif
