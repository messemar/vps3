#ifndef Mutex_h
#define Mutex_h

#include <pthread.h>

class Mutex {
	public:

		Mutex() 
		{	
			pthread_mutexattr_init(&attr);
			//pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP);
			pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_FAST_NP);
			pthread_mutex_init(&mutex, &attr); 
		}
 
		void lock() 
		{ 
			//std::cerr << "Mutex::lock" << std::endl;
			pthread_mutex_lock(&mutex); 
		}

		void unlock() 
		{ 
			//std::cerr << "Mutex::unlock" << std::endl;
			pthread_mutex_unlock(&mutex); 
		}

	private:
		pthread_mutexattr_t attr;
		pthread_mutex_t mutex;
};
#endif
