#ifndef MonoPlayerProxy_h
#define MonoPlayerProxy_h

#include "types/PlayerPositions.h"

typedef void (*START_GAME)(void*);
typedef void (*UPDATE)(void*);
typedef void (*MFINISH)();

class MonoPlayerProxy : public IPlayer {
	public:

		MonoPlayerProxy(void* sg, void* up, void* fi) : 
			fpStartGame((START_GAME)sg),
			fpUpdate((UPDATE)up),
			fpFinish((MFINISH)fi)
		{
			//cerr << "C++ --- MonoPlayerProxy cstr called: " << this << endl;
		}

		void startGame(PlayerPositions pos) 
		{
			(fpStartGame)(&pos);
		}

		void update(PlayerPositions pos) 
		{
			//cerr << "C++ --- MonoPlayerProxy::update called!" << endl;
			(fpUpdate)(&pos);
		}

		void finish() 
		{
			(fpFinish)();
		}
		
	private:
	 START_GAME fpStartGame;
	 UPDATE fpUpdate;
	 MFINISH fpFinish;
};

#endif
