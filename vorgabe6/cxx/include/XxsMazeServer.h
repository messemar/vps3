/** Defines XxsMazeServer.
 * @ file XxsMazeServer.h
 */
#ifndef XxsMazeServer_h
#define XxsMazeServer_h

#include "XxsMaze.h"

/** A thread which controls a XxsMaze game.
 *
 */
class XxsMazeServer : {
	public:
		/** Constructor from pointer to XxsMaze.
		 *
		 */
		explicit XxsMazeServer(XxsMaze* p)  : state(LOGON), game(p) {}
		/** Constructor from reference to XxsMaze.
		 *
		 */
		explicit XxsMazeServer(XxsMaze& p) : state(LOGON), game(&p) {}

		virtual ~XxsMazeServer()
		{
			join();
		}

		/** The main method of the server overriding Thread::run
		 *
		 */
		virtual void run();
		
	private:
		GameState state;
		XxsMaze* game;
};
#endif
