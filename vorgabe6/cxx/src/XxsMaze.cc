#include "XxsMaze.h"
#include <cstdlib>

PlayerId XxsMaze::join(IPlayer* pl) 
{
	if (state != LOGON) {
		std::cerr << "Game is in Progress, no logon possible!" << std::endl;
		return 4711;
	}
	// add player to list
	players.add(playerCount,pl);
	// generate initial start point for the player
	Point p;
	do {
		p.x = random()%map.width();
		p.y = random()%map.height();
	} while (!map.isFree(p));
	
	// update map
	map.setPlayerPos(playerCount,p);

	if ((unsigned int)playerCount+1 == max) {
		std::cerr << "all clients have logged on" << std::endl << std::flush;
		state = PRE_GAME;
	}
	
	return playerCount++; // so the id is ok for indexing the arrays
}


void XxsMaze::move(PlayerId player, Direction direction) {
	//std::cerr << "C++ --- XxsMaze::move called!" << std::endl;
	if (state == IN_GAME) {
		map.move(player, direction);
	} else {
		std::cerr << "game not started yet!!" << std::endl;
	}
}


void XxsMaze::leave(PlayerId player) {
	//std::cerr<<"logout[ ID: "<<player<<" ]"<<std::endl;
	players[player]->finish();
	players.remove(player);
}
