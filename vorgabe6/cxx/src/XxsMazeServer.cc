#include "XxsMazeServer.h"

void XxsMazeServer::run() {
	GameState state;

	// here comes the main-loop
	do {
		state = game->getState();		
		switch(state) {
			case PRE_GAME:
				// clients have to be informed of gamestart
				game->start();
				std::cerr << "game started" << std::endl;
				break;
			case IN_GAME:
				// send new position informations to clients
				game->sendUpdate();
				break;
			default:
				break;
		}
		// here the server determines the round time.
		// in this case it is 50ms.
		// in general this time should be quite bigger 
		// than the time the client waits to make sure 
		// the client wont lose server packets!
		usleep(50000);
	}	while (state != FINISH);

	// bye bye...
	std::cerr<<"reached FINISH-state"<<std::endl;
}
