/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.8
 *
 * This file is not intended to be easily readable and contains a number of
 * coding conventions designed to improve portability and efficiency. Do not make
 * changes to this file unless you know what you are doing--modify the SWIG
 * interface file instead.
 * ----------------------------------------------------------------------------- */


#ifndef SWIGCSHARP
#define SWIGCSHARP
#endif



#ifdef __cplusplus
/* SwigValueWrapper is described in swig.swg */
template<typename T> class SwigValueWrapper {
  struct SwigMovePointer {
    T *ptr;
    SwigMovePointer(T *p) : ptr(p) { }
    ~SwigMovePointer() { delete ptr; }
    SwigMovePointer& operator=(SwigMovePointer& rhs) { T* oldptr = ptr; ptr = 0; delete oldptr; ptr = rhs.ptr; rhs.ptr = 0; return *this; }
  } pointer;
  SwigValueWrapper& operator=(const SwigValueWrapper<T>& rhs);
  SwigValueWrapper(const SwigValueWrapper<T>& rhs);
public:
  SwigValueWrapper() : pointer(0) { }
  SwigValueWrapper& operator=(const T& t) { SwigMovePointer tmp(new T(t)); pointer = tmp; return *this; }
  operator T&() const { return *pointer.ptr; }
  T *operator&() { return pointer.ptr; }
};

template <typename T> T SwigValueInit() {
  return T();
}
#endif

/* -----------------------------------------------------------------------------
 *  This section contains generic SWIG labels for method/variable
 *  declarations/attributes, and other compiler dependent labels.
 * ----------------------------------------------------------------------------- */

/* template workaround for compilers that cannot correctly implement the C++ standard */
#ifndef SWIGTEMPLATEDISAMBIGUATOR
# if defined(__SUNPRO_CC) && (__SUNPRO_CC <= 0x560)
#  define SWIGTEMPLATEDISAMBIGUATOR template
# elif defined(__HP_aCC)
/* Needed even with `aCC -AA' when `aCC -V' reports HP ANSI C++ B3910B A.03.55 */
/* If we find a maximum version that requires this, the test would be __HP_aCC <= 35500 for A.03.55 */
#  define SWIGTEMPLATEDISAMBIGUATOR template
# else
#  define SWIGTEMPLATEDISAMBIGUATOR
# endif
#endif

/* inline attribute */
#ifndef SWIGINLINE
# if defined(__cplusplus) || (defined(__GNUC__) && !defined(__STRICT_ANSI__))
#   define SWIGINLINE inline
# else
#   define SWIGINLINE
# endif
#endif

/* attribute recognised by some compilers to avoid 'unused' warnings */
#ifndef SWIGUNUSED
# if defined(__GNUC__)
#   if !(defined(__cplusplus)) || (__GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 4))
#     define SWIGUNUSED __attribute__ ((__unused__))
#   else
#     define SWIGUNUSED
#   endif
# elif defined(__ICC)
#   define SWIGUNUSED __attribute__ ((__unused__))
# else
#   define SWIGUNUSED
# endif
#endif

#ifndef SWIG_MSC_UNSUPPRESS_4505
# if defined(_MSC_VER)
#   pragma warning(disable : 4505) /* unreferenced local function has been removed */
# endif
#endif

#ifndef SWIGUNUSEDPARM
# ifdef __cplusplus
#   define SWIGUNUSEDPARM(p)
# else
#   define SWIGUNUSEDPARM(p) p SWIGUNUSED
# endif
#endif

/* internal SWIG method */
#ifndef SWIGINTERN
# define SWIGINTERN static SWIGUNUSED
#endif

/* internal inline SWIG method */
#ifndef SWIGINTERNINLINE
# define SWIGINTERNINLINE SWIGINTERN SWIGINLINE
#endif

/* exporting methods */
#if (__GNUC__ >= 4) || (__GNUC__ == 3 && __GNUC_MINOR__ >= 4)
#  ifndef GCC_HASCLASSVISIBILITY
#    define GCC_HASCLASSVISIBILITY
#  endif
#endif

#ifndef SWIGEXPORT
# if defined(_WIN32) || defined(__WIN32__) || defined(__CYGWIN__)
#   if defined(STATIC_LINKED)
#     define SWIGEXPORT
#   else
#     define SWIGEXPORT __declspec(dllexport)
#   endif
# else
#   if defined(__GNUC__) && defined(GCC_HASCLASSVISIBILITY)
#     define SWIGEXPORT __attribute__ ((visibility("default")))
#   else
#     define SWIGEXPORT
#   endif
# endif
#endif

/* calling conventions for Windows */
#ifndef SWIGSTDCALL
# if defined(_WIN32) || defined(__WIN32__) || defined(__CYGWIN__)
#   define SWIGSTDCALL __stdcall
# else
#   define SWIGSTDCALL
# endif
#endif

/* Deal with Microsoft's attempt at deprecating C standard runtime functions */
#if !defined(SWIG_NO_CRT_SECURE_NO_DEPRECATE) && defined(_MSC_VER) && !defined(_CRT_SECURE_NO_DEPRECATE)
# define _CRT_SECURE_NO_DEPRECATE
#endif

/* Deal with Microsoft's attempt at deprecating methods in the standard C++ library */
#if !defined(SWIG_NO_SCL_SECURE_NO_DEPRECATE) && defined(_MSC_VER) && !defined(_SCL_SECURE_NO_DEPRECATE)
# define _SCL_SECURE_NO_DEPRECATE
#endif

/* Deal with Apple's deprecated 'AssertMacros.h' from Carbon-framework */
#if defined(__APPLE__) && !defined(__ASSERT_MACROS_DEFINE_VERSIONS_WITHOUT_UNDERSCORES)
# define __ASSERT_MACROS_DEFINE_VERSIONS_WITHOUT_UNDERSCORES 0
#endif

/* Intel's compiler complains if a variable which was never initialised is
 * cast to void, which is a common idiom which we use to indicate that we
 * are aware a variable isn't used.  So we just silence that warning.
 * See: https://github.com/swig/swig/issues/192 for more discussion.
 */
#ifdef __INTEL_COMPILER
# pragma warning disable 592
#endif


#include <stdlib.h>
#include <string.h>
#include <stdio.h>


/* Support for throwing C# exceptions from C/C++. There are two types: 
 * Exceptions that take a message and ArgumentExceptions that take a message and a parameter name. */
typedef enum {
  SWIG_CSharpApplicationException,
  SWIG_CSharpArithmeticException,
  SWIG_CSharpDivideByZeroException,
  SWIG_CSharpIndexOutOfRangeException,
  SWIG_CSharpInvalidCastException,
  SWIG_CSharpInvalidOperationException,
  SWIG_CSharpIOException,
  SWIG_CSharpNullReferenceException,
  SWIG_CSharpOutOfMemoryException,
  SWIG_CSharpOverflowException,
  SWIG_CSharpSystemException
} SWIG_CSharpExceptionCodes;

typedef enum {
  SWIG_CSharpArgumentException,
  SWIG_CSharpArgumentNullException,
  SWIG_CSharpArgumentOutOfRangeException
} SWIG_CSharpExceptionArgumentCodes;

typedef void (SWIGSTDCALL* SWIG_CSharpExceptionCallback_t)(const char *);
typedef void (SWIGSTDCALL* SWIG_CSharpExceptionArgumentCallback_t)(const char *, const char *);

typedef struct {
  SWIG_CSharpExceptionCodes code;
  SWIG_CSharpExceptionCallback_t callback;
} SWIG_CSharpException_t;

typedef struct {
  SWIG_CSharpExceptionArgumentCodes code;
  SWIG_CSharpExceptionArgumentCallback_t callback;
} SWIG_CSharpExceptionArgument_t;

static SWIG_CSharpException_t SWIG_csharp_exceptions[] = {
  { SWIG_CSharpApplicationException, NULL },
  { SWIG_CSharpArithmeticException, NULL },
  { SWIG_CSharpDivideByZeroException, NULL },
  { SWIG_CSharpIndexOutOfRangeException, NULL },
  { SWIG_CSharpInvalidCastException, NULL },
  { SWIG_CSharpInvalidOperationException, NULL },
  { SWIG_CSharpIOException, NULL },
  { SWIG_CSharpNullReferenceException, NULL },
  { SWIG_CSharpOutOfMemoryException, NULL },
  { SWIG_CSharpOverflowException, NULL },
  { SWIG_CSharpSystemException, NULL }
};

static SWIG_CSharpExceptionArgument_t SWIG_csharp_exceptions_argument[] = {
  { SWIG_CSharpArgumentException, NULL },
  { SWIG_CSharpArgumentNullException, NULL },
  { SWIG_CSharpArgumentOutOfRangeException, NULL }
};

static void SWIGUNUSED SWIG_CSharpSetPendingException(SWIG_CSharpExceptionCodes code, const char *msg) {
  SWIG_CSharpExceptionCallback_t callback = SWIG_csharp_exceptions[SWIG_CSharpApplicationException].callback;
  if ((size_t)code < sizeof(SWIG_csharp_exceptions)/sizeof(SWIG_CSharpException_t)) {
    callback = SWIG_csharp_exceptions[code].callback;
  }
  callback(msg);
}

static void SWIGUNUSED SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpExceptionArgumentCodes code, const char *msg, const char *param_name) {
  SWIG_CSharpExceptionArgumentCallback_t callback = SWIG_csharp_exceptions_argument[SWIG_CSharpArgumentException].callback;
  if ((size_t)code < sizeof(SWIG_csharp_exceptions_argument)/sizeof(SWIG_CSharpExceptionArgument_t)) {
    callback = SWIG_csharp_exceptions_argument[code].callback;
  }
  callback(msg, param_name);
}


#ifdef __cplusplus
extern "C" 
#endif
SWIGEXPORT void SWIGSTDCALL SWIGRegisterExceptionCallbacks_xxsMaze(
                                                SWIG_CSharpExceptionCallback_t applicationCallback,
                                                SWIG_CSharpExceptionCallback_t arithmeticCallback,
                                                SWIG_CSharpExceptionCallback_t divideByZeroCallback, 
                                                SWIG_CSharpExceptionCallback_t indexOutOfRangeCallback, 
                                                SWIG_CSharpExceptionCallback_t invalidCastCallback,
                                                SWIG_CSharpExceptionCallback_t invalidOperationCallback,
                                                SWIG_CSharpExceptionCallback_t ioCallback,
                                                SWIG_CSharpExceptionCallback_t nullReferenceCallback,
                                                SWIG_CSharpExceptionCallback_t outOfMemoryCallback, 
                                                SWIG_CSharpExceptionCallback_t overflowCallback, 
                                                SWIG_CSharpExceptionCallback_t systemCallback) {
  SWIG_csharp_exceptions[SWIG_CSharpApplicationException].callback = applicationCallback;
  SWIG_csharp_exceptions[SWIG_CSharpArithmeticException].callback = arithmeticCallback;
  SWIG_csharp_exceptions[SWIG_CSharpDivideByZeroException].callback = divideByZeroCallback;
  SWIG_csharp_exceptions[SWIG_CSharpIndexOutOfRangeException].callback = indexOutOfRangeCallback;
  SWIG_csharp_exceptions[SWIG_CSharpInvalidCastException].callback = invalidCastCallback;
  SWIG_csharp_exceptions[SWIG_CSharpInvalidOperationException].callback = invalidOperationCallback;
  SWIG_csharp_exceptions[SWIG_CSharpIOException].callback = ioCallback;
  SWIG_csharp_exceptions[SWIG_CSharpNullReferenceException].callback = nullReferenceCallback;
  SWIG_csharp_exceptions[SWIG_CSharpOutOfMemoryException].callback = outOfMemoryCallback;
  SWIG_csharp_exceptions[SWIG_CSharpOverflowException].callback = overflowCallback;
  SWIG_csharp_exceptions[SWIG_CSharpSystemException].callback = systemCallback;
}

#ifdef __cplusplus
extern "C" 
#endif
SWIGEXPORT void SWIGSTDCALL SWIGRegisterExceptionArgumentCallbacks_xxsMaze(
                                                SWIG_CSharpExceptionArgumentCallback_t argumentCallback,
                                                SWIG_CSharpExceptionArgumentCallback_t argumentNullCallback,
                                                SWIG_CSharpExceptionArgumentCallback_t argumentOutOfRangeCallback) {
  SWIG_csharp_exceptions_argument[SWIG_CSharpArgumentException].callback = argumentCallback;
  SWIG_csharp_exceptions_argument[SWIG_CSharpArgumentNullException].callback = argumentNullCallback;
  SWIG_csharp_exceptions_argument[SWIG_CSharpArgumentOutOfRangeException].callback = argumentOutOfRangeCallback;
}


/* Callback for returning strings to C# without leaking memory */
typedef char * (SWIGSTDCALL* SWIG_CSharpStringHelperCallback)(const char *);
static SWIG_CSharpStringHelperCallback SWIG_csharp_string_callback = NULL;


#ifdef __cplusplus
extern "C" 
#endif
SWIGEXPORT void SWIGSTDCALL SWIGRegisterStringCallback_xxsMaze(SWIG_CSharpStringHelperCallback callback) {
  SWIG_csharp_string_callback = callback;
}


/* Contract support */

#define SWIG_contract_assert(nullreturn, expr, msg) if (!(expr)) {SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentOutOfRangeException, msg, ""); return nullreturn; } else


#include <vector>
#include <map>
#include <iostream>
#include"types/Direction.h"
#include"types/PlayerPositions.h"
#include"IxxsMaze.h"
#include"IPlayer.h"
#include"XxsMazeMap.h"
#include"XxsMazeGUI.h"
#include"XxsMazeBot.h"
#include"XxsMaze.h"
#include"MonoXxsMazeProxy.h"
#include"MonoPlayerProxy.h"


#ifdef __cplusplus
extern "C" {
#endif

SWIGEXPORT void * SWIGSTDCALL CSharp_new_Point__SWIG_0() {
  void * jresult ;
  Point *result = 0 ;
  
  result = (Point *)new Point();
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_Point__SWIG_1(unsigned long jarg1, unsigned long jarg2) {
  void * jresult ;
  Point::coord_type arg1 ;
  Point::coord_type arg2 ;
  Point *result = 0 ;
  
  arg1 = (Point::coord_type)jarg1; 
  arg2 = (Point::coord_type)jarg2; 
  result = (Point *)new Point(arg1,arg2);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_Point_x_set(void * jarg1, unsigned long jarg2) {
  Point *arg1 = (Point *) 0 ;
  Point::coord_type arg2 ;
  
  arg1 = (Point *)jarg1; 
  arg2 = (Point::coord_type)jarg2; 
  if (arg1) (arg1)->x = arg2;
}


SWIGEXPORT unsigned long SWIGSTDCALL CSharp_Point_x_get(void * jarg1) {
  unsigned long jresult ;
  Point *arg1 = (Point *) 0 ;
  Point::coord_type result;
  
  arg1 = (Point *)jarg1; 
  result = (Point::coord_type) ((arg1)->x);
  jresult = (unsigned long)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_Point_y_set(void * jarg1, unsigned long jarg2) {
  Point *arg1 = (Point *) 0 ;
  Point::coord_type arg2 ;
  
  arg1 = (Point *)jarg1; 
  arg2 = (Point::coord_type)jarg2; 
  if (arg1) (arg1)->y = arg2;
}


SWIGEXPORT unsigned long SWIGSTDCALL CSharp_Point_y_get(void * jarg1) {
  unsigned long jresult ;
  Point *arg1 = (Point *) 0 ;
  Point::coord_type result;
  
  arg1 = (Point *)jarg1; 
  result = (Point::coord_type) ((arg1)->y);
  jresult = (unsigned long)result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_Point_undefined() {
  void * jresult ;
  Point result;
  
  result = Point::undefined();
  jresult = new Point((const Point &)result); 
  return jresult;
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_Point_isUndefined(void * jarg1) {
  unsigned int jresult ;
  Point *arg1 = (Point *) 0 ;
  bool result;
  
  arg1 = (Point *)jarg1; 
  result = (bool)(arg1)->isUndefined();
  jresult = result; 
  return jresult;
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_Point_isEqual(void * jarg1, void * jarg2) {
  unsigned int jresult ;
  Point *arg1 = (Point *) 0 ;
  Point *arg2 = 0 ;
  bool result;
  
  arg1 = (Point *)jarg1; 
  arg2 = (Point *)jarg2;
  if (!arg2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Point const & type is null", 0);
    return 0;
  } 
  result = (bool)((Point const *)arg1)->isEqual((Point const &)*arg2);
  jresult = result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_Point(void * jarg1) {
  Point *arg1 = (Point *) 0 ;
  
  arg1 = (Point *)jarg1; 
  delete arg1;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_PlayerPositions() {
  void * jresult ;
  PlayerPositions *result = 0 ;
  
  result = (PlayerPositions *)new PlayerPositions();
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_PlayerPositions_add(void * jarg1, unsigned short jarg2, void * jarg3) {
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  PlayerId arg2 ;
  Point arg3 ;
  Point *argp3 ;
  
  arg1 = (PlayerPositions *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  argp3 = (Point *)jarg3; 
  if (!argp3) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Attempt to dereference null Point", 0);
    return ;
  }
  arg3 = *argp3; 
  (arg1)->add(arg2,arg3);
}


SWIGEXPORT void SWIGSTDCALL CSharp_PlayerPositions_remove(void * jarg1, unsigned short jarg2) {
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  PlayerId arg2 ;
  
  arg1 = (PlayerPositions *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  (arg1)->remove(arg2);
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_PlayerPositions_exists(void * jarg1, unsigned short jarg2) {
  unsigned int jresult ;
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  PlayerId arg2 ;
  bool result;
  
  arg1 = (PlayerPositions *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  result = (bool)(arg1)->exists(arg2);
  jresult = result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_PlayerPositions_getPos(void * jarg1, unsigned short jarg2) {
  void * jresult ;
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  PlayerId arg2 ;
  Point result;
  
  arg1 = (PlayerPositions *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  result = ((PlayerPositions const *)arg1)->getPos(arg2);
  jresult = new Point((const Point &)result); 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_PlayerPositions_setPos(void * jarg1, unsigned short jarg2, void * jarg3) {
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  PlayerId arg2 ;
  Point arg3 ;
  Point *argp3 ;
  
  arg1 = (PlayerPositions *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  argp3 = (Point *)jarg3; 
  if (!argp3) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Attempt to dereference null Point", 0);
    return ;
  }
  arg3 = *argp3; 
  (arg1)->setPos(arg2,arg3);
}


SWIGEXPORT int SWIGSTDCALL CSharp_PlayerPositions_numPlayers(void * jarg1) {
  int jresult ;
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  int result;
  
  arg1 = (PlayerPositions *)jarg1; 
  result = (int)((PlayerPositions const *)arg1)->numPlayers();
  jresult = result; 
  return jresult;
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_PlayerPositions_isEmpty(void * jarg1) {
  unsigned int jresult ;
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  bool result;
  
  arg1 = (PlayerPositions *)jarg1; 
  result = (bool)((PlayerPositions const *)arg1)->isEmpty();
  jresult = result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_PlayerPositions(void * jarg1) {
  PlayerPositions *arg1 = (PlayerPositions *) 0 ;
  
  arg1 = (PlayerPositions *)jarg1; 
  delete arg1;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_XxsMazeMap__SWIG_0(void * jarg1) {
  void * jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  XxsMazeMap *result = 0 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  result = (XxsMazeMap *)new XxsMazeMap((XxsMazeMap const *)arg1);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_XxsMazeMap__SWIG_2(char * jarg1) {
  void * jresult ;
  char *arg1 = (char *) 0 ;
  XxsMazeMap *result = 0 ;
  
  arg1 = (char *)jarg1; 
  result = (XxsMazeMap *)new XxsMazeMap((char const *)arg1);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_XxsMazeMap(void * jarg1) {
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  delete arg1;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeMap_getMapData(void * jarg1) {
  void * jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  MapData *result = 0 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  result = (MapData *) &((XxsMazeMap const *)arg1)->getMapData();
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT unsigned long SWIGSTDCALL CSharp_XxsMazeMap_height(void * jarg1) {
  unsigned long jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  unsigned long result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  result = (unsigned long)((XxsMazeMap const *)arg1)->height();
  jresult = (unsigned long)result; 
  return jresult;
}


SWIGEXPORT unsigned long SWIGSTDCALL CSharp_XxsMazeMap_width(void * jarg1) {
  unsigned long jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  unsigned long result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  result = (unsigned long)((XxsMazeMap const *)arg1)->width();
  jresult = (unsigned long)result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeMap_val__SWIG_0(void * jarg1, void * jarg2) {
  void * jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  Point *arg2 = 0 ;
  MapChar result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  arg2 = (Point *)jarg2;
  if (!arg2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Point const & type is null", 0);
    return 0;
  } 
  result = ((XxsMazeMap const *)arg1)->val((Point const &)*arg2);
  jresult = new MapChar((const MapChar &)result); 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeMap_val__SWIG_1(void * jarg1, unsigned long jarg2, unsigned long jarg3) {
  void * jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  Point::coord_type arg2 ;
  Point::coord_type arg3 ;
  MapChar result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  arg2 = (Point::coord_type)jarg2; 
  arg3 = (Point::coord_type)jarg3; 
  result = ((XxsMazeMap const *)arg1)->val(arg2,arg3);
  jresult = new MapChar((const MapChar &)result); 
  return jresult;
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_XxsMazeMap_isFree__SWIG_0(void * jarg1, void * jarg2) {
  unsigned int jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  Point *arg2 = 0 ;
  bool result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  arg2 = (Point *)jarg2;
  if (!arg2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Point const & type is null", 0);
    return 0;
  } 
  result = (bool)((XxsMazeMap const *)arg1)->isFree((Point const &)*arg2);
  jresult = result; 
  return jresult;
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_XxsMazeMap_isFree__SWIG_1(void * jarg1, unsigned long jarg2, unsigned long jarg3) {
  unsigned int jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  Point::coord_type arg2 ;
  Point::coord_type arg3 ;
  bool result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  arg2 = (Point::coord_type)jarg2; 
  arg3 = (Point::coord_type)jarg3; 
  result = (bool)((XxsMazeMap const *)arg1)->isFree(arg2,arg3);
  jresult = result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeMap_setPlayerPos(void * jarg1, unsigned short jarg2, void * jarg3) {
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  PlayerId *arg2 = 0 ;
  Point *arg3 = 0 ;
  PlayerId temp2 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  temp2 = (PlayerId)jarg2; 
  arg2 = &temp2; 
  arg3 = (Point *)jarg3;
  if (!arg3) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Point const & type is null", 0);
    return ;
  } 
  (arg1)->setPlayerPos((PlayerId const &)*arg2,(Point const &)*arg3);
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeMap_getPlayerPos(void * jarg1, unsigned short jarg2) {
  void * jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  PlayerId arg2 ;
  Point result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  result = ((XxsMazeMap const *)arg1)->getPlayerPos(arg2);
  jresult = new Point((const Point &)result); 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeMap_getAllPos(void * jarg1) {
  void * jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  PlayerPositions *result = 0 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  result = (PlayerPositions *)((XxsMazeMap const *)arg1)->getAllPos();
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeMap_setAllPos(void * jarg1, void * jarg2) {
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  PlayerPositions *arg2 = 0 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  arg2 = (PlayerPositions *)jarg2;
  if (!arg2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "PlayerPositions const & type is null", 0);
    return ;
  } 
  (arg1)->setAllPos((PlayerPositions const &)*arg2);
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeMap_move(void * jarg1, unsigned short jarg2, int jarg3) {
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  PlayerId *arg2 = 0 ;
  Direction *arg3 = 0 ;
  PlayerId temp2 ;
  Direction temp3 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  temp2 = (PlayerId)jarg2; 
  arg2 = &temp2; 
  temp3 = (Direction)jarg3; 
  arg3 = &temp3; 
  (arg1)->move((PlayerId const &)*arg2,(Direction const &)*arg3);
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeMap_removePlayer(void * jarg1, unsigned short jarg2) {
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  PlayerId *arg2 = 0 ;
  PlayerId temp2 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  temp2 = (PlayerId)jarg2; 
  arg2 = &temp2; 
  (arg1)->removePlayer((PlayerId const &)*arg2);
}


SWIGEXPORT int SWIGSTDCALL CSharp_XxsMazeMap_numPlayers(void * jarg1) {
  int jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  int result;
  
  arg1 = (XxsMazeMap *)jarg1; 
  result = (int)((XxsMazeMap const *)arg1)->numPlayers();
  jresult = result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_XxsMazeGUI__SWIG_0(void * jarg1) {
  void * jresult ;
  XxsMazeMap *arg1 = 0 ;
  XxsMazeGUI *result = 0 ;
  
  arg1 = (XxsMazeMap *)jarg1;
  if (!arg1) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "XxsMazeMap const & type is null", 0);
    return 0;
  } 
  result = (XxsMazeGUI *)new XxsMazeGUI((XxsMazeMap const &)*arg1);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_XxsMazeGUI__SWIG_2(char * jarg1) {
  void * jresult ;
  char *arg1 = (char *) 0 ;
  XxsMazeGUI *result = 0 ;
  
  arg1 = (char *)jarg1; 
  result = (XxsMazeGUI *)new XxsMazeGUI((char const *)arg1);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_XxsMazeGUI(void * jarg1) {
  XxsMazeGUI *arg1 = (XxsMazeGUI *) 0 ;
  
  arg1 = (XxsMazeGUI *)jarg1; 
  delete arg1;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeGUI_redraw(void * jarg1) {
  XxsMazeGUI *arg1 = (XxsMazeGUI *) 0 ;
  
  arg1 = (XxsMazeGUI *)jarg1; 
  (arg1)->redraw();
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeGUI_update(void * jarg1, void * jarg2) {
  XxsMazeGUI *arg1 = (XxsMazeGUI *) 0 ;
  PlayerPositions *arg2 = 0 ;
  
  arg1 = (XxsMazeGUI *)jarg1; 
  arg2 = (PlayerPositions *)jarg2;
  if (!arg2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "PlayerPositions const & type is null", 0);
    return ;
  } 
  (arg1)->update((PlayerPositions const &)*arg2);
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeGUI_getMap(void * jarg1) {
  void * jresult ;
  XxsMazeGUI *arg1 = (XxsMazeGUI *) 0 ;
  XxsMazeMap *result = 0 ;
  
  arg1 = (XxsMazeGUI *)jarg1; 
  result = (XxsMazeMap *) &((XxsMazeGUI const *)arg1)->getMap();
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_MonoPlayerProxy(void * jarg1, void * jarg2, void * jarg3) {
  void * jresult ;
  void *arg1 = (void *) 0 ;
  void *arg2 = (void *) 0 ;
  void *arg3 = (void *) 0 ;
  MonoPlayerProxy *result = 0 ;
  
  arg1 = (void *)jarg1; 
  arg2 = (void *)jarg2; 
  arg3 = (void *)jarg3; 
  result = (MonoPlayerProxy *)new MonoPlayerProxy(arg1,arg2,arg3);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_MonoPlayerProxy(void * jarg1) {
  MonoPlayerProxy *arg1 = (MonoPlayerProxy *) 0 ;
  
  arg1 = (MonoPlayerProxy *)jarg1; 
  delete arg1;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_MonoXxsMazeProxy(void * jarg1, void * jarg2, void * jarg3) {
  void * jresult ;
  void *arg1 = (void *) 0 ;
  void *arg2 = (void *) 0 ;
  void *arg3 = (void *) 0 ;
  MonoXxsMazeProxy *result = 0 ;
  
  arg1 = (void *)jarg1; 
  arg2 = (void *)jarg2; 
  arg3 = (void *)jarg3; 
  result = (MonoXxsMazeProxy *)new MonoXxsMazeProxy(arg1,arg2,arg3);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_MonoXxsMazeProxy(void * jarg1) {
  MonoXxsMazeProxy *arg1 = (MonoXxsMazeProxy *) 0 ;
  
  arg1 = (MonoXxsMazeProxy *)jarg1; 
  delete arg1;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_XxsMaze(void * jarg1, short jarg2) {
  void * jresult ;
  XxsMazeMap *arg1 = (XxsMazeMap *) 0 ;
  short arg2 ;
  XxsMaze *result = 0 ;
  
  arg1 = (XxsMazeMap *)jarg1; 
  arg2 = (short)jarg2; 
  result = (XxsMaze *)new XxsMaze(arg1,arg2);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_XxsMaze(void * jarg1) {
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  
  arg1 = (XxsMaze *)jarg1; 
  delete arg1;
}


SWIGEXPORT unsigned short SWIGSTDCALL CSharp_XxsMaze_join(void * jarg1, void * jarg2) {
  unsigned short jresult ;
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  IPlayer *arg2 = (IPlayer *) 0 ;
  PlayerId result;
  
  arg1 = (XxsMaze *)jarg1; 
  arg2 = (IPlayer *)jarg2; 
  result = (PlayerId)(arg1)->join(arg2);
  jresult = result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMaze_leave(void * jarg1, unsigned short jarg2) {
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  PlayerId arg2 ;
  
  arg1 = (XxsMaze *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  (arg1)->leave(arg2);
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMaze_move(void * jarg1, unsigned short jarg2, int jarg3) {
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  PlayerId arg2 ;
  Direction arg3 ;
  
  arg1 = (XxsMaze *)jarg1; 
  arg2 = (PlayerId)jarg2; 
  arg3 = (Direction)jarg3; 
  (arg1)->move(arg2,arg3);
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMaze_getPos(void * jarg1, unsigned short jarg2) {
  void * jresult ;
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  PlayerId *arg2 = 0 ;
  PlayerId temp2 ;
  Point result;
  
  arg1 = (XxsMaze *)jarg1; 
  temp2 = (PlayerId)jarg2; 
  arg2 = &temp2; 
  result = (arg1)->getPos((PlayerId const &)*arg2);
  jresult = new Point((const Point &)result); 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMaze_setPos(void * jarg1, unsigned short jarg2, void * jarg3) {
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  PlayerId *arg2 = 0 ;
  Point arg3 ;
  PlayerId temp2 ;
  Point const *argp3 ;
  
  arg1 = (XxsMaze *)jarg1; 
  temp2 = (PlayerId)jarg2; 
  arg2 = &temp2; 
  argp3 = (Point *)jarg3; 
  if (!argp3) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Attempt to dereference null Point const", 0);
    return ;
  }
  arg3 = *argp3; 
  (arg1)->setPos((PlayerId const &)*arg2,arg3);
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_XxsMaze_isFree(void * jarg1, void * jarg2) {
  unsigned int jresult ;
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  Point arg2 ;
  Point const *argp2 ;
  bool result;
  
  arg1 = (XxsMaze *)jarg1; 
  argp2 = (Point *)jarg2; 
  if (!argp2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Attempt to dereference null Point const", 0);
    return 0;
  }
  arg2 = *argp2; 
  result = (bool)(arg1)->isFree(arg2);
  jresult = result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMaze_getMap(void * jarg1) {
  void * jresult ;
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  XxsMazeMap *result = 0 ;
  
  arg1 = (XxsMaze *)jarg1; 
  result = (XxsMazeMap *)((XxsMaze const *)arg1)->getMap();
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT int SWIGSTDCALL CSharp_XxsMaze_getState(void * jarg1) {
  int jresult ;
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  GameState result;
  
  arg1 = (XxsMaze *)jarg1; 
  result = (GameState)(arg1)->getState();
  jresult = (int)result; 
  return jresult;
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_XxsMaze_isReady(void * jarg1) {
  unsigned int jresult ;
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  bool result;
  
  arg1 = (XxsMaze *)jarg1; 
  result = (bool)(arg1)->isReady();
  jresult = result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMaze_start(void * jarg1) {
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  
  arg1 = (XxsMaze *)jarg1; 
  (arg1)->start();
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMaze_sendUpdate(void * jarg1) {
  XxsMaze *arg1 = (XxsMaze *) 0 ;
  
  arg1 = (XxsMaze *)jarg1; 
  (arg1)->sendUpdate();
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_XxsMazeBot__SWIG_0(void * jarg1) {
  void * jresult ;
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  XxsMazeBot *result = 0 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  result = (XxsMazeBot *)new XxsMazeBot(arg1);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_new_XxsMazeBot__SWIG_1(void * jarg1, char * jarg2, char * jarg3) {
  void * jresult ;
  IxxsMaze *arg1 = (IxxsMaze *) 0 ;
  char *arg2 = (char *) 0 ;
  char *arg3 = (char *) 0 ;
  XxsMazeBot *result = 0 ;
  
  arg1 = (IxxsMaze *)jarg1; 
  arg2 = (char *)jarg2; 
  arg3 = (char *)jarg3; 
  result = (XxsMazeBot *)new XxsMazeBot(arg1,(char const *)arg2,(char const *)arg3);
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_delete_XxsMazeBot(void * jarg1) {
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  delete arg1;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeBot_startGame(void * jarg1, void * jarg2) {
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  PlayerPositions arg2 ;
  PlayerPositions const *argp2 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  argp2 = (PlayerPositions *)jarg2; 
  if (!argp2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Attempt to dereference null PlayerPositions const", 0);
    return ;
  }
  arg2 = *argp2; 
  (arg1)->startGame(arg2);
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeBot_update(void * jarg1, void * jarg2) {
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  PlayerPositions arg2 ;
  PlayerPositions const *argp2 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  argp2 = (PlayerPositions *)jarg2; 
  if (!argp2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Attempt to dereference null PlayerPositions const", 0);
    return ;
  }
  arg2 = *argp2; 
  (arg1)->update(arg2);
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeBot_finish(void * jarg1) {
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  (arg1)->finish();
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeBot_getName(void * jarg1) {
  void * jresult ;
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  String result;
  
  arg1 = (XxsMazeBot *)jarg1; 
  result = (arg1)->getName();
  jresult = new String((const String &)result); 
  return jresult;
}


SWIGEXPORT void * SWIGSTDCALL CSharp_XxsMazeBot_getMap(void * jarg1) {
  void * jresult ;
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  XxsMazeMap *result = 0 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  result = (XxsMazeMap *) &((XxsMazeBot const *)arg1)->getMap();
  jresult = (void *)result; 
  return jresult;
}


SWIGEXPORT int SWIGSTDCALL CSharp_XxsMazeBot_getState(void * jarg1) {
  int jresult ;
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  PlayerState result;
  
  arg1 = (XxsMazeBot *)jarg1; 
  result = (PlayerState)(arg1)->getState();
  jresult = (int)result; 
  return jresult;
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeBot_calcWay(void * jarg1, void * jarg2, void * jarg3) {
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  Point *arg2 = 0 ;
  Point *arg3 = 0 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  arg2 = (Point *)jarg2;
  if (!arg2) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Point const & type is null", 0);
    return ;
  } 
  arg3 = (Point *)jarg3;
  if (!arg3) {
    SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "Point const & type is null", 0);
    return ;
  } 
  (arg1)->calcWay((Point const &)*arg2,(Point const &)*arg3);
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeBot_startup(void * jarg1) {
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  (arg1)->startup();
}


SWIGEXPORT void SWIGSTDCALL CSharp_XxsMazeBot_makeStep(void * jarg1) {
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  
  arg1 = (XxsMazeBot *)jarg1; 
  (arg1)->makeStep();
}


SWIGEXPORT unsigned int SWIGSTDCALL CSharp_XxsMazeBot_reachedGoal(void * jarg1) {
  unsigned int jresult ;
  XxsMazeBot *arg1 = (XxsMazeBot *) 0 ;
  bool result;
  
  arg1 = (XxsMazeBot *)jarg1; 
  result = (bool)((XxsMazeBot const *)arg1)->reachedGoal();
  jresult = result; 
  return jresult;
}


#ifdef __cplusplus
}
#endif

