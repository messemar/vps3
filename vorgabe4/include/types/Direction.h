#ifndef Direction_h
#define Direction_h
// directions the player can move to
typedef enum { 
	STAY, UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT 
} Direction;
#endif
