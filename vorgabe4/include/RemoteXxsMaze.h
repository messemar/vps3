#ifndef REMOTEXXSMAZE_h
#define REMOTEXXSMAZE_h

#include "IxxsMaze.h"
#include "IPlayer.h"
#include "corba/server.h"
#include "types/PlayerId.h"
#include "types/Direction.h"

#include <CORBA.h>
#include <coss/CosNaming.h>

class RemoteXxsMaze: public IxxsMaze{
public:
	RemoteXxsMaze(CORBA::ORB_var& orb, Remote::Server_var game,char* clientName):orb(orb),game(game),clientName(clientName){
		
	}

	PlayerId join(IPlayer* pl);

	void leave(PlayerId ident);

	void move(PlayerId, Direction dir);

	

	void setClientName(char* name){ clientName=name;}
private:

	CORBA::ORB_var orb;
	char* clientName;
	Remote::Server_var game;






};
#endif
