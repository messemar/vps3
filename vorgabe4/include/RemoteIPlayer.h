#ifndef REMOTEIPLAYER_h
#define REMOTEIPLAYER_h

#include "IPlayer.h"
#include "corba/client.h"
#include "types/PlayerPositions.h"

#include <CORBA.h>
#include <coss/CosNaming.h>
class RemoteIPlayer: public IPlayer{
public:

		RemoteIPlayer(Remote::Client_var client):client(client){}

		void startGame(const PlayerPositions);

		void update(const PlayerPositions);

		void nextMove();

		void finish();


private:
		Remote::Client_var client;

};
#endif
