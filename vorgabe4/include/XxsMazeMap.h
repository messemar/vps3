/** Defines XxsMazeMap
 * @file XxsMazeMap.h
 */
#ifndef XxsMazeMap_h
#define XxsMazeMap_h

#include <map>
#include <iostream>
#include "types/MapData.h"
#include "types/PlayerId.h"
#include "types/Direction.h"
#include "types/PlayerPositions.h"

/**
 *
 */
class XxsMazeMap {
public:
	/** @enum MapElements values for the map
	 *
	 */
	enum MapElements { MAP_WAY=' ', MAP_WALL='x' };

	/** Copy constructor from pointer to XxsMazeMap.
	 *
	 */
	XxsMazeMap(const XxsMazeMap* other) : map(other->getMapData()) 
	{
		int maxp = other->numPlayers();
		for (int i = 0; i < maxp; i++) {
			players.setPos(i, other->getPlayerPos(i));
		}
	}

	/** Copy constructor from reference to XxsMazeMap.
	 *
	 */
	XxsMazeMap(const XxsMazeMap& other) : map(other.getMapData()) 
	{
		//map = other->getMapData();
		int maxp = other.numPlayers();
		for (int i = 0; i < maxp; i++) {
			players.setPos(i, other.getPlayerPos(i));
		}
	}

	/** Contructor to create map from file.
	 *
	 */
	XxsMazeMap(const char* file);

	// conversion from MapData to Map
	// XxsMazeMap(const MapData& m);
	// XxsMazeMap(MapData& m);

	~XxsMazeMap() {};

	
	/** Returns the data of the map.
	 * 
	 * @return a reference to MapData
	 */
	const MapData& getMapData() const;


	/** size information.
	 *
	 */
	unsigned long height() const;

	/** size information.
	 *
	 */
	unsigned long width() const;
	
	/** Return value at given position.
	 *
	 * @param pos the coordinates of the point
	 * 				the value of which is requested
	 * @return the value as MapChar
	 * @see MapChar
	 */
	MapChar val(const Point& pos) const;

	/** Return value at given position.
	 *
	 * @param x the x coordinate of the point
	 * 				the value of which is requested
	 * @param y the y coordinate of the point
	 * 				the value of which is requested
	 * @return the value as MapChar
	 * @see MapChar
	 */
	MapChar val(const Point::coord_type x, const Point::coord_type y) const;

	/** Indicates if the given position is free.
	 *
	 * @param p coordinates of the point of interest
	 * @return true if field at given position is free
	 *         false otherwise
	 */
	bool isFree(const Point& p) const;

	/** Indicates if the given position is free.
	 *
	 * @param x the x coordinate of the point of interest
	 * @param y the y coordinate of the point of interest
	 * @return true if field at given position is free
	 *         false otherwise
	 */
	bool isFree(const Point::coord_type x, const Point::coord_type y) const;

	/** set player position
	 * @param id the player id
	 * @param pos the new position
	 */
	void setPlayerPos(const PlayerId& id, const Point& pos);

	/** get player position
	 * @param id the player id
	 * @return the position of the player of intererst
	 */
	const Point getPlayerPos(const PlayerId id) const;

	/** get position data of all players
	 * @return the positions of all players integrated
	 * 				 in the type PlayerPositions
	 */
	const PlayerPositions* getAllPos() const;

	/** set position data of all players
	 * @param positions the positions of all players integrated
	 * 				 in the type PlayerPositions
	 */
	void setAllPos(const PlayerPositions& positions);

	/** move player one step into specified direction
	 *
	 */
	void move(const PlayerId& id, const Direction& dir); 

	/** remove player from map
	 *
	 */
	void removePlayer(const PlayerId& id);
	
	/** return number of players in list 
	 *
	 */
	int numPlayers() const;

private:
	// the labyrinth
	MapData map;
	// the player positions
	PlayerPositions players;

	// read a map from a file
	void open(const char* file);
};


// most functions are really short so inline them...

inline
XxsMazeMap::XxsMazeMap(const char* file) {
	open(file);
}

inline
const MapData& XxsMazeMap::getMapData() const {
	return map;
}

inline
unsigned long XxsMazeMap::height() const {
	return map.size();
}

inline
unsigned long XxsMazeMap::width() const {
	return map[0].size();
}

inline
char XxsMazeMap::val(const Point& p) const {
	return map[p.y][p.x];
}

inline
MapChar XxsMazeMap::val(const Point::coord_type x, const Point::coord_type y)
	const
{
	return map[y][x];
}

inline
bool XxsMazeMap::isFree(const Point& p) const 
{
	return map[p.y][p.x] == MAP_WAY;
}

inline
bool XxsMazeMap::isFree(const Point::coord_type x, const Point::coord_type y)
	const 
{
	return map[y][x] == MAP_WAY;
}

inline
void XxsMazeMap::setPlayerPos(const PlayerId& id, const Point& p) 
{
	if (players.exists(id)) {
		map[players.getPos(id).y][players.getPos(id).x] = MAP_WAY;
	}
	players.setPos(id,p);
	map[players.getPos(id).y][players.getPos(id).x] = id;
}

inline
const Point XxsMazeMap::getPlayerPos(const PlayerId id) const 
{
	return players.getPos(id);
}

inline
const PlayerPositions* XxsMazeMap::getAllPos() const 
{
	return &players;
}

inline
void XxsMazeMap::setAllPos(const PlayerPositions& positions) 
{
	if (!players.isEmpty()) {
		for (int id=0; id<numPlayers(); id++) {
			map[players.getPos(id).y][players.getPos(id).x] = MAP_WAY;
		}
	}
	
	players = positions;

	for (int id=0; id<numPlayers(); id++) {
		map[players.getPos(id).y][players.getPos(id).x] = id;
	}
}

inline
void XxsMazeMap::move(const PlayerId& id, const Direction& dir)
{
	Point pos = getPlayerPos(id);
	switch (dir) {
		case UP 				: pos.y -=1; 
											break;
		case UP_RIGHT		: pos.y -=1;
											pos.x +=1;
											break;
		case RIGHT			: pos.x +=1;
											break;
		case DOWN_RIGHT	: pos.y +=1;
											pos.x +=1;
											break;
		case DOWN				: pos.y +=1;
											break;
		case DOWN_LEFT	: pos.y +=1;
											pos.x -=1;
											break;
		case LEFT				:	pos.x -=1;
											break;
		case UP_LEFT		: pos.y -=1;
											pos.x -=1;
											break;
		default					: break;
	}
	if (isFree(pos)) {
		setPlayerPos(id,pos);
	} else {
		std::cerr << "NOT FREEE!!!! (" << pos.x << "," << pos.y << ")" << std::endl;
	}
}

inline
int XxsMazeMap::numPlayers() const 
{
	return players.numPlayers();
}

inline
void XxsMazeMap::removePlayer(const PlayerId& id) 
{
	players.remove(id);
}
#endif
