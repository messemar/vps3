/**
 * 
 */
package jxxsMaze;

import jxxsMaze.types.PlayerState;

import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import Remote.ServerHelper;

/**
 * @author loki
 *
 */
public class XxsMazeClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		      // initialize the ORB
		      System.err.println("initializing the ORB...");
		      org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args, null);

		      // now resolve the reference to the RootPOA
		      System.err.println("resolving initial reference 'RootPOA'...");
		      org.omg.CORBA.Object poa_obj =
		        orb.resolve_initial_references("RootPOA");
		      org.omg.PortableServer.POA poa =
		        org.omg.PortableServer.POAHelper.narrow(poa_obj);

		      // activate the POAManager
		      System.err.println("activating the POAManager...");
		      org.omg.PortableServer.POAManager mgr = poa.the_POAManager();
		      mgr.activate();

		      // resolve initial reference to the NameService
		      System.err.println("resolving initial reference 'NameService'...");
		      org.omg.CORBA.Object ns_obj =
		        orb.resolve_initial_references("NameService");
		      NamingContextExt nc = NamingContextExtHelper.narrow(ns_obj);

		      // resolve server (little easier that in C++ :)
		      System.err.println("resolving reference to 'xxsMazeServer'...");
		      NameComponent [] name =
		        new NameComponent[]{
				  //new NameComponent("MasterServer", "XxsMaze")
		          new NameComponent("XxsMaze", "MasterServer")
		        };
		      org.omg.CORBA.Object server_obj = nc.resolve(name);
		      // and downcast it
		      Remote.Server server =
		        ServerHelper.narrow(server_obj);

		      // create MPGProxy with this game
		      //System.err.println("creating XxsMazeProxy from 'xxsMazeServer'...");
		      //XxsMazeProxy mpg = new XxsMazeProxy(orb,server);

		      // create new (local) HumanPlayer
		      System.err.println("creating HumanPlayer...");
		      XxsMazeHuman player = new XxsMazeHuman(server,"../maps/simple.map", "MisterX",poa,nc);

		      /*
		       * now the game starts
		       */
		      System.err.println("starting the game...");

		      // first of all logon
		      player.startup();
		      
		      System.err.println("entering main-loop...");
		      while (player.getState() != PlayerState.FINISHED) {
		          // has the orb work to do?
		          while (orb.work_pending()) {
		            orb.perform_work();
		          }

		          if (player.getState() == PlayerState.GAMEING) {
		            player.makeStep();
		          }

		          while (orb.work_pending()) {
		            orb.perform_work();
		          }

		          // sleep a mom (milliseconds)
		          try {
		            Thread.sleep(100);
		          } catch(InterruptedException e) { }
		        }

		        System.err.println("game done...");

		        // shutdown the POA and it's managed object's
		        poa.destroy(true, true);
		        System.err.println("POA destroyed...");

		        // shutdown the orb
		        System.err.println("shuting down the ORB...");
		        orb.shutdown(true);

		        // time to say good bye
		        System.err.println("bye bye...");


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.exit(1);
		}

	}

}
