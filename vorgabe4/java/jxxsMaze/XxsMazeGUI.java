package jxxsMaze;

//import java.awt.Window;

import jxxsMaze.map.Map;
import jxxsMaze.swing.InstructionConsumer;
import jxxsMaze.swing.InteractiveFrame;
import jxxsMaze.swing.MapView;
import jxxsMaze.types.PlayerPositions;

public class XxsMazeGUI {
	
	XxsMazeGUI(String filename, InstructionConsumer cons, String name)
	{
		map = new Map(filename);
		win = new InteractiveFrame(new MapView(map), cons);
		win.setTitle("XxsMaze--Player " + name);
		win.setVisible(true);
		win.repaint();
	}
	
	public void update(final PlayerPositions ppos) 
	{
		System.out.println("GUI::update");
		map.setAllPos(ppos);
		win.repaint();
	}
	private Map map;
	private InteractiveFrame win;
	//private Window win;
}
