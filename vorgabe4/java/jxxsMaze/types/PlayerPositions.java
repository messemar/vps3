package jxxsMaze.types;

import java.util.HashMap;
import Remote.PlayerPosition;
import jxxsMaze.types.Point;

public class PlayerPositions {
    private HashMap<Integer, Point> players;
    public PlayerPositions(final PlayerPosition[] ppos) {
        players = new HashMap<Integer, Point>();
        for(PlayerPosition player : ppos) {
            players.put(new Integer(player.id), new Point(player.xPos, player.yPos));
        }
    }

    //public PlayerPosition[] toPlayerPositions() {
        
    //}

    public void add(int id, Point pos) {
        players.put(id, pos);
    }

	public void remove(int id) {
        players.remove(id);
    }

	public boolean exists(int id) {
        return players.containsKey(id);
    }

	public Point getPos(int id) {
        return players.get(id);
    }

	public void setPos(int id, Point pos) {
        players.put(id, pos);
    }

	public int numPlayers() {
        return players.size();
    }

	public boolean isEmpty() {
        return numPlayers() == 0;
    }
};
