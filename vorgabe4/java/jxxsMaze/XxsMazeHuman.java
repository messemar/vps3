/**
 * 
 */
package jxxsMaze;

import jxxsMaze.XxsMazeGUI;
import jxxsMaze.types.PlayerState;
import Remote.ClientPOA;
import Remote.Server;
import Remote.PlayerPosition;
import jxxsMaze.swing.Instruction;
import jxxsMaze.swing.InstructionConsumer;
import jxxsMaze.types.PlayerPositions;
import jxxsMaze.types.Direction;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

/**
 * @author Andreas Lagemann
 *
 */

public class XxsMazeHuman extends ClientPOA implements InstructionConsumer{

	/**
	 * 
	 */
	public XxsMazeHuman(Server game, String filename, String name, org.omg.PortableServer.POA poa, NamingContextExt ns) {
		// Initialize your members ...
    try{
		org.omg.CORBA.Object o=poa.servant_to_reference(this);
		poa.the_POAManager().activate();
		NameComponent[] cname = 
			new NameComponent[]{
				new NameComponent(name, "")
			};
		ns.bind(cname, o);
    }catch(Exception e){
        e.printStackTrace();
	    throw new org.omg.CORBA.UNKNOWN();
    }
		ui = new XxsMazeGUI(filename, this, name);
		inst = Instruction.STAY;
        state=PlayerState.START_WAIT;
        this.game=game;
        this.name=name;
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IPlayer#startGame(jxxsMaze.types.PlayerPositions)
	 */
	public void c_startGame(PlayerPosition[] pos) {
        c_update(pos);
	}

	/* (non-Javadoc)
	 * @see jxxsMaze.IPlayer#update(jxxsMaze.types.PlayerPositions)
	 */
	public void c_update(PlayerPosition[] pos) {
        players=pos;
        ui.update(new PlayerPositions(pos));
	}

    public void c_nextMove(){
        state=PlayerState.GAMEING;
    }

	/* (non-Javadoc)
	 * @see jxxsMaze.IPlayer#finish()
	 */
	public void c_finish() {
        state=PlayerState.FINISHED;
	}
	
	/* (non-Javadoc)
	 * @see jxxsMaze.swing.InstructionConsumer#consume(jxxsMaze.swing.Instruction)
	 */
	public void consume(Instruction i) {
		// depending on the instruction ad a new step to the way or
		// quit the game
        if(i==Instruction.QUIT){
            state = PlayerState.FINISHED;
        }else{
            inst=i;
        }

	}
	
	public void startup() 
	{
        PlayerId=game.s_join(name);
	}
	
	public void makeStep()
	{
        // wait for key press 
        switch(inst){
            case UP:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.UP.ordinal());                
                break;
            case UP_RIGHT:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.UP_RIGHT.ordinal());                
                break;
            case RIGHT:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.RIGHT.ordinal());                
                break;
            case DOWN_RIGHT:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.DOWN_RIGHT.ordinal());                
                break;
            case DOWN:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.DOWN.ordinal());                
                break;
            case DOWN_LEFT:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.DOWN_LEFT.ordinal());                
                break;
            case LEFT:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.LEFT.ordinal());                
                break;
            case UP_LEFT:
                state=PlayerState.WAIT4UPDATE;
                game.s_move((short)PlayerId,(short)Direction.UP_LEFT.ordinal());                
                break;
            case STAY:
                //game.nextMove(PlayerId,Direction.UP);                
                break;

        }
        inst = Instruction.STAY;
        
        
	}
	
	PlayerState getState()
	{
        return state;
	}
	
	//...
	private XxsMazeGUI ui;
    
    int PlayerId;

    PlayerPosition[] players;

    Server game;    

    String name;

    Instruction inst;

    private PlayerState state;
}
