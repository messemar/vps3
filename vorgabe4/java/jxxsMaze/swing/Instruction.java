/**
 * 
 */
package jxxsMaze.swing;

/**
 * @author loki
 *
 */
public enum Instruction {
	STAY,UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT, QUIT
}

