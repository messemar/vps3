/**
 * 
 */
package jxxsMaze.swing;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import jxxsMaze.map.Map;

/**
 * @author loki
 *
 */
public class MapView extends JPanel {

	/**
	 * 
	 */
	public MapView(Map m) {
		super();
		map = m;
		this.setSize((map.height()+2)*PIXEL_SIZE,(map.width()+1)*PIXEL_SIZE);
		repaint();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		this.setBackground(Color.BLACK);
		
		char data[][] = map.getMapData();
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				switch (map.val(j,i)) {
				case Map.MapChars.MAP_WALL:
					g.setColor(Color.LIGHT_GRAY);
					g.fillRect(j*PIXEL_SIZE,i*PIXEL_SIZE,PIXEL_SIZE,PIXEL_SIZE);
					break;
				case Map.MapChars.MAP_WAY:
					g.setColor(Color.BLACK);
					g.fillRect(j*PIXEL_SIZE,i*PIXEL_SIZE,PIXEL_SIZE,PIXEL_SIZE);
					break;
				default: // player!
					Color col;
					switch (map.val(j,i)) {
					case 0:
						col = Color.BLUE; 
						break;
					case 1:
						col = Color.CYAN;
						break;
					case 2:
						col = Color.GREEN;
						break;
					case 3:
						col = Color.MAGENTA;
						break;
					case 4:
						col = Color.ORANGE;
						break;
					case 5:
						col = Color.PINK;
						break;
					case 6:
						col = Color.RED;
						break;
					case 7:
						col = Color.YELLOW;
						break;		
					default:
						col = Color.WHITE;
						break;
					}
					
					g.setColor(col);
					g.fillOval(j*PIXEL_SIZE,i*PIXEL_SIZE,PIXEL_SIZE,PIXEL_SIZE);
				}
			}
		}
	}
	
	static public final int PIXEL_SIZE = 15;
	static public final long serialVersionUID = 4712;  
	
	private Map map;
}
