package jxxsMaze.swing;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class InteractiveFrame extends JFrame implements KeyListener{

	public InteractiveFrame(JPanel panel, InstructionConsumer c)
	{
		cons = c;
		setSize(panel.getHeight(), panel.getWidth());
		addKeyListener(this);
		add(panel);
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		switch (e.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			cons.consume(Instruction.QUIT);
			break;
		case KeyEvent.VK_NUMPAD1:
			cons.consume(Instruction.DOWN_LEFT);
			break;
		case KeyEvent.VK_NUMPAD2:
			cons.consume(Instruction.DOWN);
			break;
		case KeyEvent.VK_NUMPAD3:
			cons.consume(Instruction.DOWN_RIGHT);
			break;
		case KeyEvent.VK_NUMPAD4:
			cons.consume(Instruction.LEFT);
			break;
		case KeyEvent.VK_NUMPAD6:
			cons.consume(Instruction.RIGHT);
			break;
		case KeyEvent.VK_NUMPAD7:
			cons.consume(Instruction.UP_LEFT);
			break;
		case KeyEvent.VK_NUMPAD8:
			cons.consume(Instruction.UP);
			break;
		case KeyEvent.VK_NUMPAD9:
			cons.consume(Instruction.UP_RIGHT);
			break;
		
		default:
			break;
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		// do nothing
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		// do nothing
	}
	
	private InstructionConsumer cons;
	public static final long serialVersionUID = 4711;  
}
