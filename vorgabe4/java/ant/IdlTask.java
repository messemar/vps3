/**
 * 
 */
package ant;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * @author loki
 *
 */
public class IdlTask extends Task {
	
	String sourcePath;
	String idlPath;
	String outputPath;
	String includePath;
	String i2jpackageString;
	String i2jpackageFile;
	boolean all = false;
	boolean forceOverwrite= false;
	boolean repoInfo= false;
    boolean noFinal= false;
    boolean uncheckedNarrow= false;
    boolean noSkel= false;
    boolean noStub= false;
    boolean cldc10= false;
    boolean sloppyForward= false;
    boolean sloppyNames= false;
    boolean permissiveRmic= false;
    boolean syntax= false;
    String cmmnd;

	public void execute() throws BuildException {
		//System.out.println("execute called");
		Runtime runtime = Runtime.getRuntime();
		
		// this is specific *NIX systems for others
		// you have to change this
		cmmnd = idlPath + "/idl ";
		System.out.println("cmmnd set to: " + cmmnd);
		
		if(outputPath != null) {
			cmmnd += "-d " + outputPath + " ";
		}		
		System.out.println("cmmnd set to: " + cmmnd);

		if(includePath != null) {
			cmmnd += "-I" + includePath + " ";
		}
		System.out.println("cmmnd set to: " + cmmnd);

		if(i2jpackageString != null) {
			cmmnd += "-i2jpackage " + i2jpackageString + " ";
		}
		System.out.println("cmmnd set to: " + cmmnd);

		if(i2jpackageFile != null) {
			cmmnd += "-i2jpackagefile " + i2jpackageFile + " ";
		}
		System.out.println("cmmnd set to: " + cmmnd);

		if (all) {
			cmmnd += "-all ";
		}
		System.out.println("cmmnd set to: " + cmmnd);

		if (forceOverwrite) {
			cmmnd += "-forceOverwrite ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (repoInfo) {
			cmmnd += "-ir ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (noFinal) {
			cmmnd += "-nofinal ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (uncheckedNarrow) {
			cmmnd += "-unchecked_narrow ";	
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (noSkel) {
			cmmnd += "-noskel ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (noStub) {
			cmmnd += "-nostub ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (cldc10) {
			cmmnd += "-cldc10 ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (sloppyForward) {
			cmmnd += "-sloppy_forward ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (sloppyNames) {
			cmmnd += "-sloppy_names ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (permissiveRmic) {
			cmmnd += "-permissive_rmic ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		if (syntax) {
			cmmnd += "-syntax ";
		}
		System.out.println("cmmnd set to: " + cmmnd);
		
		try {
			cmmnd += sourcePath + "/*.idl";
			System.out.println("cmmnd set to: " + cmmnd);
			String basedir = sourcePath + "/..";
			PrintStream skript = new PrintStream(new File(basedir + "/idl.sh"));
			//skript.write(cmmnd);
			skript.println("exec >& ./out.txt");
			skript.println(cmmnd);
			skript.close();
			Process proc = runtime.exec("sh " + basedir + "/idl.sh");
						
			BufferedReader procIn = 
				new BufferedReader(new InputStreamReader(proc.getInputStream()));
			BufferedReader procErr = 
				new BufferedReader(new InputStreamReader(proc.getErrorStream()));
			BufferedWriter procOut =
				new BufferedWriter(new OutputStreamWriter(proc.getOutputStream()));
			BufferedReader sysIn=
				new BufferedReader(new InputStreamReader(System.in));
			
			while(procIn.ready() || procErr.ready() || sysIn.ready()) {
				if (procIn.ready()) {
					System.out.write(procIn.read());
				}
				if (procErr.ready()) {
					System.err.write(procErr.read());
				}
				if (sysIn.ready()) {
					procOut.write(sysIn.read());
				}
			}
						
			System.out.println("process created!!!");
			proc.waitFor();
			System.out.print("IDL exited with exit code " + proc.exitValue());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public void setSourcePath(String pathname) {
		if (pathname != null) {
			System.out.println("sourcePath set to " + pathname);
			sourcePath = pathname;
		}
	}

	public void setIdlPath(String pathname) {
		if (pathname != null) {
			System.out.println("idlPath set to " + pathname);
			idlPath = pathname;
		}

	}

	public void setOutputPath(String pathname) {
		if (pathname != null) {
			outputPath = pathname;
		}
	}

	/**
	 * @param all The all to set.
	 */
	public void setAll(boolean all) {
		this.all = all;
	}

	/**
	 * @param cldc10 The cldc10 to set.
	 */
	public void setCldc10(boolean cldc10) {
		this.cldc10 = cldc10;
	}

	/**
	 * @param forceOverwrite The forceOverwrite to set.
	 */
	public void setForceOverwrite(boolean forceOverwrite) {
		this.forceOverwrite = forceOverwrite;
	}

	/**
	 * @param filename The i2jpackageFilename to set.
	 */
	public void setI2jpackageFile(String filename) {
		i2jpackageFile = filename;
	}

	/**
	 * @param string The i2jpackageString to set.
	 */
	public void setI2jpackageString(String string) {
		i2jpackageString = string;
	}

	/**
	 * @param includePath The includePath to set.
	 */
	public void setIncludePath(String includePath) {
		this.includePath = includePath;
	}

	/**
	 * @param noFinal The noFinal to set.
	 */
	public void setNoFinal(boolean noFinal) {
		this.noFinal = noFinal;
	}

	/**
	 * @param noSkel The noSkel to set.
	 */
	public void setNoSkel(boolean noSkel) {
		this.noSkel = noSkel;
	}

	/**
	 * @param noStub The noStub to set.
	 */
	public void setNoStub(boolean noStub) {
		this.noStub = noStub;
	}

	/**
	 * @param permissiveRmic The permissiveRmic to set.
	 */
	public void setPermissiveRmic(boolean permissiveRmic) {
		this.permissiveRmic = permissiveRmic;
	}

	/**
	 * @param repoInfo The repoInfo to set.
	 */
	public void setRepoInfo(boolean repoInfo) {
		this.repoInfo = repoInfo;
	}

	/**
	 * @param sloppyForward The sloppyForward to set.
	 */
	public void setSloppyForward(boolean sloppyForward) {
		this.sloppyForward = sloppyForward;
	}

	/**
	 * @param sloppyNames The sloppyNames to set.
	 */
	public void setSloppyNames(boolean sloppyNames) {
		this.sloppyNames = sloppyNames;
	}

	/**
	 * @param syntax The syntax to set.
	 */
	public void setSyntax(boolean syntax) {
		this.syntax = syntax;
	}

	/**
	 * @param uncheckedNarrow The uncheckedNarrow to set.
	 */
	public void setUncheckedNarrow(boolean uncheckedNarrow) {
		this.uncheckedNarrow = uncheckedNarrow;
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.Task#init()
	 */
	@Override
	public void init() throws BuildException {
		// TODO Auto-generated method stub
		System.out.println("Init called!");
		super.init();
	}
}
