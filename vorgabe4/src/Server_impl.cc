#include "Server_impl.h"
#include "corba/client.h"
#include "types/PlayerId.h"
#include "types/Direction.h"
#include "RemoteIPlayer.h"

CORBA::UShort Server_impl::s_join(const char* name){
	
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
		CosNaming::NamingContext_var nc= CosNaming::NamingContext::_narrow(nsobj);
		CosNaming::Name cname;
		cname.length(1);
		cname[0].id= CORBA::string_dup(name);
		cname[0].kind=CORBA::string_dup("");
		CORBA::Object_var obj = nc->resolve(cname);
		Remote::Client_var client_var= Remote::Client::_narrow(obj);
		std::cout<< "Client " << name <<"is joined"<<std::endl;
		RemoteIPlayer* ip = new RemoteIPlayer(client_var);
		return (CORBA::UShort)game->join(ip);
}

void Server_impl::s_move(CORBA::UShort id, CORBA::UShort dir){
	game->move((PlayerId) id, (Direction) dir); 
}

void Server_impl::s_leave(CORBA::UShort ident){
	game->leave((PlayerId) ident);
}
