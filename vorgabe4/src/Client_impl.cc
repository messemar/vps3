#include "Client_impl.h"
#include "types/Point.h"
#include "types/PlayerPositions.h"

void Client_impl::c_startGame(SequenceTmpl< ::Remote::PlayerPosition,MICO_TID_DEF> ppos)
{
	PlayerPositions pos;
	for(int i=0;i<ppos.length();i++){
		Point p(ppos[i].xPos,ppos[i].yPos);
		pos.add((PlayerId) i,p);
	}

	ip->startGame(pos);
}

void Client_impl::c_update(SequenceTmpl< ::Remote::PlayerPosition,MICO_TID_DEF> ppos)
{
	PlayerPositions pos;
	for(int i=0;i<ppos.length();i++){
		Point p(ppos[i].xPos,ppos[i].yPos);
		pos.add((PlayerId) i,p);
	}

	ip->update(pos);
}

void Client_impl::c_nextMove(){
	ip->nextMove();
}

void Client_impl::c_finish(){
	ip->finish();
}
