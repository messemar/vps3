#include "XxsMazeBot.h"
#include "XxsMazeClient.h"
#include "RemoteXxsMaze.h"
#include "XxsMazeMap.h"
#include <cstdlib>
#include <cstring>
#include <cstdio>

#include <CORBA.h>
#include <coss/CosNaming.h>

int main(int argc, char* argv[]) {
	if (argc < 3) {
		cerr << "usage: xxsMaze  <mapfile> <clientname> <corba> " << endl;
		exit(4711);
	}
	char* mapfile = argv[1];

	// create an instance of the map and the game
	XxsMazeMap theMap(mapfile);

	//create orb
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");
	std::cout<< " ORB initialised"<<std::endl;
	//get remote game infos
	char* cname= argv[2];
	std::cout<< " name created"<<std::endl;
	
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
		CosNaming::NamingContext_var nc= CosNaming::NamingContext::_narrow(nsobj);
		CosNaming::Name name;
		name.length(1);
		name[0].id= CORBA::string_dup("XxsMaze");
		name[0].kind=CORBA::string_dup("MasterServer");
		CORBA::Object_var obj = nc->resolve(name);
		Remote::Server_var sgame= Remote::Server::_narrow(obj);
	RemoteXxsMaze* game= new RemoteXxsMaze(orb,sgame,(char*)cname);
	XxsMazeBot* bot= new XxsMazeBot(game,mapfile,cname);
	std::cout<< " bot created"<<std::endl;
	// create the corresponding client instances and start it
	XxsMazeBotClient* client = new XxsMazeBotClient(bot);
	std::cout<< " clients Created"<<std::endl;
	client->start();
	orb->run();
	std::cout<< "clients started"<<std::endl;
	client->join();
	
	delete bot;
	delete client;

	

}
