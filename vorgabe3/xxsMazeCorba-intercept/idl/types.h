/*
 *  MICO --- an Open Source CORBA implementation
 *  Copyright (c) 1997-2006 by The Mico Team
 *
 *  This file was automatically generated. DO NOT EDIT!
 */

#include <CORBA.h>
#include <mico/throw.h>

#ifndef __TYPES_H__
#define __TYPES_H__






struct PlayerPosition;
typedef TFixVar< PlayerPosition > PlayerPosition_var;
typedef PlayerPosition& PlayerPosition_out;


struct PlayerPosition {
  #ifdef HAVE_TYPEDEF_OVERLOAD
  typedef PlayerPosition_var _var_type;
  #endif
  #ifdef HAVE_EXPLICIT_STRUCT_OPS
  PlayerPosition();
  ~PlayerPosition();
  PlayerPosition( const PlayerPosition& s );
  PlayerPosition& operator=( const PlayerPosition& s );
  #endif //HAVE_EXPLICIT_STRUCT_OPS

  CORBA::ULong xPos;
  CORBA::ULong yPos;
  CORBA::UShort id;
};

#ifndef MICO_CONF_NO_POA

#endif // MICO_CONF_NO_POA

extern CORBA::StaticTypeInfo *_marshaller_PlayerPosition;

#endif
