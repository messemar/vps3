#include "XxsMazeServer.h"
#include "Server_impl.h"
#include "corba/trace/clientTracer.h"
#include "corba/trace/serverTracer.h"
#include "corba/service/clientPWDService.h"
#include "corba/service/serverPWDService.h"

#include <cstdlib>
#include <cstring>
#include <cstdio>

#include <CORBA.h>
#include <coss/CosNaming.h>


using namespace std;

static const bool TRACE_VERBOSE=false;

class MyTracer 
    : virtual public PortableInterceptor::ORBInitializer
    , virtual public CORBA::LocalObject
 {
public:
    MyTracer() {}
    ~MyTracer() {}

    virtual void pre_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	//ServerTracer *sTrace = new ServerTracer("server role", TRACE_VERBOSE);
    	//ClientTracer *cTrace = new ClientTracer("client role", TRACE_VERBOSE);
    	//info->add_server_request_interceptor(sTrace);// server only
    	//info->add_client_request_interceptor(cTrace); 
       ServerPWDService* service= new ServerPWDService();
       ClientPWDService* clientService=new ClientPWDService(47124712,"client role","47124712");
        info->add_server_request_interceptor(service);
        info->add_client_request_interceptor(clientService); 
    }
    
    virtual void post_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	// nothing
    }
};

int main(int argc, char* argv[]) {
	if (argc < 2) {
		cerr << "usage: xxsMaze <corba> <mapfile> <playercount> " << endl;
		exit(4711);
	}
	char* mapfile = argv[3];
	int playercount = atoi(argv[4]);

	// create an instance of the map and the game
	XxsMazeMap theMap(mapfile);
	XxsMaze theGame(&theMap,playercount);
    // tracer
	MyTracer* bb_init=new MyTracer();
	PortableInterceptor::register_orb_initializer(bb_init);
	//create orb
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");
	cout<<"orb created"<<endl;
	//create Server_impl 
	const char* servername= "XxsMaze";
		
	Server_impl* server= new Server_impl(servername,orb,&theGame);
	cout<<"server impl created"<<endl;
	CORBA::Object_var poaobj = orb->resolve_initial_references("RootPOA");
		PortableServer::POA_var poa = PortableServer::POA::_narrow(poaobj);
		// ...and the manager
		PortableServer::POAManager_var mgr = poa->the_POAManager();
		PortableServer::ObjectId_var oid=poa->activate_object(server);
		mgr->activate();
		// naming service

		CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
		CosNaming::NamingContext_var nc = CosNaming::NamingContext::_narrow(nsobj);

		CosNaming::Name name;
		name.length(1);
		name[0].id=CORBA::string_dup(servername);
		name[0].kind=CORBA::string_dup("");
		nc->bind(name, server->_this());

		cout<<"server impl started"<<endl;
	XxsMazeServer mserver(theGame);
	mserver.start();
	while(true){
        orb->perform_work();
    }
	mserver.join();

    orb->destroy();
	
}
