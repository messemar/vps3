#include <CORBA.h>
#include <coss/CosNaming.h>

// includes interfaces for Client and Server
#include "corba/IxxsMaze.h"
#include "corba/PlayerAdapter.h"
#include "corba/trace/clientTracer.h"
#include "corba/trace/serverTracer.h"
#include "XxsMazeProxy.h"
#include "XxsMazeBot.h"



using namespace std;
using namespace xxsMaze;


// switch to true to see more information
static const bool TRACE_VERBOSE = false;

class MyTracer : virtual public PortableInterceptor::ORBInitializer {
public:
    MyTracer() {}
    ~MyTracer() {}

    virtual void pre_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	// register interceptors	
    	ServerTracer *sTrace = new ServerTracer("server role", TRACE_VERBOSE);
    	ClientTracer *cTrace = new ClientTracer("client role", TRACE_VERBOSE);
    	info->add_server_request_interceptor(sTrace);
    	info->add_client_request_interceptor(cTrace);
    }
    
    virtual void post_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	// nothing
    }
};


int main(int argc, char* argv[]) 
{
	// Interceptor must be activated before ORB-init
	MyTracer bb_init;
	PortableInterceptor::register_orb_initializer(&bb_init);
	// initialize the ORB
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");

	// get a reference to the RootPOA...
	CORBA::Object_var poaobj = orb->resolve_initial_references("RootPOA");
	PortableServer::POA_var poa = PortableServer::POA::_narrow(poaobj);
	// ...and the manager
	PortableServer::POAManager_var mgr = poa->the_POAManager();

	if (argc < 2) {
		cerr << "usage; " << argv[0] << "<CORBA_ARGs> <clientname> <mapfilename>" << endl << flush;
		exit(1);
	}
	char* robotName = argv[1];
	const char* mapfile = argv[2];

	// get reference to naming service
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
	CosNaming::NamingContext_var nc = CosNaming::NamingContext::_narrow(nsobj);

	// genereate name entry to resolve
	CosNaming::Name name;
	name.length(1);
	name[0].id   = CORBA::string_dup("MasterServer");
	name[0].kind = CORBA::string_dup("xxsMazeServer");
	
	// get the ObjectId from the nsd
	CORBA::Object_var obj = nc->resolve(name);
	
	// check if the Object is aviable
	if (CORBA::is_nil(obj)) {
		cerr << "xxsMazeServer not found...exiting :(" << endl;
		return -1;
	}
	
	// make a XxsMaze reference out of it
	Remote::IxxsMaze_ptr server = 
		Remote::IxxsMaze::_narrow(obj);

	XxsMazeProxy* proxy = 
		new XxsMazeProxy(server);

	//create a new xxsMazeRobot
	XxsMazeBot* thePlayer = new XxsMazeBot(proxy, mapfile, robotName);

	//create the client object
	//PlayerAdapter* client = new PlayerAdapter(thePlayer);

	// activate implemented object...
	//PortableServer::ObjectId_var oid = poa->activate_object(client);
	// ...and get a reference to it (for naming service)
	//CORBA::Object_var client_ref = poa->id_to_reference(oid.in());
	
	// activate the manager...
	mgr->activate();

	thePlayer->startup();

	// here comes the main-loop
	do {
		// if the ORB has work to do let him
		while (orb->work_pending()) {
			// remember: either orb->run() 
			// or orb->perform_work() !
			orb->perform_work();
		}

		// server already started the game and we 
		// haven't send our changePos() request yet...
		thePlayer->makeStep();

		// get those mesages on the wire before going to sleep
		while (orb->work_pending())
			orb->perform_work();
		// wait a moment to free the CPU for other
		// processes.
		// this amount of time should be smaller
		// than the one the server waits to get
		// the update from the server in time
		usleep(10000);
	} while (thePlayer->getState() != FINISHED);

	// bye bye...
	cerr<<"reached FINISH-state"<<endl;
	// shutdown our ORB to initiate a clean exit
	orb->shutdown(TRUE);
	cerr<<"ORB shut down..."<<endl;


	// shutdown managed object
	poa->destroy(TRUE, TRUE);
	
	return 0;
}
