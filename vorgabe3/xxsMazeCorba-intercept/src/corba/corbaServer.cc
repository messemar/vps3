#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <CORBA.h>
#include <coss/CosNaming.h>

	// includes interfaces for the
#include "corba/IxxsMaze.h"
#include "corba/XxsMazeAdapter.h"
#include "corba/trace/clientTracer.h"
#include "corba/trace/serverTracer.h"
#include "PlayerProxy.h"
#include "XxsMazeMap.h"
#include "XxsMaze.h"



// easy usage of our namespaces
using namespace std;
using namespace xxsMaze;


// switch to true to see more information
static const bool TRACE_VERBOSE = false;

class MyTracer : virtual public PortableInterceptor::ORBInitializer {
public:
    MyTracer() {}
    ~MyTracer() {}

    virtual void pre_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	// register interceptors	
    	ServerTracer *sTrace = new ServerTracer("server role", TRACE_VERBOSE);
    	ClientTracer *cTrace = new ClientTracer("client role", TRACE_VERBOSE);
    	info->add_server_request_interceptor(sTrace);
    	info->add_client_request_interceptor(cTrace);
    }
    
    virtual void post_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	// nothing
    }
};


int main(int argc, char* argv[]) {
	// Interceptor must be activated before ORB-init
	MyTracer bb_init;
	PortableInterceptor::register_orb_initializer(&bb_init);
	// initialize the ORB
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");
	// get a reference to the RootPOA...
	CORBA::Object_var poaobj = orb->resolve_initial_references("RootPOA");
	PortableServer::POA_var poa = PortableServer::POA::_narrow(poaobj);
	// ...and the manager
	PortableServer::POAManager_var mgr = poa->the_POAManager();

	if (argc < 2) {
		cout << "usage: " << argv[0] << " <CORBA_ARGs> <mapfile> <clientnumber>" << endl << flush;
		exit(1);
	}
	const char* filename = argv[1];
	unsigned int num = atoi(argv[2]);

	// create the game map
	XxsMazeMap* theMap = new XxsMazeMap(filename); 
	// create the game
	XxsMaze* theGame = new xxsMaze::XxsMaze(theMap, num);
	
	// create new Server
	XxsMazeAdapter* server = new XxsMazeAdapter(theGame);

	// activate implemented object...
	PortableServer::ObjectId_var oid = poa->activate_object(server);
	// ...and get a reference to it (for naming service)
	CORBA::Object_var server_ref = poa->id_to_reference(oid.in());
	
	// get reference to naming service
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
	CosNaming::NamingContext_var nc = CosNaming::NamingContext::_narrow(nsobj);
	
	// genereate name entry to resolve
	CosNaming::Name name;
	name.length(1);
	name[0].id   = CORBA::string_dup("MasterServer");
	name[0].kind = CORBA::string_dup("xxsMazeServer");
	
	// bind server to name entry (use rebind to avoid 
	// binding to an already binded name)
	nc->rebind(name, server_ref);

	// activate the manager...
	mgr->activate();
	// ...and start the main loop 
	// here comes the main-loop
	while (theGame->getState() != FINISH) {
		// if the ORB has work to do let him
		while (orb->work_pending()) {
			// remember: either orb->run() 
			// or orb->perform_work() !
			orb->perform_work();
		}

		// clients have to be informed of gamestart 
		if (theGame->getState() == PRE_GAME) {
			//send playerlist to all clients
			theGame->start();

			// now the game starts
			//theGame->setState(IN_GAME);

		} else if (theGame->getState() == IN_GAME) {
			// send playerposlist to all clients
			theGame->sendUpdate();
		}

		/*if (theGame->getState() == LOGON) {
			// maximal player count reached ??
			if (theGame->isReady()) {
				theGame->setState(PRE_GAME);
			}
		}*/
		
		// get those mesages on the wire before going to sleep
		while (orb->work_pending())
			orb->perform_work();
		// here the server determines the round time.
		// in this case it is half a second.
		// in general this time should be quite bigger 
		// than the time the client waits to make sure 
		// the client wont lose server packets!
		usleep(500000);
	}

	// bye bye...
	cerr<<"reached FINISH-state"<<endl;
	// shutdown our ORB to initiate a clean exit
	orb->shutdown(TRUE);
	cerr<<"ORB shutdown..."<<endl;

	// shutdown managed object
	poa->destroy(TRUE, TRUE);
	
	return 0;
}
