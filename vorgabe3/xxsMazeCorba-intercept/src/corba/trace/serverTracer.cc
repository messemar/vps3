#include <iostream>
#include "corba/trace/serverTracer.h"
#include "corba/trace/corbaAnyHelper.h"

using namespace std;


void ServerTracer::printRequInfo(PortableInterceptor::ServerRequestInfo_ptr ri) {
	// operation name
	cerr << ri->operation() << "(";
	
	// arguments including type information
	Dynamic::ParameterList *arg = ri->arguments();
	for (unsigned int i = 0; i < arg->length(); i++) {
		// parameter seperation
		if (i)
			cerr << ", ";
		else
			cerr << " ";
		
		// print parameter value
		corbaAnyHelper::printAny((*arg)[i].argument);
		
		// param type (in,out,inout)
		if ((*arg)[i].mode == CORBA::PARAM_IN)
			cerr << "/*in*/ ";
		else if ((*arg)[i].mode == CORBA::PARAM_OUT)
			cerr << "/*out*/ ";
		else if ((*arg)[i].mode == CORBA::PARAM_INOUT)
			cerr << "/*inout*/ ";
	}
	
	cerr << ")";
}

void ServerTracer::destroy() {
	cerr << name() << ": destroying ServerRequestInterceptor " << endl;
}


void ServerTracer::receive_request_service_contexts(PortableInterceptor::ServerRequestInfo_ptr ri) {
	cerr << name() << ": receive_request_service_contexts : "
		<< " [ operation = " << ri->operation() << " ]" << endl;
}

void ServerTracer::receive_request(PortableInterceptor::ServerRequestInfo_ptr ri) {
	cerr << name() << ": receive_request : ";
	if (all_info)
		cerr << endl << "\t";
	
	printRequInfo(ri);
	// response expected?
	// almost anytime true. just for one-way messages
	// expect no result
	if (!ri->response_expected())
		cerr << " /* no response expected */";
	
	if (all_info) {
		CORBA::OctetSeq *oaid = ri->adapter_id();
		CORBA::OctetSeq *oid = ri->object_id();
		cerr << endl << "\t" 
			<< "[ OA = ";
		for (unsigned int i = 0; i < oaid->length(); i++)
			cerr << (*oaid)[i];
		cerr << ", ObjectID = \"";
		for (unsigned int i = 0; i < oid->length(); i++)
			cerr << (*oid)[i];
		cerr << "\", Interface = \""
			<< ri->target_most_derived_interface() << "\""
			<< ", requestId = " << ri->request_id()
			<< ", operation context = { ";
		Dynamic::RequestContext* ctx = ri->operation_context();
		for (unsigned int i = 0; i < ctx->length(); i += 2) {
			if (i) cerr << ", ";
			cerr << (*ctx)[i] << " : " << (*ctx)[i + 1];
		}
		cerr << "} ]";
	}
	
	cerr << endl;
}

void ServerTracer::send_reply(PortableInterceptor::ServerRequestInfo_ptr ri) {
	cerr << name() << ": send_reply      : ";
	if (all_info)
		cerr << endl << "\t";
	
	printRequInfo(ri);
	
	CORBA::Any *res = ri->result();
	CORBA::TypeCode_ptr t = res->type();
	if (t->unalias()->kind() != CORBA::tk_null &&
		t->unalias()->kind() != CORBA::tk_void)
	{
		cerr << " = ";
//		cerr << " /* pre printAny() */ ";
		corbaAnyHelper::printAny(res);
//		cerr << " /* post printAny() */ ";
	}

	if (all_info) {
		CORBA::OctetSeq *oaid = ri->adapter_id();
		CORBA::OctetSeq *oid = ri->object_id();
		cerr << endl << "\t" 
			<< "[ OA = ";
		for (unsigned int i = 0; i < oaid->length(); i++)
			cerr << (*oaid)[i];
		cerr << ", ObjectID = \"";
		for (unsigned int i = 0; i < oid->length(); i++)
			cerr << (*oid)[i];
		cerr << ", replyStatus = ";
		switch (ri->reply_status()) {
		case PortableInterceptor::SUCCESSFUL:		cerr << "SUCCESSFUL"; break;
		case PortableInterceptor::SYSTEM_EXCEPTION:	cerr << "SYSTEM_EXCEPTION"; break;
		case PortableInterceptor::USER_EXCEPTION:	cerr << "USER_EXCEPTION"; break;
		case PortableInterceptor::LOCATION_FORWARD:	cerr << "LOCATION_FORWARD"; break;
		case PortableInterceptor::TRANSPORT_RETRY:	cerr << "TRANSPORT_RETRY"; break;
//		case PortableInterceptor::UNKNOWN:			cerr << "UNKNOWN"; break;
		}
		cerr << ", requestId = " << ri->request_id()
			<< ", operation context = { ";
		Dynamic::RequestContext* ctx = ri->operation_context();
		for (unsigned int i = 0; i < ctx->length(); i += 2) {
			if (i) cerr << ", ";
			cerr << (*ctx)[i] << " : " << (*ctx)[i + 1];
		}
		cerr << "} ]";
	}
	
	cerr << endl;
}

void ServerTracer::send_exception( PortableInterceptor::ServerRequestInfo_ptr ri) {
	const char *op = ri->operation();
	CORBA::Any *ex = ri->sending_exception();
	CORBA::TypeCode_ptr tc = ex->type();
	const char *exid = tc->id();
	
	cerr << name() << ": send_exception  : ";
	cerr << " [ operation = " << op
		<< ", replyStatus = ";
	switch (ri->reply_status()) {
	case PortableInterceptor::SUCCESSFUL:		cerr << "SUCCESSFUL"; break;
	case PortableInterceptor::SYSTEM_EXCEPTION:	cerr << "SYSTEM_EXCEPTION"; break;
	case PortableInterceptor::USER_EXCEPTION:	cerr << "USER_EXCEPTION"; break;
	case PortableInterceptor::LOCATION_FORWARD:	cerr << "LOCATION_FORWARD"; break;
	case PortableInterceptor::TRANSPORT_RETRY:	cerr << "TRANSPORT_RETRY"; break;
//	case PortableInterceptor::UNKNOWN:			cerr << "UNKNOWN"; break;
	}
	cerr << ", exception ID = " << exid 
		<< ", exception = ";
	corbaAnyHelper::printAny(ex);
	if (all_info)
		cerr << ", requestId = " << ri->request_id();
	cerr << " ]" << endl;
}

void ServerTracer::ServerTracer::send_other( PortableInterceptor::ServerRequestInfo_ptr ri) {
	const char *op = ri->operation();
	
	cerr << name() << ": send_other      : ";
	cerr << " [ operation = " << op
		<< ", replyStatus = ";
	switch (ri->reply_status()) {
	case PortableInterceptor::SUCCESSFUL:		cerr << "SUCCESSFUL"; break;
	case PortableInterceptor::SYSTEM_EXCEPTION:	cerr << "SYSTEM_EXCEPTION"; break;
	case PortableInterceptor::USER_EXCEPTION:	cerr << "USER_EXCEPTION"; break;
	case PortableInterceptor::LOCATION_FORWARD:	cerr << "LOCATION_FORWARD"; break;
	case PortableInterceptor::TRANSPORT_RETRY:	cerr << "TRANSPORT_RETRY"; break;
//	case PortableInterceptor::UNKNOWN:			cerr << "UNKNOWN"; break;
	}
	if (ri->reply_status() == PortableInterceptor::LOCATION_FORWARD) {
// 		CORBA::Object_ptr fr = ri->forward_reference();
		cerr << ", forwardReference = \"";
//		printAny(fr);
		cerr << "\"";
	}
	if (all_info)
		cerr << ", requestId = " << ri->request_id();
	cerr << " ]" << endl;
}
