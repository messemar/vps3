#include <iostream>
#include "corba/trace/clientTracer.h"
#include "corba/trace/corbaAnyHelper.h"

using namespace std;


void ClientTracer::printRequInfo(PortableInterceptor::ClientRequestInfo_ptr ri) {
	// operation name
	cerr << ri->operation() << "(";
	
	// arguments including type information
	Dynamic::ParameterList *arg = ri->arguments();
	for (unsigned int i = 0; i < arg->length(); i++) {
		// parameter seperation
		if (i)
			cerr << ", ";
		else
			cerr << " ";
		
		// print parameter value
		corbaAnyHelper::printAny((*arg)[i].argument);
		
		// param type (in,out,inout)
		if ((*arg)[i].mode == CORBA::PARAM_IN)
			cerr << "/*in*/ ";
		else if ((*arg)[i].mode == CORBA::PARAM_OUT)
			cerr << "/*out*/ ";
		else if ((*arg)[i].mode == CORBA::PARAM_INOUT)
			cerr << "/*inout*/ ";
	}
	
	cerr << ")";
}
	
void ClientTracer::destroy() {
	cerr << name() << ": destroying ClientRequestInterceptor " << endl;
}

void ClientTracer::send_request(PortableInterceptor::ClientRequestInfo_ptr ri) {
	cerr << name() << ": send_request  : ";
	if (all_info)
		cerr << endl << "\t";
	printRequInfo(ri);

	// response expected?
	// almost anytime true. just for one-way messages
	// expect no result
	if (!ri->response_expected())
		cerr << " /* no response expected */";
	
	if (all_info) {
		const char *repoid = ri->target()->_repoid();
		cerr << endl << "\t" 
			<< "[ repoid = \"" << repoid << "\""
			<< ", requestId = " << ri->request_id()
			<< ", operation context = { ";
		Dynamic::RequestContext* ctx = ri->operation_context();
		for (unsigned int i = 0; i < ctx->length(); i += 2) {
			if (i) cerr << ", ";
			cerr << (*ctx)[i] << " : " << (*ctx)[i + 1];
		}
		cerr << "} ]";
	}
	
	cerr << endl;
}

void ClientTracer::send_poll(PortableInterceptor::ClientRequestInfo_ptr ri) {
	const char *op = ri->operation();
	const char *repoid = ri->target()->_repoid(); 
	
	cerr << name() << ": send_poll     : "
		<< " [ operation = " << op
		<< ", repoid = \"" << repoid << "\" ]" << endl;
}

void ClientTracer::receive_reply(PortableInterceptor::ClientRequestInfo_ptr ri) {
	    cerr << name() << ": receive_reply : ";
    if (all_info)
    	cerr << endl << "\t";
    printRequInfo(ri);
	
    CORBA::Any *res = ri->result();
    CORBA::TypeCode_ptr t = res->type();
    if (t->unalias()->kind() != CORBA::tk_null &&
    t->unalias()->kind() != CORBA::tk_void)
    {
    	cerr << " = ";
// 		cerr << " /* pre printAny() */ ";
    	corbaAnyHelper::printAny(res);
//    	cerr << " /* post printAny() */ ";
    }

    if (all_info) {
    	const char *repoid = ri->target()->_repoid();
    	cerr << endl << "\t" 
	    	<< "[ replyStatus = ";
	    switch (ri->reply_status()) {
	    case PortableInterceptor::SUCCESSFUL:		cerr << "SUCCESSFUL"; break;
	    case PortableInterceptor::SYSTEM_EXCEPTION:	cerr << "SYSTEM_EXCEPTION"; break;
	    case PortableInterceptor::USER_EXCEPTION:	cerr << "USER_EXCEPTION"; break;
	    case PortableInterceptor::LOCATION_FORWARD:	cerr << "LOCATION_FORWARD"; break;
	    case PortableInterceptor::TRANSPORT_RETRY:	cerr << "TRANSPORT_RETRY"; break;
//	    case PortableInterceptor::UNKNOWN:			cerr << "UNKNOWN"; break;
	    }
	    cerr << ", repoid = \"" << repoid << "\""
	    	<< ", requestId = " << ri->request_id()
	    	<< ", operation context = { ";
	    Dynamic::RequestContext* ctx = ri->operation_context();
	    for (unsigned int i = 0; i < ctx->length(); i += 2) {
		    if (i) cerr << ", ";
		    cerr << (*ctx)[i] << " : " << (*ctx)[i + 1];
	    }
	    cerr << "} ]";
    }

    cerr << endl;
}

void ClientTracer::receive_exception(PortableInterceptor::ClientRequestInfo_ptr ri) {
	const char *op = ri->operation();
	const char *exid = ri->received_exception_id();
	CORBA::Any *ex = ri->received_exception();
	const char *repoid = ri->target()->_repoid(); 
	
	cerr << name() << ": receive_exception : ";
	cerr << " [ operation = " << op
		<< ", replyStatus = ";
	switch (ri->reply_status()) {
	case PortableInterceptor::SUCCESSFUL:		cerr << "SUCCESSFUL"; break;
	case PortableInterceptor::SYSTEM_EXCEPTION:	cerr << "SYSTEM_EXCEPTION"; break;
	case PortableInterceptor::USER_EXCEPTION:	cerr << "USER_EXCEPTION"; break;
	case PortableInterceptor::LOCATION_FORWARD:	cerr << "LOCATION_FORWARD"; break;
	case PortableInterceptor::TRANSPORT_RETRY:	cerr << "TRANSPORT_RETRY"; break;
//	case PortableInterceptor::UNKNOWN:			cerr << "UNKNOWN"; break;
	}
	cerr << ", exception ID = " << exid 
		<< ", exception = ";
	corbaAnyHelper::printAny(ex);
	if (all_info)
		cerr << ", requestId = " << ri->request_id();
	cerr << ", repoid = \"" << repoid << "\" ]" << endl;
}

void ClientTracer::receive_other(PortableInterceptor::ClientRequestInfo_ptr ri) {
      const char *op = ri->operation();
    //const char *exid = ri->received_exception_id(); // not allowed in receive_other --> see Corba sec
    const char *repoid = ri->target()->_repoid(); 

    cerr << name() << ": receive_other : ";
    cerr << " [ operation = " << op
	<< ", replyStatus = ";
    switch (ri->reply_status()) {
        case PortableInterceptor::SUCCESSFUL:		cerr << "SUCCESSFUL"; break;
        case PortableInterceptor::SYSTEM_EXCEPTION:	cerr << "SYSTEM_EXCEPTION"; break;
        case PortableInterceptor::USER_EXCEPTION:	cerr << "USER_EXCEPTION"; break;
        case PortableInterceptor::LOCATION_FORWARD:	cerr << "LOCATION_FORWARD"; break;
        case PortableInterceptor::TRANSPORT_RETRY:	cerr << "TRANSPORT_RETRY"; break;
//	    case PortableInterceptor::UNKNOWN:			cerr << "UNKNOWN"; break;
    }
    //cerr << ", exception ID = " << exid ;// see exid declaration
    if (all_info)
    	cerr << ", requestId = " << ri->request_id();
    cerr << ", repoid = \"" << repoid << "\" ]" << endl;
}
