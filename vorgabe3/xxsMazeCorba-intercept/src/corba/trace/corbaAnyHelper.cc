#include <iostream>
#include "corba/trace/corbaAnyHelper.h"

using namespace std;


void corbaAnyHelper::printAny(CORBA::Any& a) {
	CORBA::TypeCode_ptr t = a.type();
	switch (t->unalias()->kind()) {
	case CORBA::tk_null:
		cerr << "(NULL)";
		break;
	case CORBA::tk_void:
		cerr << "(void)";
		break;
	case CORBA::tk_short:
		cerr << "(short)";
		_printhelper<CORBA::Short>(a);
		break;
	case CORBA::tk_long:
		cerr << "(long)";
		_printhelper<CORBA::Long>(a);
		break;
	case CORBA::tk_longlong:
		cerr << "(long long)";
		_printhelper<CORBA::LongLong>(a);
		break;
	case CORBA::tk_ushort:
		cerr << "(unsigned short)";
		_printhelper<CORBA::UShort>(a);
		break;
	case CORBA::tk_ulong:
		cerr << "(unsigned long)";
		_printhelper<CORBA::ULong>(a);
		break;
	case CORBA::tk_ulonglong:
		cerr << "(unsigned long long)";
		_printhelper<CORBA::ULongLong>(a);
		break;
	case CORBA::tk_float:
		cerr << "(float)";
		_printhelper<CORBA::Float>(a);
		break;
	case CORBA::tk_double:
		cerr << "(double)";
		_printhelper<CORBA::Double>(a);
		break;
	case CORBA::tk_string:
		cerr << "(char*)";
		cerr << '"';
		_printhelper<const char*>(a);
		cerr << '"';
		break;
	case CORBA::tk_char:
		cerr << "(char)";
		cerr << "'";
		_printhelper<CORBA::Char>(a);
		cerr << "'";
		break;
	case CORBA::tk_objref:
		cerr << "(CORBA::Object_ptr)";
		_printhelper<CORBA::Object_ptr>(a);
		break;
	// there are plenty more types which are
	// possible but I think the handled ones 
	// are enougth and the rest are a little
	// complex th handle ;)
	default:
		//cerr << "<unhandled type [name=" << t->unalias()->name() << "]>";
		cerr << "<unhandled type>";
	}
}


void corbaAnyHelper::printAny(CORBA::Any* a) {
	if (a)
		printAny(*a);
	else
		cerr << "<null>";
}

