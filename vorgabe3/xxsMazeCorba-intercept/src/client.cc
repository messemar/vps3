#include "XxsMazeBot.h"
#include "XxsMazeClient.h"
#include "RemoteXxsMaze.h"
#include "XxsMazeMap.h"
#include "corba/trace/clientTracer.h"
#include "corba/trace/serverTracer.h"
#include "corba/service/clientPWDService.h"
#include "corba/service/serverPWDService.h"

#include <cstdlib>
#include <cstring>
#include <cstdio>

#include <CORBA.h>
#include <coss/CosNaming.h>

static const bool TRACE_VERBOSE=false;

class MyTracer 
: virtual public PortableInterceptor::ORBInitializer
, virtual public CORBA::LocalObject {
public:
    MyTracer() {}
    ~MyTracer() {}

    virtual void pre_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	// register interceptors	
    	//ServerTracer *sTrace = new ServerTracer("server role", TRACE_VERBOSE);
    	//ClientTracer *cTrace = new ClientTracer("client role", TRACE_VERBOSE);
    	//info->add_server_request_interceptor(sTrace);
    	//info->add_client_request_interceptor(cTrace);
        //ClientPWDService * clientService = new ClientPWDService();
        ClientPWDService * clientService = new ClientPWDService(47114711,"name","halloooo");
        ServerPWDService * serverService = new ServerPWDService(47124712,"server role","47124712");
        info->add_client_request_interceptor(clientService);
        info->add_server_request_interceptor(serverService);
    }
    
    virtual void post_init(PortableInterceptor::ORBInitInfo_ptr info) {
    	// nothing
    }
};

int main(int argc, char* argv[]) {
	if (argc < 3) {
		cerr << "usage: xxsMaze  <mapfile> <clientname> <corba> " << endl;
		exit(4711);
	}
	char* mapfile = argv[1];

	// create an instance of the map and the game
	XxsMazeMap theMap(mapfile);
    // tracer
   MyTracer *bb_init= new MyTracer();
   PortableInterceptor::register_orb_initializer(bb_init); 
	//create orb
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");
	std::cout<< " ORB initialised"<<std::endl;
	//get remote game infos
	char* cname= argv[2];
	std::cout<< " name created"<<std::endl;
	
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
		CosNaming::NamingContext_var nc= CosNaming::NamingContext::_narrow(nsobj);
		CosNaming::Name name;
		name.length(1);
		name[0].id= CORBA::string_dup("XxsMaze");
		name[0].kind=CORBA::string_dup("");
		CORBA::Object_var obj = nc->resolve(name);
		Server_var sgame= Server::_narrow(obj);
	RemoteXxsMaze* game= new RemoteXxsMaze(orb,sgame,(char*)cname);
	XxsMazeBot* bot= new XxsMazeBot(game,mapfile,cname);
	std::cout<< " bot created"<<std::endl;
	// create the corresponding client instances and start it
	XxsMazeBotClient* client = new XxsMazeBotClient(bot);
	std::cout<< " clients Created"<<std::endl;
	client->start();
	orb->run();
	std::cout<< "clients started"<<std::endl;
	client->join();
	
	delete bot;
	delete client;

	

}
