#include "XxsMazeClient.h"
#include "XxsMazeBot.h"
#include "XxsMazeHuman.h"

void XxsMazeBotClient::run()
{
	// code for handling a robot player
	
	// first of all logon
	cerr<<"logon as '"<<myBot->getName()<<"'...";cerr.flush();
	//PlayerId id = game->join(myBot);
	myBot->startup();
	//cerr<<"done(ID="<<id<<")"<<endl;
	
	// wait for the server to begin the game
	cerr<<"waiting for server to start the game...";cerr.flush();
	//myBot->setState(START_WAIT);

	PlayerState state;

	// here comes the main-loop
	do {
		state = myBot->getState();		
		switch(state) {
			case GAMEING:
				// we're allowed to make a move
				myBot->makeStep();
				break;
			case START_WAIT:
				//cerr << "." << flush;
				break;
			case WAIT4UPDATE:
				// still waiting for the server to
				// start the next round
				//cerr <<"+" << flush;
			default:
				break;
		}
		// wait a moment before doing the next move
		// this asumes that the server sends a reply
		// before this amount of time is over.
		// if not we're still in the state WAIT4UPDATE
		// and goning to sleep again.
		//usleep(10000);
	}	while (state != FINISHED);
	
	// bye bye...
	cerr<<"reached FINISH-state"<<endl;
}
