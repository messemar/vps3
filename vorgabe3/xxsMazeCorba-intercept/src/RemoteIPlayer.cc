#include "RemoteIPlayer.h"

void RemoteIPlayer::startGame(const PlayerPositions pos){
	SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos;
	ppos.length(pos.numPlayers());
	std::cout<<"fill points"<< std::endl;
	for(int i=0;i<pos.numPlayers();i++){
		Point p = pos.getPos(i);
		ppos[i].xPos=(CORBA::ULong) p.x;
		ppos[i].yPos=(CORBA::ULong) p.y;
		ppos[i].id=i;
	}
	client->c_startGame(ppos);
	std::cout<<"client started"<< std::endl;
}	

void RemoteIPlayer::update(const PlayerPositions pos){
	SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos;
	ppos.length(pos.numPlayers());
	for(int i=0;i<pos.numPlayers();i++){
		Point p = pos.getPos(i);
		ppos[i].xPos=(CORBA::ULong) p.x;
		ppos[i].yPos=(CORBA::ULong) p.y;
		ppos[i].id=i;
	}
	client->c_update(ppos);
	std::cout<<"client updated"<< std::endl;
}

void RemoteIPlayer::nextMove(){
	client->c_nextMove();
	std::cout<<"client nextMove"<< std::endl;
}

void RemoteIPlayer::finish(){
	client->c_finish();
}	
