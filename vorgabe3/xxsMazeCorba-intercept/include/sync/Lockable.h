#ifndef Lockable_h
#define Lockable_h

//#include "sync/Mutex.h"

template <class T> 
class Lockable {
	public:
		explicit Lockable(T* other)
		{
			ptr = other;
		}

		Lockable(const Lockable<T>& other) 
		{
			ptr = other.getPtr();
			ptr->lock();
		}
			
		
		~Lockable() { ptr->unlock(); }

		T* operator->() const
		{
			return ptr;
		}

		void setPtr(T* other)
		{
			ptr = other;
		}
		
		T* getPtr() const
		{
			return ptr;
		}

	private:
		T* ptr;
};
#endif
