#ifndef CLIENT_IMPL_h
#define CLIENT_IMPL_h

#include "corba/client.h"
#include "IPlayer.h"

#include <CORBA.h>
#include <coss/CosNaming.h>

class Client_impl: virtual public POA_Client{
public:

		Client_impl(IPlayer* ip): ip(ip){}

		void c_startGame( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos );

		void c_update( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos );

		void c_nextMove();

		void c_finish();

private:

	IPlayer* ip;


};
#endif
