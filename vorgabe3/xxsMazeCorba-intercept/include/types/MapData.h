#ifndef types_MapData_h
#define types_MapData_h

#include <vector>
#include "types/MapChar.h"

// internal representation of the map is a 2-dimensional vector
typedef std::vector< std::vector< MapChar> > MapData;
#endif
