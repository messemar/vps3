#ifndef types_PlayerList_h
#define types_PlayerList_h

#include <map>
#include "types/PlayerId.h"
#include "IPlayer.h"

class PlayerList {
public:
	void add(PlayerId id, IPlayer* pl);
	void remove(PlayerId id);
	const std::size_t numPlayers() const; 
	IPlayer* operator[](PlayerId);

private:
	typedef std::map<PlayerId,IPlayer*>::const_iterator const_iterator;
	typedef std::map<PlayerId,IPlayer*>::iterator iterator;
	std::map<PlayerId, IPlayer*> playerMap;
};


inline
void PlayerList::add(PlayerId id, IPlayer* pl)
{
	playerMap[id] = pl;
}

inline
void PlayerList::remove(PlayerId id)
{
	iterator it = playerMap.find(id);
	if ( it != playerMap.end()) {
		playerMap.erase(it);
	}
}

inline
const std::size_t PlayerList::numPlayers () const 
{
	return playerMap.size();
}

inline
IPlayer* PlayerList::operator[](PlayerId id)
{
	return playerMap[id];
}
#endif
