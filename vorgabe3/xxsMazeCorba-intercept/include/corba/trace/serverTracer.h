#ifndef __CORBA_SERVERTRACER_H__
#define __CORBA_SERVERTRACER_H__

#include <CORBA.h>
#include <string>


class ServerTracer:  virtual public PortableInterceptor::ServerRequestInterceptor,
				virtual public CORBA::LocalObject {
	std::string my_name;	// name to identify the interceptor
	bool all_info;	// be a little more verbose

	// another helper function for easy printing of the
	// ServerRequestInfo - speak the operation
	void printRequInfo(PortableInterceptor::ServerRequestInfo_ptr ri);
	
public:
	ServerTracer(char* name="", bool verbose=false) : my_name(name), all_info(verbose) { }
	
	//
	// here comes the the interface we need to implement
	//

	char* name() {
		return CORBA::string_dup(my_name.c_str());
	}

	void destroy();

    void receive_request_service_contexts(PortableInterceptor::ServerRequestInfo_ptr ri);
    
    void receive_request(PortableInterceptor::ServerRequestInfo_ptr ri);
    
    void send_reply(PortableInterceptor::ServerRequestInfo_ptr ri);

    void send_exception( PortableInterceptor::ServerRequestInfo_ptr ri);
		
    void send_other( PortableInterceptor::ServerRequestInfo_ptr ri);
};
#endif
