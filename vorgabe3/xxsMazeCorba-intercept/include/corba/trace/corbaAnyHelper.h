#ifndef __corbaAnyHelper__
#define __corbaAnyHelper__

#include <CORBA.h>

using namespace std;


// little helper class to have an easyier handling
// of the CORBA::Any type
class corbaAnyHelper {
public:

	// helper function to get any type out of 
	// CORBA::Any and print it to stderr
	template<typename T>
	static void _printhelper(CORBA::Any& a) {
		T v;
		a >>= v;
		cerr << v;
	}
	
	// print value stored in CORBA::Any-type 
	// with preceding type-cast information
	static void printAny(CORBA::Any& a);
	
	// to process pointers the right way
	static void printAny(CORBA::Any* a);
};

#endif

