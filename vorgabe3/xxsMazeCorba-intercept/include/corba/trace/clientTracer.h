#ifndef __CORBA_CLIENTTRACER_H__
#define __CORBA_CLIENTTRACER_H__

#include <CORBA.h>
#include <string>


class ClientTracer: virtual public  PortableInterceptor::ClientRequestInterceptor,
	virtual public CORBA::LocalObject {
	std::string my_name;	// name to identify the interceptor
	bool all_info;	// be a little more verbose

	// another helper function for easy printing of the
	// ClientRequestInfo - speak the operation
	void printRequInfo(PortableInterceptor::ClientRequestInfo_ptr ri);
	
public:
	ClientTracer(char* name="", bool verbose=false) : my_name(name), all_info(verbose) { }
	
	//
	// here comes the the interface we need to implement
	//

	char* name() {
		return CORBA::string_dup(my_name.c_str());
	}

	void destroy();

	void send_request(PortableInterceptor::ClientRequestInfo_ptr ri);
    
	void send_poll(PortableInterceptor::ClientRequestInfo_ptr ri);
    
	void receive_reply(PortableInterceptor::ClientRequestInfo_ptr ri);
    
	void receive_exception(PortableInterceptor::ClientRequestInfo_ptr ri);
    
	void receive_other(PortableInterceptor::ClientRequestInfo_ptr ri);
};
#endif
