#ifndef __CORBA_ClientPWDService_h__
#define __CORBA_clientPWDService_h__

#include <CORBA.h>
#include <string>

using namespace std;

class ClientPWDService
    : virtual public PortableInterceptor::ClientRequestInterceptor
    , virtual public CORBA::LocalObject
{
    std::string my_name;
    public:

        ClientPWDService(unsigned long iID=47114711,char* name="",char* passwd="47114711"): iID(iID),  my_name(name){
        // we only allow 8 characters
        pwd=new CORBA::OctetSeq(8);
        pwd->length(8);
        for(int i=0;i<8;i++){
            (*pwd)[i]=passwd[i];
        }
} // we have to initialize password
    
    char* name(){
        return CORBA::string_dup(my_name.c_str());
    }    

    void destroy(){
        cerr<<" destroying ClientPWDService"<<endl;
    }

    void send_request(PortableInterceptor::ClientRequestInfo_ptr ri){
        IOP::ServiceContext sc;
        sc.context_id=iID;
        sc.context_data=*pwd;
        ri->add_request_service_context(sc,true); // if not true we get an error, if sc already exists
        std::cout<<" service context added"<<endl;
    }
    
    void send_poll(PortableInterceptor::ClientRequestInfo_ptr){}
       
	void receive_reply(PortableInterceptor::ClientRequestInfo_ptr ri)
    {
        std::cerr<<"reply received"<<std::endl;
    }
    
	void receive_exception(PortableInterceptor::ClientRequestInfo_ptr ri)
    {
		char *op = ri->operation();
		cout << name() << ": " << "receive_eception, repoid = " << ri->target()->_repoid() << " operation = " << op << endl;
		char *exid = ri->received_exception_id();
		if(strcmp("IDL:omg.org/CORBA/NO_PERMISSION:1.0", exid) == 0)
		{
			cerr << "Wrong Password! Check your credentials and try again!" << endl;
			//exit(0);
		}
	}
    
	void receive_other(PortableInterceptor::ClientRequestInfo_ptr ri){
        std::cerr<<"other received"<<std::endl;
    }
        

    private:
     
    unsigned long iID;
    CORBA::OctetSeq * pwd;
};
#endif
