#ifndef __CORBA_ServerPWDService_h__
#define __CORBA_ServerPWDService_h__

#include <CORBA.h>
#include <string>

class ServerPWDService
    : virtual public PortableInterceptor::ServerRequestInterceptor
    , virtual public CORBA::LocalObject{
    
    std::string my_name;

    public:
        ServerPWDService(unsigned long passwdID=47114711,char* name="",char* pwd="47114711"):passwdID(passwdID), my_name(name), pwd(pwd){}// init password
   
    char* name() {
		return CORBA::string_dup(my_name.c_str());
    }

  	void destroy(){}

    void receive_request_service_contexts(PortableInterceptor::ServerRequestInfo_ptr ri){
        IOP::ServiceContext *sc=ri->get_request_service_context(passwdID);
        // only 8 characters in password ar allowed;
        for(int i=0;i<8;i++){
           // std::cerr<<" passwd char: "<< pwd[i]<< "transmitted char: "<< (char) sc->context_data[i]<<std::endl;// for password check only
            if(sc->context_data[i]!=pwd[i]){
                std::cerr<<" wrong password is Transmitted"<<std::endl;
               //ri->sending_exception();
               mico_throw(CORBA::NO_PERMISSION()); 
            }
        }

    }
    
    void receive_request(PortableInterceptor::ServerRequestInfo_ptr ri){}
    
    void send_reply(PortableInterceptor::ServerRequestInfo_ptr ri){
        std::cerr<<"send reply"<<std::endl;
    }

    void send_exception( PortableInterceptor::ServerRequestInfo_ptr ri)
    {
        std::cerr<<"send exception"<<std::endl;
    }
		
    void send_other( PortableInterceptor::ServerRequestInfo_ptr ri)
    {
        std::cerr<<"send other"<<std::endl;
    }
    
    private:
        char* pwd;
        unsigned long passwdID;
};
#endif
