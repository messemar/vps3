/*
 *  MICO --- an Open Source CORBA implementation
 *  Copyright (c) 1997-2006 by The Mico Team
 *
 *  This file was automatically generated. DO NOT EDIT!
 */

#include <corba/client.h>


using namespace std;

//--------------------------------------------------------
//  Implementation of stubs
//--------------------------------------------------------

/*
 * Base interface for class Client
 */

Client::~Client()
{
}

void *
Client::_narrow_helper( const char *_repoid )
{
  if( strcmp( _repoid, "IDL:Client:1.0" ) == 0 )
    return (void *)this;
  return NULL;
}

Client_ptr
Client::_narrow( CORBA::Object_ptr _obj )
{
  Client_ptr _o;
  if( !CORBA::is_nil( _obj ) ) {
    void *_p;
    if( (_p = _obj->_narrow_helper( "IDL:Client:1.0" )))
      return _duplicate( (Client_ptr) _p );
    if (!strcmp (_obj->_repoid(), "IDL:Client:1.0") || _obj->_is_a_remote ("IDL:Client:1.0")) {
      _o = new Client_stub;
      _o->CORBA::Object::operator=( *_obj );
      return _o;
    }
  }
  return _nil();
}

Client_ptr
Client::_narrow( CORBA::AbstractBase_ptr _obj )
{
  return _narrow (_obj->_to_object());
}

class _Marshaller_Client : public ::CORBA::StaticTypeInfo {
    typedef Client_ptr _MICO_T;
  public:
    ~_Marshaller_Client();
    StaticValueType create () const;
    void assign (StaticValueType dst, const StaticValueType src) const;
    void free (StaticValueType) const;
    void release (StaticValueType) const;
    ::CORBA::Boolean demarshal (::CORBA::DataDecoder&, StaticValueType) const;
    void marshal (::CORBA::DataEncoder &, StaticValueType) const;
};


_Marshaller_Client::~_Marshaller_Client()
{
}

::CORBA::StaticValueType _Marshaller_Client::create() const
{
  return (StaticValueType) new _MICO_T( 0 );
}

void _Marshaller_Client::assign( StaticValueType d, const StaticValueType s ) const
{
  *(_MICO_T*) d = ::Client::_duplicate( *(_MICO_T*) s );
}

void _Marshaller_Client::free( StaticValueType v ) const
{
  ::CORBA::release( *(_MICO_T *) v );
  delete (_MICO_T*) v;
}

void _Marshaller_Client::release( StaticValueType v ) const
{
  ::CORBA::release( *(_MICO_T *) v );
}

::CORBA::Boolean _Marshaller_Client::demarshal( ::CORBA::DataDecoder &dc, StaticValueType v ) const
{
  ::CORBA::Object_ptr obj;
  if (!::CORBA::_stc_Object->demarshal(dc, &obj))
    return FALSE;
  *(_MICO_T *) v = ::Client::_narrow( obj );
  ::CORBA::Boolean ret = ::CORBA::is_nil (obj) || !::CORBA::is_nil (*(_MICO_T *)v);
  ::CORBA::release (obj);
  return ret;
}

void _Marshaller_Client::marshal( ::CORBA::DataEncoder &ec, StaticValueType v ) const
{
  ::CORBA::Object_ptr obj = *(_MICO_T *) v;
  ::CORBA::_stc_Object->marshal( ec, &obj );
}

::CORBA::StaticTypeInfo *_marshaller_Client;


/*
 * Stub interface for class Client
 */

Client_stub::~Client_stub()
{
}

#ifndef MICO_CONF_NO_POA

void *
POA_Client::_narrow_helper (const char * repoid)
{
  if (strcmp (repoid, "IDL:Client:1.0") == 0) {
    return (void *) this;
  }
  return NULL;
}

POA_Client *
POA_Client::_narrow (PortableServer::Servant serv) 
{
  void * p;
  if ((p = serv->_narrow_helper ("IDL:Client:1.0")) != NULL) {
    serv->_add_ref ();
    return (POA_Client *) p;
  }
  return NULL;
}

Client_stub_clp::Client_stub_clp ()
{
}

Client_stub_clp::Client_stub_clp (PortableServer::POA_ptr poa, CORBA::Object_ptr obj)
  : CORBA::Object(*obj), PortableServer::StubBase(poa)
{
}

Client_stub_clp::~Client_stub_clp ()
{
}

#endif // MICO_CONF_NO_POA

void Client_stub::startGame( SequenceTmpl< PlayerPosition,MICO_TID_DEF> _par_ppos )
{
  CORBA::StaticAny _sa_ppos( _marshaller__seq_PlayerPosition, &_par_ppos );
  CORBA::StaticRequest __req( this, "startGame" );
  __req.add_in_arg( &_sa_ppos );

  __req.invoke();

  mico_sii_throw( &__req, 
    0);
}


#ifndef MICO_CONF_NO_POA

void
Client_stub_clp::startGame( SequenceTmpl< PlayerPosition,MICO_TID_DEF> _par_ppos )
{
  PortableServer::Servant _serv = _preinvoke ();
  if (_serv) {
    POA_Client * _myserv = POA_Client::_narrow (_serv);
    if (_myserv) {
      #ifdef HAVE_EXCEPTIONS
      try {
      #endif
        _myserv->startGame(_par_ppos);
      #ifdef HAVE_EXCEPTIONS
      }
      catch (...) {
        _myserv->_remove_ref();
        _postinvoke();
        throw;
      }
      #endif

      _myserv->_remove_ref();
      _postinvoke ();
      return;
    }
    _postinvoke ();
  }

  Client_stub::startGame(_par_ppos);
}

#endif // MICO_CONF_NO_POA

void Client_stub::update( SequenceTmpl< PlayerPosition,MICO_TID_DEF> _par_ppos )
{
  CORBA::StaticAny _sa_ppos( _marshaller__seq_PlayerPosition, &_par_ppos );
  CORBA::StaticRequest __req( this, "update" );
  __req.add_in_arg( &_sa_ppos );

  __req.oneway();

  mico_sii_throw( &__req, 
    0);
}


#ifndef MICO_CONF_NO_POA

void
Client_stub_clp::update( SequenceTmpl< PlayerPosition,MICO_TID_DEF> _par_ppos )
{
  PortableServer::Servant _serv = _preinvoke ();
  if (_serv) {
    POA_Client * _myserv = POA_Client::_narrow (_serv);
    if (_myserv) {
      _myserv->update(_par_ppos);
      _myserv->_remove_ref();
      _postinvoke ();
      return;
    }
    _postinvoke ();
  }

  Client_stub::update(_par_ppos);
}

#endif // MICO_CONF_NO_POA

void Client_stub::nextMove()
{
  CORBA::StaticRequest __req( this, "nextMove" );

  __req.invoke();

  mico_sii_throw( &__req, 
    0);
}


#ifndef MICO_CONF_NO_POA

void
Client_stub_clp::nextMove()
{
  PortableServer::Servant _serv = _preinvoke ();
  if (_serv) {
    POA_Client * _myserv = POA_Client::_narrow (_serv);
    if (_myserv) {
      #ifdef HAVE_EXCEPTIONS
      try {
      #endif
        _myserv->nextMove();
      #ifdef HAVE_EXCEPTIONS
      }
      catch (...) {
        _myserv->_remove_ref();
        _postinvoke();
        throw;
      }
      #endif

      _myserv->_remove_ref();
      _postinvoke ();
      return;
    }
    _postinvoke ();
  }

  Client_stub::nextMove();
}

#endif // MICO_CONF_NO_POA

void Client_stub::finish()
{
  CORBA::StaticRequest __req( this, "finish" );

  __req.invoke();

  mico_sii_throw( &__req, 
    0);
}


#ifndef MICO_CONF_NO_POA

void
Client_stub_clp::finish()
{
  PortableServer::Servant _serv = _preinvoke ();
  if (_serv) {
    POA_Client * _myserv = POA_Client::_narrow (_serv);
    if (_myserv) {
      #ifdef HAVE_EXCEPTIONS
      try {
      #endif
        _myserv->finish();
      #ifdef HAVE_EXCEPTIONS
      }
      catch (...) {
        _myserv->_remove_ref();
        _postinvoke();
        throw;
      }
      #endif

      _myserv->_remove_ref();
      _postinvoke ();
      return;
    }
    _postinvoke ();
  }

  Client_stub::finish();
}

#endif // MICO_CONF_NO_POA

class _Marshaller__seq_PlayerPosition : public ::CORBA::StaticTypeInfo {
    typedef SequenceTmpl< PlayerPosition,MICO_TID_DEF> _MICO_T;
  public:
    ~_Marshaller__seq_PlayerPosition();
    StaticValueType create () const;
    void assign (StaticValueType dst, const StaticValueType src) const;
    void free (StaticValueType) const;
    ::CORBA::Boolean demarshal (::CORBA::DataDecoder&, StaticValueType) const;
    void marshal (::CORBA::DataEncoder &, StaticValueType) const;
};


_Marshaller__seq_PlayerPosition::~_Marshaller__seq_PlayerPosition()
{
}

::CORBA::StaticValueType _Marshaller__seq_PlayerPosition::create() const
{
  return (StaticValueType) new _MICO_T;
}

void _Marshaller__seq_PlayerPosition::assign( StaticValueType d, const StaticValueType s ) const
{
  *(_MICO_T*) d = *(_MICO_T*) s;
}

void _Marshaller__seq_PlayerPosition::free( StaticValueType v ) const
{
  delete (_MICO_T*) v;
}

::CORBA::Boolean _Marshaller__seq_PlayerPosition::demarshal( ::CORBA::DataDecoder &dc, StaticValueType v ) const
{
  ::CORBA::ULong len;
  if( !dc.seq_begin( len ) )
    return FALSE;
  ((_MICO_T *) v)->length( len );
  for( ::CORBA::ULong i = 0; i < len; i++ ) {
    if( !_marshaller_PlayerPosition->demarshal( dc, &(*(_MICO_T*)v)[i] ) )
      return FALSE;
  }
  return dc.seq_end();
}

void _Marshaller__seq_PlayerPosition::marshal( ::CORBA::DataEncoder &ec, StaticValueType v ) const
{
  ::CORBA::ULong len = ((_MICO_T *) v)->length();
  ec.seq_begin( len );
  for( ::CORBA::ULong i = 0; i < len; i++ )
    _marshaller_PlayerPosition->marshal( ec, &(*(_MICO_T*)v)[i] );
  ec.seq_end();
}

::CORBA::StaticTypeInfo *_marshaller__seq_PlayerPosition;

struct __tc_init_CLIENT {
  __tc_init_CLIENT()
  {
    _marshaller_Client = new _Marshaller_Client;
    _marshaller__seq_PlayerPosition = new _Marshaller__seq_PlayerPosition;
  }

  ~__tc_init_CLIENT()
  {
    delete static_cast<_Marshaller_Client*>(_marshaller_Client);
    delete static_cast<_Marshaller__seq_PlayerPosition*>(_marshaller__seq_PlayerPosition);
  }
};

static __tc_init_CLIENT __init_CLIENT;

//--------------------------------------------------------
//  Implementation of skeletons
//--------------------------------------------------------

// PortableServer Skeleton Class for interface Client
POA_Client::~POA_Client()
{
}

::Client_ptr
POA_Client::_this ()
{
  CORBA::Object_var obj = PortableServer::ServantBase::_this();
  return ::Client::_narrow (obj);
}

CORBA::Boolean
POA_Client::_is_a (const char * repoid)
{
  if (strcmp (repoid, "IDL:Client:1.0") == 0) {
    return TRUE;
  }
  return FALSE;
}

CORBA::InterfaceDef_ptr
POA_Client::_get_interface ()
{
  CORBA::InterfaceDef_ptr ifd = PortableServer::ServantBase::_get_interface ("IDL:Client:1.0");

  if (CORBA::is_nil (ifd)) {
    mico_throw (CORBA::OBJ_ADAPTER (0, CORBA::COMPLETED_NO));
  }

  return ifd;
}

CORBA::RepositoryId
POA_Client::_primary_interface (const PortableServer::ObjectId &, PortableServer::POA_ptr)
{
  return CORBA::string_dup ("IDL:Client:1.0");
}

CORBA::Object_ptr
POA_Client::_make_stub (PortableServer::POA_ptr poa, CORBA::Object_ptr obj)
{
  return new ::Client_stub_clp (poa, obj);
}

bool
POA_Client::dispatch (CORBA::StaticServerRequest_ptr __req)
{
  #ifdef HAVE_EXCEPTIONS
  try {
  #endif
    switch (mico_string_hash (__req->op_name(), 7)) {
    case 0:
      if( strcmp( __req->op_name(), "nextMove" ) == 0 ) {

        if( !__req->read_args() )
          return true;

        nextMove();
        __req->write_results();
        return true;
      }
      break;
    case 1:
      if( strcmp( __req->op_name(), "update" ) == 0 ) {
        SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> _par_ppos;
        CORBA::StaticAny _sa_ppos( _marshaller__seq_PlayerPosition, &_par_ppos );

        __req->add_in_arg( &_sa_ppos );

        if( !__req->read_args() )
          return true;

        update( _par_ppos );
        __req->write_results();
        return true;
      }
      break;
    case 2:
      if( strcmp( __req->op_name(), "startGame" ) == 0 ) {
        SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> _par_ppos;
        CORBA::StaticAny _sa_ppos( _marshaller__seq_PlayerPosition, &_par_ppos );

        __req->add_in_arg( &_sa_ppos );

        if( !__req->read_args() )
          return true;

        startGame( _par_ppos );
        __req->write_results();
        return true;
      }
      break;
    case 5:
      if( strcmp( __req->op_name(), "finish" ) == 0 ) {

        if( !__req->read_args() )
          return true;

        finish();
        __req->write_results();
        return true;
      }
      break;
    }
  #ifdef HAVE_EXCEPTIONS
  } catch( CORBA::SystemException_catch &_ex ) {
    __req->set_exception( _ex->_clone() );
    __req->write_results();
    return true;
  } catch( ... ) {
    CORBA::UNKNOWN _ex (CORBA::OMGVMCID | 1, CORBA::COMPLETED_MAYBE);
    __req->set_exception (_ex->_clone());
    __req->write_results ();
    return true;
  }
  #endif

  return false;
}

void
POA_Client::invoke (CORBA::StaticServerRequest_ptr __req)
{
  if (dispatch (__req)) {
      return;
  }

  CORBA::Exception * ex = 
    new CORBA::BAD_OPERATION (0, CORBA::COMPLETED_NO);
  __req->set_exception (ex);
  __req->write_results();
}

