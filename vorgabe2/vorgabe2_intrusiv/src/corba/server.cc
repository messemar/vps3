/*
 *  MICO --- an Open Source CORBA implementation
 *  Copyright (c) 1997-2006 by The Mico Team
 *
 *  This file was automatically generated. DO NOT EDIT!
 */

#include <corba/server.h>


using namespace std;

//--------------------------------------------------------
//  Implementation of stubs
//--------------------------------------------------------

/*
 * Base interface for class Server
 */

Server::~Server()
{
}

void *
Server::_narrow_helper( const char *_repoid )
{
  if( strcmp( _repoid, "IDL:Server:1.0" ) == 0 )
    return (void *)this;
  return NULL;
}

Server_ptr
Server::_narrow( CORBA::Object_ptr _obj )
{
  Server_ptr _o;
  if( !CORBA::is_nil( _obj ) ) {
    void *_p;
    if( (_p = _obj->_narrow_helper( "IDL:Server:1.0" )))
      return _duplicate( (Server_ptr) _p );
    if (!strcmp (_obj->_repoid(), "IDL:Server:1.0") || _obj->_is_a_remote ("IDL:Server:1.0")) {
      _o = new Server_stub;
      _o->CORBA::Object::operator=( *_obj );
      return _o;
    }
  }
  return _nil();
}

Server_ptr
Server::_narrow( CORBA::AbstractBase_ptr _obj )
{
  return _narrow (_obj->_to_object());
}

class _Marshaller_Server : public ::CORBA::StaticTypeInfo {
    typedef Server_ptr _MICO_T;
  public:
    ~_Marshaller_Server();
    StaticValueType create () const;
    void assign (StaticValueType dst, const StaticValueType src) const;
    void free (StaticValueType) const;
    void release (StaticValueType) const;
    ::CORBA::Boolean demarshal (::CORBA::DataDecoder&, StaticValueType) const;
    void marshal (::CORBA::DataEncoder &, StaticValueType) const;
};


_Marshaller_Server::~_Marshaller_Server()
{
}

::CORBA::StaticValueType _Marshaller_Server::create() const
{
  return (StaticValueType) new _MICO_T( 0 );
}

void _Marshaller_Server::assign( StaticValueType d, const StaticValueType s ) const
{
  *(_MICO_T*) d = ::Server::_duplicate( *(_MICO_T*) s );
}

void _Marshaller_Server::free( StaticValueType v ) const
{
  ::CORBA::release( *(_MICO_T *) v );
  delete (_MICO_T*) v;
}

void _Marshaller_Server::release( StaticValueType v ) const
{
  ::CORBA::release( *(_MICO_T *) v );
}

::CORBA::Boolean _Marshaller_Server::demarshal( ::CORBA::DataDecoder &dc, StaticValueType v ) const
{
  ::CORBA::Object_ptr obj;
  if (!::CORBA::_stc_Object->demarshal(dc, &obj))
    return FALSE;
  *(_MICO_T *) v = ::Server::_narrow( obj );
  ::CORBA::Boolean ret = ::CORBA::is_nil (obj) || !::CORBA::is_nil (*(_MICO_T *)v);
  ::CORBA::release (obj);
  return ret;
}

void _Marshaller_Server::marshal( ::CORBA::DataEncoder &ec, StaticValueType v ) const
{
  ::CORBA::Object_ptr obj = *(_MICO_T *) v;
  ::CORBA::_stc_Object->marshal( ec, &obj );
}

::CORBA::StaticTypeInfo *_marshaller_Server;


/*
 * Stub interface for class Server
 */

Server_stub::~Server_stub()
{
}

#ifndef MICO_CONF_NO_POA

void *
POA_Server::_narrow_helper (const char * repoid)
{
  if (strcmp (repoid, "IDL:Server:1.0") == 0) {
    return (void *) this;
  }
  return NULL;
}

POA_Server *
POA_Server::_narrow (PortableServer::Servant serv) 
{
  void * p;
  if ((p = serv->_narrow_helper ("IDL:Server:1.0")) != NULL) {
    serv->_add_ref ();
    return (POA_Server *) p;
  }
  return NULL;
}

Server_stub_clp::Server_stub_clp ()
{
}

Server_stub_clp::Server_stub_clp (PortableServer::POA_ptr poa, CORBA::Object_ptr obj)
  : CORBA::Object(*obj), PortableServer::StubBase(poa)
{
}

Server_stub_clp::~Server_stub_clp ()
{
}

#endif // MICO_CONF_NO_POA

CORBA::UShort Server_stub::join( const char* _par_name )
{
  CORBA::StaticAny _sa_name( CORBA::_stc_string, &_par_name );
  CORBA::UShort _res;
  CORBA::StaticAny __res( CORBA::_stc_ushort, &_res );

  CORBA::StaticRequest __req( this, "join" );
  __req.add_in_arg( &_sa_name );
  __req.set_result( &__res );

  __req.invoke();

  mico_sii_throw( &__req, 
    0);
  return _res;
}


#ifndef MICO_CONF_NO_POA

CORBA::UShort
Server_stub_clp::join( const char* _par_name )
{
  PortableServer::Servant _serv = _preinvoke ();
  if (_serv) {
    POA_Server * _myserv = POA_Server::_narrow (_serv);
    if (_myserv) {
      CORBA::UShort __res;

      #ifdef HAVE_EXCEPTIONS
      try {
      #endif
        __res = _myserv->join(_par_name);
      #ifdef HAVE_EXCEPTIONS
      }
      catch (...) {
        _myserv->_remove_ref();
        _postinvoke();
        throw;
      }
      #endif

      _myserv->_remove_ref();
      _postinvoke ();
      return __res;
    }
    _postinvoke ();
  }

  return Server_stub::join(_par_name);
}

#endif // MICO_CONF_NO_POA

void Server_stub::move( CORBA::UShort _par_id, CORBA::UShort _par_direction )
{
  CORBA::StaticAny _sa_id( CORBA::_stc_ushort, &_par_id );
  CORBA::StaticAny _sa_direction( CORBA::_stc_ushort, &_par_direction );
  CORBA::StaticRequest __req( this, "move" );
  __req.add_in_arg( &_sa_id );
  __req.add_in_arg( &_sa_direction );

  __req.oneway();

  mico_sii_throw( &__req, 
    0);
}


#ifndef MICO_CONF_NO_POA

void
Server_stub_clp::move( CORBA::UShort _par_id, CORBA::UShort _par_direction )
{
  PortableServer::Servant _serv = _preinvoke ();
  if (_serv) {
    POA_Server * _myserv = POA_Server::_narrow (_serv);
    if (_myserv) {
      _myserv->move(_par_id, _par_direction);
      _myserv->_remove_ref();
      _postinvoke ();
      return;
    }
    _postinvoke ();
  }

  Server_stub::move(_par_id, _par_direction);
}

#endif // MICO_CONF_NO_POA

void Server_stub::leave( CORBA::UShort _par_id )
{
  CORBA::StaticAny _sa_id( CORBA::_stc_ushort, &_par_id );
  CORBA::StaticRequest __req( this, "leave" );
  __req.add_in_arg( &_sa_id );

  __req.invoke();

  mico_sii_throw( &__req, 
    0);
}


#ifndef MICO_CONF_NO_POA

void
Server_stub_clp::leave( CORBA::UShort _par_id )
{
  PortableServer::Servant _serv = _preinvoke ();
  if (_serv) {
    POA_Server * _myserv = POA_Server::_narrow (_serv);
    if (_myserv) {
      #ifdef HAVE_EXCEPTIONS
      try {
      #endif
        _myserv->leave(_par_id);
      #ifdef HAVE_EXCEPTIONS
      }
      catch (...) {
        _myserv->_remove_ref();
        _postinvoke();
        throw;
      }
      #endif

      _myserv->_remove_ref();
      _postinvoke ();
      return;
    }
    _postinvoke ();
  }

  Server_stub::leave(_par_id);
}

#endif // MICO_CONF_NO_POA

struct __tc_init_SERVER {
  __tc_init_SERVER()
  {
    _marshaller_Server = new _Marshaller_Server;
  }

  ~__tc_init_SERVER()
  {
    delete static_cast<_Marshaller_Server*>(_marshaller_Server);
  }
};

static __tc_init_SERVER __init_SERVER;

//--------------------------------------------------------
//  Implementation of skeletons
//--------------------------------------------------------

// PortableServer Skeleton Class for interface Server
POA_Server::~POA_Server()
{
}

::Server_ptr
POA_Server::_this ()
{
  CORBA::Object_var obj = PortableServer::ServantBase::_this();
  return ::Server::_narrow (obj);
}

CORBA::Boolean
POA_Server::_is_a (const char * repoid)
{
  if (strcmp (repoid, "IDL:Server:1.0") == 0) {
    return TRUE;
  }
  return FALSE;
}

CORBA::InterfaceDef_ptr
POA_Server::_get_interface ()
{
  CORBA::InterfaceDef_ptr ifd = PortableServer::ServantBase::_get_interface ("IDL:Server:1.0");

  if (CORBA::is_nil (ifd)) {
    mico_throw (CORBA::OBJ_ADAPTER (0, CORBA::COMPLETED_NO));
  }

  return ifd;
}

CORBA::RepositoryId
POA_Server::_primary_interface (const PortableServer::ObjectId &, PortableServer::POA_ptr)
{
  return CORBA::string_dup ("IDL:Server:1.0");
}

CORBA::Object_ptr
POA_Server::_make_stub (PortableServer::POA_ptr poa, CORBA::Object_ptr obj)
{
  return new ::Server_stub_clp (poa, obj);
}

bool
POA_Server::dispatch (CORBA::StaticServerRequest_ptr __req)
{
  #ifdef HAVE_EXCEPTIONS
  try {
  #endif
    if( strcmp( __req->op_name(), "join" ) == 0 ) {
      CORBA::String_var _par_name;
      CORBA::StaticAny _sa_name( CORBA::_stc_string, &_par_name._for_demarshal() );

      CORBA::UShort _res;
      CORBA::StaticAny __res( CORBA::_stc_ushort, &_res );
      __req->add_in_arg( &_sa_name );
      __req->set_result( &__res );

      if( !__req->read_args() )
        return true;

      _res = join( _par_name.inout() );
      __req->write_results();
      return true;
    }
    if( strcmp( __req->op_name(), "move" ) == 0 ) {
      CORBA::UShort _par_id;
      CORBA::StaticAny _sa_id( CORBA::_stc_ushort, &_par_id );
      CORBA::UShort _par_direction;
      CORBA::StaticAny _sa_direction( CORBA::_stc_ushort, &_par_direction );

      __req->add_in_arg( &_sa_id );
      __req->add_in_arg( &_sa_direction );

      if( !__req->read_args() )
        return true;

      move( _par_id, _par_direction );
      __req->write_results();
      return true;
    }
    if( strcmp( __req->op_name(), "leave" ) == 0 ) {
      CORBA::UShort _par_id;
      CORBA::StaticAny _sa_id( CORBA::_stc_ushort, &_par_id );

      __req->add_in_arg( &_sa_id );

      if( !__req->read_args() )
        return true;

      leave( _par_id );
      __req->write_results();
      return true;
    }
  #ifdef HAVE_EXCEPTIONS
  } catch( CORBA::SystemException_catch &_ex ) {
    __req->set_exception( _ex->_clone() );
    __req->write_results();
    return true;
  } catch( ... ) {
    CORBA::UNKNOWN _ex (CORBA::OMGVMCID | 1, CORBA::COMPLETED_MAYBE);
    __req->set_exception (_ex->_clone());
    __req->write_results ();
    return true;
  }
  #endif

  return false;
}

void
POA_Server::invoke (CORBA::StaticServerRequest_ptr __req)
{
  if (dispatch (__req)) {
      return;
  }

  CORBA::Exception * ex = 
    new CORBA::BAD_OPERATION (0, CORBA::COMPLETED_NO);
  __req->set_exception (ex);
  __req->write_results();
}

