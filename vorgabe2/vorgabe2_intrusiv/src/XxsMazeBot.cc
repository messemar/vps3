#include "XxsMazeBot.h"
#include <iostream>


//------------------------------------------------------------------

void XxsMazeBot::startGame(SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos)
{
	
	//std::cerr << "Player: "<< myName << " startGame" << endl;
	this->update(ppos);
}	
	
void XxsMazeBot::update(SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos)
{
	PlayerPositions pos;
	for(int i=0;i<ppos.length();i++){
		Point p(ppos[i].xPos,ppos[i].yPos);
		pos.add((PlayerId) i,p);
	}
	PlayerPositions pp = pos;
	ui.update(pp);
	const XxsMazeMap& map = ui.getMap();
	if (this->reachedGoal() || state == START_WAIT) {
		do {
			goal.x = random() % map.width();
			goal.y = random() % map.height();
		} while (!map.isFree(goal));
	}

	Point source = pp.getPos(myId);
	// recalculate way
	calcWay(map.getPlayerPos(myId), goal);

//	state = GAMEING;
}
	
void XxsMazeBot::nextMove()
{
  state = GAMEING;
}

void XxsMazeBot::finish()
{
	//myGame->leaveGame();
	state = FINISHED;
}
//------------------------------------------------------------------

void XxsMazeBot::calcWay(
	const Point& source,
	const Point& dest
) {
	const XxsMazeMap& map = getMap();
	StepMap stepMap(map.height(), vector<unsigned int>(map.width()));

	// calculate the stepping map
	stepMap[source.y][source.x] = (unsigned int)-1;
	calcWayHelper(stepMap, source.x, source.y, 0);
	
	// now generate the best way to dest
	way.clear();
	calcWay_generateWay(stepMap, source, dest.x, dest.y);


	// debugging information
	/*cerr<<"going from ("<<source.x<<","<<source.y<<") to ("
		<<dest.x<<","<<dest.y<<") in "<<way.size()<<" steps [ way : ";
	for (unsigned int i=0; i<way.size(); i++) {
		switch(way[i]) {
		case UP:		cerr<<"'up'";break;
		case UP_RIGHT:	cerr<<"'up right'";break;
		case RIGHT:		cerr<<"'right'";break;
		case DOWN_RIGHT:cerr<<"'down right'";break;
		case DOWN:		cerr<<"'down'";break;
		case DOWN_LEFT:	cerr<<"'down left'";break;
		case LEFT:		cerr<<"'left'";break;
		case UP_LEFT:	cerr<<"'up left'";break;
		default:		cerr<<"'stay'";
		}
		cerr<<" ";
	}
	cerr<<"]"<<endl;
	*/
}


void XxsMazeBot::calcWayHelper(
	StepMap& stepMap,
	const unsigned int x,
	const unsigned int y,
	const unsigned int steps
) const {
	// assume that the map has a 1 point wall so no
	// range checks are needed here
	calcWay_checkPoint(stepMap,x  ,y-1,steps+2); // upper
	calcWay_checkPoint(stepMap,x+1,y-1,steps+3); // upper right
	calcWay_checkPoint(stepMap,x+1,y  ,steps+2); // right
	calcWay_checkPoint(stepMap,x+1,y+1,steps+3); // lower right
	calcWay_checkPoint(stepMap,x  ,y+1,steps+2); // lower
	calcWay_checkPoint(stepMap,x-1,y+1,steps+3); // lower left
	calcWay_checkPoint(stepMap,x-1,y  ,steps+2); // left
	calcWay_checkPoint(stepMap,x-1,y-1,steps+3); // upper left
}


void XxsMazeBot::calcWay_generateWay(
	StepMap& stepMap,
	const Point& src,
	const unsigned int x,
	const unsigned int y
) {
	// reached startpoint?
	if (x == src.x && y == src.y)
		return;

	// determine the smallest neightbourpoint...
	Direction dir = STAY;
	unsigned int steps = stepMap[y][x];
	// we are going from the goal to our current position,
	// so we need to add inverted directions to the list!
	calcWay_checkWay(stepMap,src,x  ,y-1,steps,dir,DOWN      ); // come from upper
	calcWay_checkWay(stepMap,src,x+1,y-1,steps,dir,DOWN_LEFT ); // come from upper right
	calcWay_checkWay(stepMap,src,x+1,y  ,steps,dir,LEFT      ); // come from right
	calcWay_checkWay(stepMap,src,x+1,y+1,steps,dir,UP_LEFT   ); // come from lower right
	calcWay_checkWay(stepMap,src,x  ,y+1,steps,dir,UP        ); // come from lower
	calcWay_checkWay(stepMap,src,x-1,y+1,steps,dir,UP_RIGHT  ); // come from lower left
	calcWay_checkWay(stepMap,src,x-1,y  ,steps,dir,RIGHT     ); // come from left
	calcWay_checkWay(stepMap,src,x-1,y-1,steps,dir,DOWN_RIGHT); // come from upper left
	
	// ...and take it
	switch(dir) {
	case UP:		calcWay_generateWay(stepMap,src,x  ,y+1);break;
	case UP_RIGHT:	calcWay_generateWay(stepMap,src,x-1,y+1);break;
	case RIGHT:		calcWay_generateWay(stepMap,src,x-1,y);break;
	case DOWN_RIGHT:calcWay_generateWay(stepMap,src,x-1,y-1);break;
	case DOWN:		calcWay_generateWay(stepMap,src,x  ,y-1);break;
	case DOWN_LEFT:	calcWay_generateWay(stepMap,src,x+1,y-1);break;
	case LEFT:		calcWay_generateWay(stepMap,src,x+1,y);break;
	case UP_LEFT:	calcWay_generateWay(stepMap,src,x+1,y+1);break;
	default:		return; // reached a blind alley
	}
	
	// append _after_ the recursion to ensure right order
	// e.g. way[0] will be the first direction to move to
	if (dir != STAY) // STAY ^= dead end
		way.push_back(dir);
}

void XxsMazeBot::startup()
{
	myId = myGame->join(getName().c_str());
}

void XxsMazeBot::makeStep()
{
	if (state == GAMEING) {
		myGame->move(myId,way[0]);
		state = WAIT4UPDATE;
	}
}
