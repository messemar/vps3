#include "XxsMaze.h"

CORBA::UShort XxsMaze::join(const char* name) 
{
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
		CosNaming::NamingContext_var nc= CosNaming::NamingContext::_narrow(nsobj);
		CosNaming::Name cname;
		cname.length(1);
		cname[0].id= CORBA::string_dup(name);
		cname[0].kind=CORBA::string_dup("");
		CORBA::Object_var obj = nc->resolve(cname);
		Client_var pl= Client::_narrow(obj);
		std::cout<< "Client " << name <<"is joined"<<std::endl;
	if (state != LOGON) {
		std::cerr << "Game is in Progress, no logon possible!" << std::endl;
		return 4711;
	}
	// add player to list
	players.add(playerCount,pl);
	// generate initial start point for the player
	Point p;
	do {
		p.x = random()%map.width();
		p.y = random()%map.height();
	} while (!map.isFree(p));
	
	// update map
	map.setPlayerPos(playerCount,p);

	if ((unsigned int)playerCount+1 == max) {
		std::cerr << "all clients have logged on" << std::endl << std::flush;
		state = PRE_GAME;
	}
	
	return playerCount++; // so the id is ok for indexing the arrays
}


void XxsMaze::move(CORBA::UShort player, CORBA::UShort direction) {
	if (state == IN_GAME) {
		map.move((PlayerId)player,(Direction) direction);
	} else {
		std::cerr << "game not started yet!!" << std::endl;
	}
}


void XxsMaze::leave(CORBA::UShort player) {
	std::cerr<<"logout[ ID: "<<(PlayerId)player<<" ]"<<std::endl;
	players[(PlayerId)player]->finish();
	players.remove((PlayerId)player);
}
