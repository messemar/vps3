#include "XxsMazeGUI.h"

XxsMazeGUI::XxsMazeGUI(const XxsMazeMap& data) : map(&data)
{
	window = new XWindow(map.width()*PIX_SIZE,
		map.height()*PIX_SIZE, "xxsMaze");
}

XxsMazeGUI::XxsMazeGUI(const XxsMazeMap* data) : map(data)
{
	window = new XWindow(map.width()*PIX_SIZE,
		map.height()*PIX_SIZE, "xxsMaze");
}

XxsMazeGUI::XxsMazeGUI(const char* filename) : map(filename)
{
	map = new XxsMazeMap(filename);
	window = new XWindow(map.width()*PIX_SIZE,
		map.height()*PIX_SIZE, "xxsMaze");
	redraw();
}

XxsMazeGUI::~XxsMazeGUI() {
	delete window;
}

void XxsMazeGUI::redraw() {
	
	unsigned long h = map.height();
	unsigned long w = map.width();
	
	for (unsigned long y=0; y<h; y++)
		for (unsigned long x=0; x<w; x++) {
			switch (map.val(x,y)) {
			case XxsMazeMap::MAP_WALL: // wall
				window->fillRectangle(Gray, x*PIX_SIZE, y*PIX_SIZE, 
										PIX_SIZE, PIX_SIZE);
				break;
			case XxsMazeMap::MAP_WAY: // free
				window->fillRectangle(Black, x*PIX_SIZE, y*PIX_SIZE,
										PIX_SIZE, PIX_SIZE);
				break;
			default: // player
				{
				Color color;
				switch (map.val(x,y)%9) {
				case 0: color = Blue3; break;
				case 1: color = Red3; break;
				case 2: color = Purple3; break;
				case 3: color = Green3; break;
				case 4: color = Cyan3; break;
				case 5: color = Magenta3; break;
				case 6: color = Yellow3; break;
				case 7: color = Orange3; break;
				default: color = White; break;
				}
				window->fillRectangle(Black, x*PIX_SIZE, y*PIX_SIZE,
										PIX_SIZE, PIX_SIZE);
				window->fillArc(color, x*PIX_SIZE+1, y*PIX_SIZE+1, 
									PIX_SIZE-2, PIX_SIZE-2, 0, 360*64);
				}
			}
		}
}

void XxsMazeGUI::update(const PlayerPositions& pos)
{
	map.setAllPos(pos);
	redraw();
}
