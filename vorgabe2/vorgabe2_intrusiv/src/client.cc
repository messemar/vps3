#include "XxsMazeBot.h"
#include "XxsMazeClient.h"
#include "XxsMazeServer.h"

int main(int argc, char* argv[]) {
		if (argc < 2) {
		cerr << "usage: xxsMaze <corba> <mapfile> <clientname>" << endl;
		exit(4711);
	}
	char* mapfile = argv[3];

	// create an instance of the map and the game
	XxsMazeMap theMap(mapfile);

	//create orb
	CORBA::ORB_var orb = CORBA::ORB_init(argc, argv, "mico-local-orb");
	std::cout<< " ORB initialised"<<std::endl;
	//get remote game infos
	char* cname= argv[4];
	std::cout<< " name created"<<std::endl;
	
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
		CosNaming::NamingContext_var nc= CosNaming::NamingContext::_narrow(nsobj);
		CosNaming::Name name;
		name.length(1);
		name[0].id= CORBA::string_dup("XxsMaze");
		name[0].kind=CORBA::string_dup("");
		CORBA::Object_var obj = nc->resolve(name);
		Server_var sgame= Server::_narrow(obj);
	XxsMazeBot* bot= new XxsMazeBot(sgame,mapfile,cname);
	std::cout<< " bot created"<<std::endl;
	CORBA::Object_var poaobj = orb->resolve_initial_references("RootPOA");
		PortableServer::POA_var poa = PortableServer::POA::_narrow(poaobj);
		std::cout<< " poa"<<std::endl;
		// ...and the manager
		PortableServer::POAManager_var mgr = poa->the_POAManager();
		PortableServer::ObjectId_var oid=poa->activate_object(bot);
		mgr->activate();
		// naming service


		CosNaming::Name bname;
		bname.length(1);
		bname[0].id=CORBA::string_dup(cname);
		bname[0].kind=CORBA::string_dup("");
		nc->bind(bname, bot->_this());
	// create the corresponding client instances and start it
	XxsMazeBotClient* client = new XxsMazeBotClient(bot);
	std::cout<< " clients Created"<<std::endl;
	client->start();
	orb->run();
	std::cout<< "clients started"<<std::endl;
	client->join();
	
	delete bot;
	delete client;

}
