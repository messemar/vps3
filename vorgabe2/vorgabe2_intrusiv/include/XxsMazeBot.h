/** Defines XxsMazeBot.
 * 
 * @file XxsMazeBot.h
 */
#ifndef XxsMazeBot_h
#define XxsMazeBot_h

#include <vector>
#include "types/Direction.h"
#include "types/String.h"
#include "IPlayer.h"
#include "IxxsMaze.h"
#include "XxsMazeMap.h"
#include "XxsMazeGUI.h"
#include "XxsMaze.h"
#include "corba/client.h"
#include "corba/types.h"

#include <CORBA.h>
#include <coss/CosNaming.h>

/** This is a robot wich can play XxsMaze.
 * It implements the IPlayer interface. A XxsMazeBot
 * can connect to XxsMaze and make several moves.
 * Each robot choses a goal and tries to reach it.
 * Whenever it reaches that goal it choses another one.
 *
 */
class XxsMazeBot : public IPlayer {
	// shorter writing ;)
	typedef std::vector< std::vector< unsigned int > > StepMap;
	typedef std::vector< Direction > Way;

public:

	/** Copyconstructor from pointer to an other Bot.
	 *
	 */
	explicit XxsMazeBot(XxsMazeBot* other) : IPlayer(), 
		state(START_WAIT),
		myGame(other->getGame()), ui(other->getMap()), myName(other->getName())	{}

	/** Copyconstructor from reference to an other Bot.
	 *
	 */
	explicit XxsMazeBot(XxsMazeBot& other) : IPlayer(), 
		state(START_WAIT),
		myGame(other.getGame()), ui(other.getMap()), myName(other.getName()) {}

	/** Constructor from pointer to a game, a filename and 
	 * a name for this bot.
	 * 
	 */
	XxsMazeBot(Server_var game, const char* filename, String name): IPlayer(), 
		state(START_WAIT),
		myGame(game), ui(filename), myName(name) {}
	
	/**
	 *
	 */
	virtual ~XxsMazeBot(){}

	// the methods of the interface
	// -------------------------------------------------------------
	virtual void startGame(SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos);
	
	virtual void update(SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos);

        virtual void nextMove();
	
	virtual void finish();
	// --------------------------------------------------------------

	/** Returns a pointer to the game which this bot
	 * paricipates in.
	 *
	 * @return a pointer to the game
	 */
	//IxxsMaze* getGame() { return myGame.getProtectee(); }
	Server_var getGame() { return myGame; }

	/** Returns the name of this bot.
	 *
	 * @return the name of the player as String
	 */
	String getName() { return myName; }

	/** Returns a reference to the map.
	 *
	 * @return a reference to the map
	 */
	const XxsMazeMap& getMap() const { return ui.getMap(); }

	/** Returns the current state this bot is in.
	 * @see PlayerState
	 */
	PlayerState getState() { return state; }
	//void setState(PlayerState st) {  state = st; }

	/** Calculates a way to the chosen goal.
	 *
	 */
	void calcWay(const Point& source, const Point& dest);

	/** Starts this Robot by joining the game. 
	 *
	 */
	virtual void startup();

	/** Issues the next step. If necessary calculate
	 * the way to the goal. Then calculate the
	 * next step and send it to the game.
	 *
	 */
	virtual void makeStep();

	/** Tests if the chosen goal is already reached.
	 *
	 */
	bool reachedGoal () const
	{
		return getMap().getPlayerPos(myId) == goal; 
	}


private:
	// helper functions for calcWay
	void calcWayHelper(StepMap& sMap, unsigned int x, unsigned int y, 
			unsigned int steps) const;
	void calcWay_generateWay(StepMap& sMap, const Point& src, 
			const unsigned int x, const unsigned int y);
	inline void calcWay_checkPoint(StepMap& sMap, const unsigned int x,
			const unsigned int y, const unsigned int steps) const;
	inline void calcWay_checkWay(StepMap& sMap, const Point& src, 
			const unsigned int x, const unsigned int y, unsigned int& steps, 
			Direction& dir, const Direction newdir) const;

	// the current state of this robot
	PlayerState state;
	// the id of this robot
	PlayerId myId;
	// the game in which this robot participates
	Server_var myGame;
	// the graphical user interface
	XxsMazeGUI ui;
	// the name of this robot
	String myName;
	// the calculated way
	Way way;
	// the goal to reach
	Point goal;
};

inline
void XxsMazeBot::calcWay_checkPoint(
	StepMap& stepMap,
	const unsigned int x,
	const unsigned int y,
	const unsigned int steps
) const {
	const XxsMazeMap& map = getMap();
	if (map.isFree(x,y) && 
		(stepMap[y][x] == 0 || stepMap[y][x] > steps))
	{
		stepMap[y][x] = steps;
		calcWayHelper(stepMap, x, y, steps);
	}
}

inline
void XxsMazeBot::calcWay_checkWay(
	StepMap& stepMap,
	const Point& src,
	const unsigned int x,
	const unsigned int y,
	unsigned int& steps,
	Direction& dir,
	const Direction newdir
) const {
	const XxsMazeMap map = getMap();
	if (map.isFree(x,y) && stepMap[y][x] < steps) {
		dir = newdir;
		steps = stepMap[y][x];
	} else if (x == src.x && y == src.y) {
		dir = newdir;
		steps = 0;
	}
}
#endif
