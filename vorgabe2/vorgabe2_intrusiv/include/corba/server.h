/*
 *  MICO --- an Open Source CORBA implementation
 *  Copyright (c) 1997-2006 by The Mico Team
 *
 *  This file was automatically generated. DO NOT EDIT!
 */

#include <CORBA.h>
#include <mico/throw.h>

#ifndef __SERVER_H__
#define __SERVER_H__




class Server;
typedef Server *Server_ptr;
typedef Server_ptr ServerRef;
typedef ObjVar< Server > Server_var;
typedef ObjOut< Server > Server_out;




/*
 * Base class and common definitions for interface Server
 */

class Server : 
  virtual public CORBA::Object
{
  public:
    virtual ~Server();

    #ifdef HAVE_TYPEDEF_OVERLOAD
    typedef Server_ptr _ptr_type;
    typedef Server_var _var_type;
    #endif

    static Server_ptr _narrow( CORBA::Object_ptr obj );
    static Server_ptr _narrow( CORBA::AbstractBase_ptr obj );
    static Server_ptr _duplicate( Server_ptr _obj )
    {
      CORBA::Object::_duplicate (_obj);
      return _obj;
    }

    static Server_ptr _nil()
    {
      return 0;
    }

    virtual void *_narrow_helper( const char *repoid );

    virtual CORBA::UShort join( const char* name ) = 0;
    virtual void move( CORBA::UShort id, CORBA::UShort direction ) = 0;
    virtual void leave( CORBA::UShort id ) = 0;

  protected:
    Server() {};
  private:
    Server( const Server& );
    void operator=( const Server& );
};

// Stub for interface Server
class Server_stub:
  virtual public Server
{
  public:
    virtual ~Server_stub();
    CORBA::UShort join( const char* name );
    void move( CORBA::UShort id, CORBA::UShort direction );
    void leave( CORBA::UShort id );

  private:
    void operator=( const Server_stub& );
};

#ifndef MICO_CONF_NO_POA

class Server_stub_clp :
  virtual public Server_stub,
  virtual public PortableServer::StubBase
{
  public:
    Server_stub_clp (PortableServer::POA_ptr, CORBA::Object_ptr);
    virtual ~Server_stub_clp ();
    CORBA::UShort join( const char* name );
    void move( CORBA::UShort id, CORBA::UShort direction );
    void leave( CORBA::UShort id );

  protected:
    Server_stub_clp ();
  private:
    void operator=( const Server_stub_clp & );
};

#endif // MICO_CONF_NO_POA

#ifndef MICO_CONF_NO_POA

class POA_Server : virtual public PortableServer::StaticImplementation
{
  public:
    virtual ~POA_Server ();
    Server_ptr _this ();
    bool dispatch (CORBA::StaticServerRequest_ptr);
    virtual void invoke (CORBA::StaticServerRequest_ptr);
    virtual CORBA::Boolean _is_a (const char *);
    virtual CORBA::InterfaceDef_ptr _get_interface ();
    virtual CORBA::RepositoryId _primary_interface (const PortableServer::ObjectId &, PortableServer::POA_ptr);

    virtual void * _narrow_helper (const char *);
    static POA_Server * _narrow (PortableServer::Servant);
    virtual CORBA::Object_ptr _make_stub (PortableServer::POA_ptr, CORBA::Object_ptr);

    virtual CORBA::UShort join( const char* name ) = 0;
    virtual void move( CORBA::UShort id, CORBA::UShort direction ) = 0;
    virtual void leave( CORBA::UShort id ) = 0;

  protected:
    POA_Server () {};

  private:
    POA_Server (const POA_Server &);
    void operator= (const POA_Server &);
};

#endif // MICO_CONF_NO_POA

extern CORBA::StaticTypeInfo *_marshaller_Server;

#endif
