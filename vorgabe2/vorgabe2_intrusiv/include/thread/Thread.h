#ifndef Thread_h
#define Thread_h

#include <pthread.h> 
#include <iostream>


class Thread {
	public:
		Thread()
		{
			pthread_attr_init(&tattr);
			// this is the default value in linux
			pthread_attr_setscope(&tattr,PTHREAD_SCOPE_SYSTEM);
		}

		Thread(pthread_t other):tid(other) {}

		// implementing classes should override this and use join
		virtual ~Thread() { pthread_cancel(tid); }

//		static Thread self() { return Thread(pthread_self()); }

		void join() { pthread_join(tid,NULL); }

		//void start(void* (*run)(void*), void* arg);
		void start() 
		{ 
			pthread_create(&tid, &tattr, Thread::starter, this); 
			std::cout << "Thread " << tid << " started!" << std::endl;
		}
		

		void exit(int i) { pthread_exit(&i); }

		virtual void run() = 0;

	private:
		static void* starter(void* t) { ((Thread*)t)->run(); return 0; }

		pthread_attr_t tattr; // thread attributes
		pthread_t tid; // thread descriptor
};
#endif
