#ifndef IGame_h
#define IGame_h

#include "IPlayer.h"
#include "types/Direction.h"
#include "sync/Mutex.h"
#include "corba/server.h"

#include <CORBA.h>
#include <coss/CosNaming.h>

//States the server can be in
typedef enum {LOGON,PRE_GAME,IN_GAME,FINISH} GameState;

class IxxsMaze : public Mutex, virtual public POA_Server{
public:
	virtual ~IxxsMaze() {}

	virtual CORBA::UShort join(const char* name) = 0;

	virtual void leave(CORBA::UShort ident) = 0;

	virtual void move(CORBA::UShort id, CORBA::UShort dir) = 0;
};
#endif
