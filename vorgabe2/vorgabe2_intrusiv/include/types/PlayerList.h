#ifndef types_PlayerList_h
#define types_PlayerList_h

#include <map>
#include "types/PlayerId.h"
#include "IPlayer.h"
#include "corba/client.h"

class PlayerList {
public:
	void add(PlayerId id, Client_var pl);
	void remove(PlayerId id);
	const std::size_t numPlayers() const; 
	Client_var operator[](PlayerId);

private:
	typedef std::map<PlayerId,Client_var>::const_iterator const_iterator;
	typedef std::map<PlayerId,Client_var>::iterator iterator;
	std::map<PlayerId, Client_var> playerMap;
};


inline
void PlayerList::add(PlayerId id, Client_var pl)
{
	playerMap[id] = pl;
}

inline
void PlayerList::remove(PlayerId id)
{
	iterator it = playerMap.find(id);
	if ( it != playerMap.end()) {
		playerMap.erase(it);
	}
}

inline
const std::size_t PlayerList::numPlayers () const 
{
	return playerMap.size();
}

inline
Client_var PlayerList::operator[](PlayerId id)
{
	return playerMap[id];
}
#endif
