#ifndef XxsMazeGUI_h
#define XxsMazeGUI_h

#include "x/XWindow.h"
#include "XxsMazeMap.h"

class XxsMazeGUI {
public:
	/** constructor for an XMap out of a Map
	 *
	 */
	XxsMazeGUI(const XxsMazeMap& data);
	 
	/** constructor for an XMap out of a Map
	 *
	 */
	XxsMazeGUI(const XxsMazeMap* data);
	
	/** constructor for an XMap out of a map file
	 *
	 */
	XxsMazeGUI(const char* filename);

	/** to destroy XWindow again
	 *
	 */
	~XxsMazeGUI();

	/**redraw the map
	 *
	 */
	void redraw();

	/** Refreshes the Display. Positions of the players
	 * given are updated and the Display is refreshed.
	 *
	 * @param pos the positions of all players
	 */
	void update(const PlayerPositions& pos);

	/** Returns a refernece to the map.
	 *
	 * @ return a reference to a XxsMazeMap
	 */
	const XxsMazeMap& getMap() const { return map; }

private:
	// the map data
	XxsMazeMap map;

	// the window to draw at
	XWindow* window;

	// size of one point in the map in pixels on the screen
	enum { PIX_SIZE=15 };
	
};
#endif
