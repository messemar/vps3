/**
 * @file XxsMazeClient.h
 */
#ifndef XxsMazeBotClient_h
#define XxsMazeBotClient_h

#include "thread/Thread.h"
#include "sync/Monitor.h"
#include "XxsMazeBot.h"
#include "XxsMaze.h"
#include "corba/server.h"

/** A thread which controls a XxsMazeBot.
 *
 */
class XxsMazeBotClient : public Thread {
	public:
		/** Contructor from pointer to XxsMazeBot.
		 *
		 */
		explicit XxsMazeBotClient(XxsMazeBot* p)  
			: state(START_WAIT), myBot(p), game(p->getGame()) {}

		/** Contructor from reference to XxsMazeBot.
		 *
		 */
		explicit XxsMazeBotClient(XxsMazeBot& p) 
			: state(START_WAIT), myBot(&p), game(p.getGame()) {}

		virtual ~XxsMazeBotClient() 
		{
			join();
		}

		/** The main method of the client implementing Thread::run.
		 *
		 */
		virtual void run();
		 
	private:
		PlayerState state;
		Monitor<XxsMazeBot> myBot;
		//IxxsMaze* game;
		Server_var game;
};
#endif
