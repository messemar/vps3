#include "RemoteXxsMaze.h"
#include "Client_impl.h"

PlayerId RemoteXxsMaze::join(IPlayer* ip){
	
	CORBA::Object_var poaobj = orb->resolve_initial_references("RootPOA");
	PortableServer::POA_var poa = PortableServer::POA::_narrow(poaobj);
	std::cout<< "poa"<<std::endl;
	// ...and the manager
	PortableServer::POAManager_var mgr = poa->the_POAManager();
	Client_impl* client= new Client_impl(ip);
	PortableServer::ObjectId_var oid=poa->activate_object(client);
	mgr->activate();
	// naming service
	std::cout<< "mgr"<<std::endl;
	CORBA::Object_var nsobj = orb->resolve_initial_references("NameService");
	CosNaming::NamingContext_var nc = CosNaming::NamingContext::_narrow(nsobj);
	std::cout<< "nc"<<std::endl;
	CosNaming::Name name;
	name.length(1);
	name[0].id=CORBA::string_dup(clientName);
	name[0].kind=CORBA::string_dup("");
	nc->bind(name, client->_this());
	std::cout<< "nc binded"<<std::endl;
	//orb->run();
	std::cout<< "player joined"<<std::endl;
	return (PlayerId) game->s_join(clientName);
}

void RemoteXxsMaze::leave(PlayerId ident){
	game->s_leave((CORBA::UShort)ident);
}

void RemoteXxsMaze::move(PlayerId id, Direction dir){
	game->s_move((CORBA::UShort) id, (CORBA::UShort) dir);
	std::cout<<"server move"<< std::endl;
}
