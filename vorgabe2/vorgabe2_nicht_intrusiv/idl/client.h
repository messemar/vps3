/*
 *  MICO --- an Open Source CORBA implementation
 *  Copyright (c) 1997-2006 by The Mico Team
 *
 *  This file was automatically generated. DO NOT EDIT!
 */

#include <CORBA.h>
#include <mico/throw.h>

#ifndef __CLIENT_H__
#define __CLIENT_H__




class Client;
typedef Client *Client_ptr;
typedef Client_ptr ClientRef;
typedef ObjVar< Client > Client_var;
typedef ObjOut< Client > Client_out;


#include <types.h>


/*
 * Base class and common definitions for interface Client
 */

class Client : 
  virtual public CORBA::Object
{
  public:
    virtual ~Client();

    #ifdef HAVE_TYPEDEF_OVERLOAD
    typedef Client_ptr _ptr_type;
    typedef Client_var _var_type;
    #endif

    static Client_ptr _narrow( CORBA::Object_ptr obj );
    static Client_ptr _narrow( CORBA::AbstractBase_ptr obj );
    static Client_ptr _duplicate( Client_ptr _obj )
    {
      CORBA::Object::_duplicate (_obj);
      return _obj;
    }

    static Client_ptr _nil()
    {
      return 0;
    }

    virtual void *_narrow_helper( const char *repoid );

    virtual void c_startGame( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos ) = 0;
    virtual void c_update( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos ) = 0;
    virtual void c_nextMove() = 0;
    virtual void c_finish() = 0;

  protected:
    Client() {};
  private:
    Client( const Client& );
    void operator=( const Client& );
};

// Stub for interface Client
class Client_stub:
  virtual public Client
{
  public:
    virtual ~Client_stub();
    void c_startGame( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos );
    void c_update( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos );
    void c_nextMove();
    void c_finish();

  private:
    void operator=( const Client_stub& );
};

#ifndef MICO_CONF_NO_POA

class Client_stub_clp :
  virtual public Client_stub,
  virtual public PortableServer::StubBase
{
  public:
    Client_stub_clp (PortableServer::POA_ptr, CORBA::Object_ptr);
    virtual ~Client_stub_clp ();
    void c_startGame( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos );
    void c_update( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos );
    void c_nextMove();
    void c_finish();

  protected:
    Client_stub_clp ();
  private:
    void operator=( const Client_stub_clp & );
};

#endif // MICO_CONF_NO_POA

#ifndef MICO_CONF_NO_POA

class POA_Client : virtual public PortableServer::StaticImplementation
{
  public:
    virtual ~POA_Client ();
    Client_ptr _this ();
    bool dispatch (CORBA::StaticServerRequest_ptr);
    virtual void invoke (CORBA::StaticServerRequest_ptr);
    virtual CORBA::Boolean _is_a (const char *);
    virtual CORBA::InterfaceDef_ptr _get_interface ();
    virtual CORBA::RepositoryId _primary_interface (const PortableServer::ObjectId &, PortableServer::POA_ptr);

    virtual void * _narrow_helper (const char *);
    static POA_Client * _narrow (PortableServer::Servant);
    virtual CORBA::Object_ptr _make_stub (PortableServer::POA_ptr, CORBA::Object_ptr);

    virtual void c_startGame( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos ) = 0;
    virtual void c_update( SequenceTmpl< ::PlayerPosition,MICO_TID_DEF> ppos ) = 0;
    virtual void c_nextMove() = 0;
    virtual void c_finish() = 0;

  protected:
    POA_Client () {};

  private:
    POA_Client (const POA_Client &);
    void operator= (const POA_Client &);
};

#endif // MICO_CONF_NO_POA

extern CORBA::StaticTypeInfo *_marshaller_Client;

extern CORBA::StaticTypeInfo *_marshaller__seq_PlayerPosition;

#endif
