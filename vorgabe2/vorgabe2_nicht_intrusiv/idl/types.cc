/*
 *  MICO --- an Open Source CORBA implementation
 *  Copyright (c) 1997-2006 by The Mico Team
 *
 *  This file was automatically generated. DO NOT EDIT!
 */

#include <types.h>


using namespace std;

//--------------------------------------------------------
//  Implementation of stubs
//--------------------------------------------------------
#ifdef HAVE_EXPLICIT_STRUCT_OPS
PlayerPosition::PlayerPosition()
{
}

PlayerPosition::PlayerPosition( const PlayerPosition& _s )
{
  xPos = ((PlayerPosition&)_s).xPos;
  yPos = ((PlayerPosition&)_s).yPos;
  id = ((PlayerPosition&)_s).id;
}

PlayerPosition::~PlayerPosition()
{
}

PlayerPosition&
PlayerPosition::operator=( const PlayerPosition& _s )
{
  xPos = ((PlayerPosition&)_s).xPos;
  yPos = ((PlayerPosition&)_s).yPos;
  id = ((PlayerPosition&)_s).id;
  return *this;
}
#endif

class _Marshaller_PlayerPosition : public ::CORBA::StaticTypeInfo {
    typedef PlayerPosition _MICO_T;
  public:
    ~_Marshaller_PlayerPosition();
    StaticValueType create () const;
    void assign (StaticValueType dst, const StaticValueType src) const;
    void free (StaticValueType) const;
    ::CORBA::Boolean demarshal (::CORBA::DataDecoder&, StaticValueType) const;
    void marshal (::CORBA::DataEncoder &, StaticValueType) const;
};


_Marshaller_PlayerPosition::~_Marshaller_PlayerPosition()
{
}

::CORBA::StaticValueType _Marshaller_PlayerPosition::create() const
{
  return (StaticValueType) new _MICO_T;
}

void _Marshaller_PlayerPosition::assign( StaticValueType d, const StaticValueType s ) const
{
  *(_MICO_T*) d = *(_MICO_T*) s;
}

void _Marshaller_PlayerPosition::free( StaticValueType v ) const
{
  delete (_MICO_T*) v;
}

::CORBA::Boolean _Marshaller_PlayerPosition::demarshal( ::CORBA::DataDecoder &dc, StaticValueType v ) const
{
  return
    dc.struct_begin() &&
    CORBA::_stc_ulong->demarshal( dc, &((_MICO_T*)v)->xPos ) &&
    CORBA::_stc_ulong->demarshal( dc, &((_MICO_T*)v)->yPos ) &&
    CORBA::_stc_ushort->demarshal( dc, &((_MICO_T*)v)->id ) &&
    dc.struct_end();
}

void _Marshaller_PlayerPosition::marshal( ::CORBA::DataEncoder &ec, StaticValueType v ) const
{
  ec.struct_begin();
  CORBA::_stc_ulong->marshal( ec, &((_MICO_T*)v)->xPos );
  CORBA::_stc_ulong->marshal( ec, &((_MICO_T*)v)->yPos );
  CORBA::_stc_ushort->marshal( ec, &((_MICO_T*)v)->id );
  ec.struct_end();
}

::CORBA::StaticTypeInfo *_marshaller_PlayerPosition;

struct __tc_init_TYPES {
  __tc_init_TYPES()
  {
    _marshaller_PlayerPosition = new _Marshaller_PlayerPosition;
  }

  ~__tc_init_TYPES()
  {
    delete static_cast<_Marshaller_PlayerPosition*>(_marshaller_PlayerPosition);
  }
};

static __tc_init_TYPES __init_TYPES;

//--------------------------------------------------------
//  Implementation of skeletons
//--------------------------------------------------------
