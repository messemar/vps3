#ifndef SERVER_IMPL_h
#define SERVER_IMPL_h
#include "corba/server.h"
#include "IxxsMaze.h"

#include <CORBA.h>
#include <coss/CosNaming.h>

class Server_impl: virtual public POA_Server{
public:
	Server_impl(const char* bindName,CORBA::ORB_var& orb, IxxsMaze* game): orb(orb), game(game){
		
	}

	CORBA::UShort s_join(const char* name);

	void s_move(CORBA::UShort id, CORBA::UShort direction);

	void s_leave(CORBA::UShort id);


private:

	CORBA::ORB_var orb;
	IxxsMaze* game;
};
#endif
